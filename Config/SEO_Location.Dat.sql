-- ====2020-04-11 13:22:53.5240====

-- 2020-04-11 13:22:53.5260
SELECT * FROM [t_Config_DB];

-- 2020-04-11 13:22:53.5400
-- Transaction Begin
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[country_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);
;CREATE TABLE IF NOT EXISTS [t_Config_DB] (
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT,
	[Created] BOOLEAN,
	[LogLeave] INTEGER);

-- Transaction End

-- 2020-04-11 13:22:53.5480
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[country_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- 2020-04-11 13:22:58.9342
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2004,
	'Afghanistan',
	Null,
	'AF',
	'Country',
	'Afghanistan',
	'AF',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:58.9512
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2008,
	'Albania',
	Null,
	'AL',
	'Country',
	'Albania',
	'AL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:58.9572
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2010,
	'Antarctica',
	Null,
	'AQ',
	'Country',
	'Antarctica',
	'AQ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:58.9632
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2012,
	'Algeria',
	Null,
	'DZ',
	'Country',
	'Algeria',
	'DZ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:58.9681
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2016,
	'American Samoa',
	Null,
	'AS',
	'Country',
	'American Samoa',
	'AS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:58.9751
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2020,
	'Andorra',
	Null,
	'AD',
	'Country',
	'Andorra',
	'AD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:58.9801
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2024,
	'Angola',
	Null,
	'AO',
	'Country',
	'Angola',
	'AO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:58.9861
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2028,
	'Antigua and Barbuda',
	Null,
	'AG',
	'Country',
	'Antigua and Barbuda',
	'AG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:58.9921
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2031,
	'Azerbaijan',
	Null,
	'AZ',
	'Country',
	'Azerbaijan',
	'AZ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:58.9991
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2032,
	'Argentina',
	Null,
	'AR',
	'Country',
	'Argentina',
	'AR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.0051
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2036,
	'Australia',
	Null,
	'AU',
	'Country',
	'Australia',
	'AU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.0111
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2040,
	'Austria',
	Null,
	'AT',
	'Country',
	'Austria',
	'AT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.0170
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2044,
	'The Bahamas',
	Null,
	'BS',
	'Country',
	'The Bahamas',
	'BS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.0220
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2048,
	'Bahrain',
	Null,
	'BH',
	'Country',
	'Bahrain',
	'BH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.0300
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2050,
	'Bangladesh',
	Null,
	'BD',
	'Country',
	'Bangladesh',
	'BD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.0360
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2051,
	'Armenia',
	Null,
	'AM',
	'Country',
	'Armenia',
	'AM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.0410
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2052,
	'Barbados',
	Null,
	'BB',
	'Country',
	'Barbados',
	'BB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.0470
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2056,
	'Belgium',
	Null,
	'BE',
	'Country',
	'Belgium',
	'BE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.0539
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2060,
	'Bermuda',
	Null,
	'BM',
	'Region',
	'Bermuda',
	'BM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.0602
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2064,
	'Bhutan',
	Null,
	'BT',
	'Country',
	'Bhutan',
	'BT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.0652
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2068,
	'Bolivia',
	Null,
	'BO',
	'Country',
	'Bolivia',
	'BO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.0712
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2070,
	'Bosnia and Herzegovina',
	Null,
	'BA',
	'Country',
	'Bosnia and Herzegovina',
	'BA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.0812
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2072,
	'Botswana',
	Null,
	'BW',
	'Country',
	'Botswana',
	'BW',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.0883
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2074,
	'Bouvet Island',
	Null,
	'BV',
	'Region',
	'Bouvet Island',
	'BV',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.0954
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2076,
	'Brazil',
	Null,
	'BR',
	'Country',
	'Brazil',
	'BR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.1014
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2084,
	'Belize',
	Null,
	'BZ',
	'Country',
	'Belize',
	'BZ-BZ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.1074
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2086,
	'British Indian Ocean Territory',
	Null,
	'IO',
	'Region',
	'British Indian Ocean Territory',
	'IO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.1134
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2090,
	'Solomon Islands',
	Null,
	'SB',
	'Country',
	'Solomon Islands',
	'SB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.1194
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2092,
	'British Virgin Islands',
	Null,
	'VG',
	'Region',
	'British Virgin Islands',
	'VG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.1264
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2096,
	'Brunei',
	Null,
	'BN',
	'Country',
	'Brunei',
	'BN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.1324
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2100,
	'Bulgaria',
	Null,
	'BG',
	'Country',
	'Bulgaria',
	'BG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.1405
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2104,
	'Myanmar (Burma)',
	Null,
	'MM',
	'Country',
	'Myanmar (Burma)',
	'MM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.1475
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2108,
	'Burundi',
	Null,
	'BI',
	'Country',
	'Burundi',
	'BI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.1535
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2112,
	'Belarus',
	Null,
	'BY',
	'Country',
	'Belarus',
	'BY',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.1606
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2116,
	'Cambodia',
	Null,
	'KH',
	'Country',
	'Cambodia',
	'KH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.1666
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2120,
	'Cameroon',
	Null,
	'CM',
	'Country',
	'Cameroon',
	'CM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.1746
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2124,
	'Canada',
	Null,
	'CA',
	'Country',
	'Canada',
	'CA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.1817
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2132,
	'Cape Verde',
	Null,
	'CV',
	'Country',
	'Cape Verde',
	'CV',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.1877
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2136,
	'Cayman Islands',
	Null,
	'KY',
	'Region',
	'Cayman Islands',
	'KY',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.1937
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2140,
	'Central African Republic',
	Null,
	'CF',
	'Country',
	'Central African Republic',
	'CF',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.2018
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2144,
	'Sri Lanka',
	Null,
	'LK',
	'Country',
	'Sri Lanka',
	'LK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.2079
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2148,
	'Chad',
	Null,
	'TD',
	'Country',
	'Chad',
	'TD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.2139
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2152,
	'Chile',
	Null,
	'CL',
	'Country',
	'Chile',
	'CL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.2219
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2156,
	'China',
	Null,
	'CN',
	'Country',
	'China',
	'CN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.2279
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2158,
	'Taiwan',
	Null,
	'TW',
	'Region',
	'Taiwan',
	'TW',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.2339
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2162,
	'Christmas Island',
	Null,
	'CX',
	'Country',
	'Christmas Island',
	'CX',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.2398
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2166,
	'Cocos (Keeling) Islands',
	Null,
	'CC',
	'Country',
	'Cocos (Keeling) Islands',
	'CC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.2458
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2170,
	'Colombia',
	Null,
	'CO',
	'Country',
	'Colombia',
	'CO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.2518
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2174,
	'Comoros',
	Null,
	'KM',
	'Country',
	'Comoros',
	'KM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.2578
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2175,
	'Mayotte',
	Null,
	'YT',
	'Region',
	'Mayotte',
	'YT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.2638
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2178,
	'Republic of the Congo',
	Null,
	'CG',
	'Country',
	'Republic of the Congo',
	'CG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.2698
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2180,
	'Democratic Republic of the Congo',
	Null,
	'CD',
	'Country',
	'Democratic Republic of the Congo',
	'CD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.2768
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2184,
	'Cook Islands',
	Null,
	'CK',
	'Country',
	'Cook Islands',
	'CK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.2830
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2188,
	'Costa Rica',
	Null,
	'CR',
	'Country',
	'Costa Rica',
	'CR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.2890
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2191,
	'Croatia',
	Null,
	'HR',
	'Country',
	'Croatia',
	'HR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.2950
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2196,
	'Cyprus',
	Null,
	'CY',
	'Country',
	'Cyprus',
	'CY',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.3021
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2203,
	'Czechia',
	Null,
	'CZ',
	'Country',
	'Czechia',
	'CZ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.3081
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2204,
	'Benin',
	Null,
	'BJ',
	'Country',
	'Benin',
	'BJ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.3141
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2208,
	'Denmark',
	Null,
	'DK',
	'Country',
	'Denmark',
	'DK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.3201
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2212,
	'Dominica',
	Null,
	'DM',
	'Country',
	'Dominica',
	'DM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.3261
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2214,
	'Dominican Republic',
	Null,
	'DO',
	'Country',
	'Dominican Republic',
	'DO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.3321
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2218,
	'Ecuador',
	Null,
	'EC',
	'Country',
	'Ecuador',
	'EC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.3381
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2222,
	'El Salvador',
	Null,
	'SV',
	'Country',
	'El Salvador',
	'SV',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.3452
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2226,
	'Equatorial Guinea',
	Null,
	'GQ',
	'Country',
	'Equatorial Guinea',
	'GQ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.3512
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2231,
	'Ethiopia',
	Null,
	'ET',
	'Country',
	'Ethiopia',
	'ET',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.3573
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2232,
	'Eritrea',
	Null,
	'ER',
	'Country',
	'Eritrea',
	'ER',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.3632
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2233,
	'Estonia',
	Null,
	'EE',
	'Country',
	'Estonia',
	'EE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.3692
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2234,
	'Faroe Islands',
	Null,
	'FO',
	'Region',
	'Faroe Islands',
	'FO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.3762
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2238,
	'Falkland Islands (Islas Malvinas)',
	Null,
	'FK',
	'Region',
	'Falkland Islands (Islas Malvinas)',
	'FK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.3844
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2239,
	'South Georgia and the South Sandwich Islands',
	Null,
	'GS',
	'Country',
	'South Georgia and the South Sandwich Islands',
	'GS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.3904
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2242,
	'Fiji',
	Null,
	'FJ',
	'Country',
	'Fiji',
	'FJ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.3974
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2246,
	'Finland',
	Null,
	'FI',
	'Country',
	'Finland',
	'FI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.4037
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2250,
	'France',
	Null,
	'FR',
	'Country',
	'France',
	'FR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.4096
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2254,
	'French Guiana',
	Null,
	'GF',
	'Region',
	'French Guiana',
	'GF',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.4158
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2258,
	'French Polynesia',
	Null,
	'PF',
	'Country',
	'French Polynesia',
	'PF',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.4238
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2260,
	'French Southern and Antarctic Lands',
	Null,
	'TF',
	'Country',
	'French Southern and Antarctic Lands',
	'TF',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.4308
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2262,
	'Djibouti',
	Null,
	'DJ',
	'Country',
	'Djibouti',
	'DJ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.4368
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2266,
	'Gabon',
	Null,
	'GA',
	'Country',
	'Gabon',
	'GA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.4438
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2268,
	'Georgia',
	Null,
	'GE',
	'Country',
	'Georgia',
	'US-GA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.4488
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2270,
	'The Gambia',
	Null,
	'GM',
	'Country',
	'The Gambia',
	'GM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.4547
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2275,
	'Palestine',
	Null,
	'PS',
	'Region',
	'Palestine',
	'PS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.4607
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2276,
	'Germany',
	Null,
	'DE',
	'Country',
	'Germany',
	'DE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.4690
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2288,
	'Ghana',
	Null,
	'GH',
	'Country',
	'Ghana',
	'GH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.4750
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2292,
	'Gibraltar',
	Null,
	'GI',
	'Region',
	'Gibraltar',
	'GI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.4810
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2296,
	'Kiribati',
	Null,
	'KI',
	'Country',
	'Kiribati',
	'KI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.4879
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2300,
	'Greece',
	Null,
	'GR',
	'Country',
	'Greece',
	'GR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.4940
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2304,
	'Greenland',
	Null,
	'GL',
	'Region',
	'Greenland',
	'GL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.5012
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2308,
	'Grenada',
	Null,
	'GD',
	'Country',
	'Grenada',
	'GD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.5072
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2312,
	'Guadeloupe',
	Null,
	'GP',
	'Region',
	'Guadeloupe',
	'GP',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.5143
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2316,
	'Guam',
	Null,
	'GU',
	'Country',
	'Guam',
	'GU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.5213
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2320,
	'Guatemala',
	Null,
	'GT',
	'Country',
	'Guatemala',
	'GT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.5273
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2324,
	'Guinea',
	Null,
	'GN',
	'Country',
	'Guinea',
	'GN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.5343
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2328,
	'Guyana',
	Null,
	'GY',
	'Country',
	'Guyana',
	'GY',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.5414
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2332,
	'Haiti',
	Null,
	'HT',
	'Country',
	'Haiti',
	'HT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.5475
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2334,
	'Heard Island and McDonald Islands',
	Null,
	'HM',
	'Country',
	'Heard Island and McDonald Islands',
	'HM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.5545
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2336,
	'Vatican City',
	Null,
	'VA',
	'Country',
	'Vatican City',
	'VA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.5614
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2340,
	'Honduras',
	Null,
	'HN',
	'Country',
	'Honduras',
	'HN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.5674
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2344,
	'Hong Kong',
	Null,
	'HK',
	'Region',
	'Hong Kong',
	'HK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.5734
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2348,
	'Hungary',
	Null,
	'HU',
	'Country',
	'Hungary',
	'HU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.5794
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2352,
	'Iceland',
	Null,
	'IS',
	'Country',
	'Iceland',
	'IS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.5854
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2356,
	'India',
	Null,
	'IN',
	'Country',
	'India',
	'IN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.5925
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2360,
	'Indonesia',
	Null,
	'ID',
	'Country',
	'Indonesia',
	'ID',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.5994
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2368,
	'Iraq',
	Null,
	'IQ',
	'Country',
	'Iraq',
	'IQ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.6055
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2372,
	'Ireland',
	Null,
	'IE',
	'Country',
	'Ireland',
	'IE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.6115
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2376,
	'Israel',
	Null,
	'IL',
	'Country',
	'Israel',
	'IL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.6185
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2380,
	'Italy',
	Null,
	'IT',
	'Country',
	'Italy',
	'IT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.6245
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2384,
	'Cote d''Ivoire',
	Null,
	'CI',
	'Country',
	'Cote d''Ivoire',
	'CI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.6306
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2388,
	'Jamaica',
	Null,
	'JM',
	'Country',
	'Jamaica',
	'JM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.6366
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2392,
	'Japan',
	Null,
	'JP',
	'Country',
	'Japan',
	'JP',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.6446
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2398,
	'Kazakhstan',
	Null,
	'KZ',
	'Country',
	'Kazakhstan',
	'KZ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.6515
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2400,
	'Jordan',
	Null,
	'JO',
	'Country',
	'Jordan',
	'JO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.6575
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2404,
	'Kenya',
	Null,
	'KE',
	'Country',
	'Kenya',
	'KE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.6646
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2410,
	'South Korea',
	Null,
	'KR',
	'Country',
	'South Korea',
	'KR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.6716
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2414,
	'Kuwait',
	Null,
	'KW',
	'Country',
	'Kuwait',
	'KW',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.6786
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2417,
	'Kyrgyzstan',
	Null,
	'KG',
	'Country',
	'Kyrgyzstan',
	'KG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.6856
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2418,
	'Laos',
	Null,
	'LA',
	'Country',
	'Laos',
	'LA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.6916
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2422,
	'Lebanon',
	Null,
	'LB',
	'Country',
	'Lebanon',
	'LB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.6986
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2426,
	'Lesotho',
	Null,
	'LS',
	'Country',
	'Lesotho',
	'LS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.7056
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2428,
	'Latvia',
	Null,
	'LV',
	'Country',
	'Latvia',
	'LV',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.7116
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2430,
	'Liberia',
	Null,
	'LR',
	'Country',
	'Liberia',
	'LR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.7176
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2434,
	'Libya',
	Null,
	'LY',
	'Country',
	'Libya',
	'LY',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.7247
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2438,
	'Liechtenstein',
	Null,
	'LI',
	'Country',
	'Liechtenstein',
	'LI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.7317
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2440,
	'Lithuania',
	Null,
	'LT',
	'Country',
	'Lithuania',
	'LT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.7387
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2442,
	'Luxembourg',
	Null,
	'LU',
	'Country',
	'Luxembourg',
	'LU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.7448
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2446,
	'Macau',
	Null,
	'MO',
	'Region',
	'Macau',
	'MO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.7518
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2450,
	'Madagascar',
	Null,
	'MG',
	'Country',
	'Madagascar',
	'MG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.7578
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2454,
	'Malawi',
	Null,
	'MW',
	'Country',
	'Malawi',
	'MW',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.7638
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2458,
	'Malaysia',
	Null,
	'MY',
	'Country',
	'Malaysia',
	'MY',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.7699
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2462,
	'Maldives',
	Null,
	'MV',
	'Country',
	'Maldives',
	'MV',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.7759
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2466,
	'Mali',
	Null,
	'ML',
	'Country',
	'Mali',
	'ML',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.7819
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2470,
	'Malta',
	Null,
	'MT',
	'Country',
	'Malta',
	'MT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.7889
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2474,
	'Martinique',
	Null,
	'MQ',
	'Region',
	'Martinique',
	'MQ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.7950
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2478,
	'Mauritania',
	Null,
	'MR',
	'Country',
	'Mauritania',
	'MR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.8019
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2480,
	'Mauritius',
	Null,
	'MU',
	'Country',
	'Mauritius',
	'MU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.8089
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2484,
	'Mexico',
	Null,
	'MX',
	'Country',
	'Mexico',
	'MX',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.8150
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2492,
	'Monaco',
	Null,
	'MC',
	'Country',
	'Monaco',
	'MC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.8210
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2496,
	'Mongolia',
	Null,
	'MN',
	'Country',
	'Mongolia',
	'MN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.8270
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2498,
	'Moldova',
	Null,
	'MD',
	'Country',
	'Moldova',
	'MD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.8350
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2499,
	'Montenegro',
	Null,
	'ME',
	'Country',
	'Montenegro',
	'ME',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.8409
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2500,
	'Montserrat',
	Null,
	'MS',
	'Region',
	'Montserrat',
	'MS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.8470
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2504,
	'Morocco',
	Null,
	'MA',
	'Country',
	'Morocco',
	'MA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.8539
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2508,
	'Mozambique',
	Null,
	'MZ',
	'Country',
	'Mozambique',
	'MZ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.8609
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2512,
	'Oman',
	Null,
	'OM',
	'Country',
	'Oman',
	'OM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.8669
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2516,
	'Namibia',
	Null,
	'NA',
	'Country',
	'Namibia',
	'NA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.8729
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2520,
	'Nauru',
	Null,
	'NR',
	'Country',
	'Nauru',
	'NR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.8799
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2524,
	'Nepal',
	Null,
	'NP',
	'Country',
	'Nepal',
	'NP',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.8859
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2528,
	'Netherlands',
	Null,
	'NL',
	'Country',
	'Netherlands',
	'NL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.8920
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2531,
	'Curacao',
	Null,
	'CW',
	'Country',
	'Curacao',
	'CW',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.8990
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2533,
	'Aruba',
	Null,
	'AW',
	'Region',
	'Aruba',
	'AW',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.9050
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2534,
	'Sint Maarten',
	Null,
	'SX',
	'Country',
	'Sint Maarten',
	'SX',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.9110
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2535,
	'Caribbean Netherlands',
	Null,
	'BQ',
	'Country',
	'Caribbean Netherlands',
	'BQ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.9181
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2540,
	'New Caledonia',
	Null,
	'NC',
	'Country',
	'New Caledonia',
	'NC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.9251
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2548,
	'Vanuatu',
	Null,
	'VU',
	'Country',
	'Vanuatu',
	'VU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.9311
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2554,
	'New Zealand',
	Null,
	'NZ',
	'Country',
	'New Zealand',
	'NZ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.9381
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2558,
	'Nicaragua',
	Null,
	'NI',
	'Country',
	'Nicaragua',
	'NI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.9450
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2562,
	'Niger',
	Null,
	'NE',
	'Country',
	'Niger',
	'NE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.9520
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2566,
	'Nigeria',
	Null,
	'NG',
	'Country',
	'Nigeria',
	'NG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.9580
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2570,
	'Niue',
	Null,
	'NU',
	'Country',
	'Niue',
	'NU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.9650
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2574,
	'Norfolk Island',
	Null,
	'NF',
	'Country',
	'Norfolk Island',
	'NF',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.9710
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2578,
	'Norway',
	Null,
	'NO',
	'Country',
	'Norway',
	'NO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.9770
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2580,
	'Northern Mariana Islands',
	Null,
	'MP',
	'Country',
	'Northern Mariana Islands',
	'MP',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.9839
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2581,
	'United States Minor Outlying Islands',
	Null,
	'UM',
	'Country',
	'United States Minor Outlying Islands',
	'UM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.9899
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2583,
	'Federated States of Micronesia',
	Null,
	'FM',
	'Country',
	'Federated States of Micronesia',
	'FM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:22:59.9969
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2584,
	'Marshall Islands',
	Null,
	'MH',
	'Country',
	'Marshall Islands',
	'MH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.0029
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2585,
	'Palau',
	Null,
	'PW',
	'Country',
	'Palau',
	'PW',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.0109
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2586,
	'Pakistan',
	Null,
	'PK',
	'Country',
	'Pakistan',
	'PK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.0168
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2591,
	'Panama',
	Null,
	'PA',
	'Country',
	'Panama',
	'PA-8',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.0238
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2598,
	'Papua New Guinea',
	Null,
	'PG',
	'Country',
	'Papua New Guinea',
	'PG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.0298
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2600,
	'Paraguay',
	Null,
	'PY',
	'Country',
	'Paraguay',
	'PY',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.0358
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2604,
	'Peru',
	Null,
	'PE',
	'Country',
	'Peru',
	'PE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.0418
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2608,
	'Philippines',
	Null,
	'PH',
	'Country',
	'Philippines',
	'PH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.0488
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2612,
	'Pitcairn Islands',
	Null,
	'PN',
	'Country',
	'Pitcairn Islands',
	'PN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.0547
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2616,
	'Poland',
	Null,
	'PL',
	'Country',
	'Poland',
	'PL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.0617
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2620,
	'Portugal',
	Null,
	'PT',
	'Country',
	'Portugal',
	'PT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.0687
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2624,
	'Guinea-Bissau',
	Null,
	'GW',
	'Country',
	'Guinea-Bissau',
	'GW',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.0747
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2626,
	'Timor-Leste',
	Null,
	'TL',
	'Country',
	'Timor-Leste',
	'TL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.0817
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2630,
	'Puerto Rico',
	Null,
	'PR',
	'Region',
	'Puerto Rico',
	'PR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.0877
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2634,
	'Qatar',
	Null,
	'QA',
	'Country',
	'Qatar',
	'QA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.0946
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2638,
	'Reunion',
	Null,
	'RE',
	'Region',
	'Reunion',
	'RE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.1006
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2642,
	'Romania',
	Null,
	'RO',
	'Country',
	'Romania',
	'RO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.1076
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2643,
	'Russia',
	Null,
	'RU',
	'Country',
	'Russia',
	'RU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.1126
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2646,
	'Rwanda',
	Null,
	'RW',
	'Country',
	'Rwanda',
	'RW',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.1186
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2654,
	'Saint Helena, Ascension and Tristan da Cunha',
	Null,
	'SH',
	'Country',
	'Saint Helena, Ascension and Tristan da Cunha',
	'SH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.1256
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2659,
	'Saint Kitts and Nevis',
	Null,
	'KN',
	'Country',
	'Saint Kitts and Nevis',
	'KN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.1315
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2660,
	'Anguilla',
	Null,
	'AI',
	'Region',
	'Anguilla',
	'AI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.1385
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2662,
	'Saint Lucia',
	Null,
	'LC',
	'Country',
	'Saint Lucia',
	'LC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.1445
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2666,
	'Saint Pierre and Miquelon',
	Null,
	'PM',
	'Country',
	'Saint Pierre and Miquelon',
	'PM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.1505
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2670,
	'Saint Vincent and the Grenadines',
	Null,
	'VC',
	'Country',
	'Saint Vincent and the Grenadines',
	'VC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.1565
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2674,
	'San Marino',
	Null,
	'SM',
	'Country',
	'San Marino',
	'SM-07',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.1625
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2678,
	'Sao Tome and Principe',
	Null,
	'ST',
	'Country',
	'Sao Tome and Principe',
	'ST',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.1684
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2682,
	'Saudi Arabia',
	Null,
	'SA',
	'Country',
	'Saudi Arabia',
	'SA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.1754
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2686,
	'Senegal',
	Null,
	'SN',
	'Country',
	'Senegal',
	'SN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.1814
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2688,
	'Serbia',
	Null,
	'RS',
	'Country',
	'Serbia',
	'RS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.1874
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2690,
	'Seychelles',
	Null,
	'SC',
	'Country',
	'Seychelles',
	'SC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.1944
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2694,
	'Sierra Leone',
	Null,
	'SL',
	'Country',
	'Sierra Leone',
	'SL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.2014
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2702,
	'Singapore',
	Null,
	'SG',
	'Country',
	'Singapore',
	'SG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.2213
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2703,
	'Slovakia',
	Null,
	'SK',
	'Country',
	'Slovakia',
	'SK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.2273
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2704,
	'Vietnam',
	Null,
	'VN',
	'Country',
	'Vietnam',
	'VN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.2343
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2705,
	'Slovenia',
	Null,
	'SI',
	'Country',
	'Slovenia',
	'SI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.2393
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2706,
	'Somalia',
	Null,
	'SO',
	'Country',
	'Somalia',
	'SO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.2462
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2710,
	'South Africa',
	Null,
	'ZA',
	'Country',
	'South Africa',
	'ZA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.2522
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2716,
	'Zimbabwe',
	Null,
	'ZW',
	'Country',
	'Zimbabwe',
	'ZW',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.2582
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2724,
	'Spain',
	Null,
	'ES',
	'Country',
	'Spain',
	'ES',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.2652
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2732,
	'Western Sahara',
	Null,
	'EH',
	'Region',
	'Western Sahara',
	'EH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.2712
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2740,
	'Suriname',
	Null,
	'SR',
	'Country',
	'Suriname',
	'SR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.2771
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2744,
	'Svalbard and Jan Mayen',
	Null,
	'SJ',
	'Region',
	'Svalbard and Jan Mayen',
	'SJ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.2841
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2748,
	'Swaziland',
	Null,
	'SZ',
	'Country',
	'Swaziland',
	'SZ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.2911
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2752,
	'Sweden',
	Null,
	'SE',
	'Country',
	'Sweden',
	'SE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.2981
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2756,
	'Switzerland',
	Null,
	'CH',
	'Country',
	'Switzerland',
	'CH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.3051
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2762,
	'Tajikistan',
	Null,
	'TJ',
	'Country',
	'Tajikistan',
	'TJ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.3121
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2764,
	'Thailand',
	Null,
	'TH',
	'Country',
	'Thailand',
	'TH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.3190
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2768,
	'Togo',
	Null,
	'TG',
	'Country',
	'Togo',
	'TG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.3250
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2772,
	'Tokelau',
	Null,
	'TK',
	'Country',
	'Tokelau',
	'TK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.3320
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2776,
	'Tonga',
	Null,
	'TO',
	'Country',
	'Tonga',
	'TO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.3390
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2780,
	'Trinidad and Tobago',
	Null,
	'TT',
	'Country',
	'Trinidad and Tobago',
	'TT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.3450
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2784,
	'United Arab Emirates',
	Null,
	'AE',
	'Country',
	'United Arab Emirates',
	'AE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.3519
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2788,
	'Tunisia',
	Null,
	'TN',
	'Country',
	'Tunisia',
	'TN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.3589
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2792,
	'Turkey',
	Null,
	'TR',
	'Country',
	'Turkey',
	'TR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.3649
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2795,
	'Turkmenistan',
	Null,
	'TM',
	'Country',
	'Turkmenistan',
	'TM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.3719
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2796,
	'Turks and Caicos Islands',
	Null,
	'TC',
	'Region',
	'Turks and Caicos Islands',
	'TC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.3799
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2798,
	'Tuvalu',
	Null,
	'TV',
	'Country',
	'Tuvalu',
	'TV',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.3859
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2800,
	'Uganda',
	Null,
	'UG',
	'Country',
	'Uganda',
	'UG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.3928
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2804,
	'Ukraine',
	Null,
	'UA',
	'Country',
	'Ukraine',
	'UA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.3998
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2807,
	'Macedonia (FYROM)',
	Null,
	'MK',
	'Country',
	'Macedonia (FYROM)',
	'MK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.4058
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2818,
	'Egypt',
	Null,
	'EG',
	'Country',
	'Egypt',
	'EG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.4119
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2826,
	'United Kingdom',
	Null,
	'GB',
	'Country',
	'United Kingdom',
	'GB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.4189
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2831,
	'Guernsey',
	Null,
	'GG',
	'Country',
	'Guernsey',
	'GG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.4260
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2832,
	'Jersey',
	Null,
	'JE',
	'Country',
	'Jersey',
	'JE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.4320
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2834,
	'Tanzania',
	Null,
	'TZ',
	'Country',
	'Tanzania',
	'TZ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.4381
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2840,
	'United States',
	Null,
	'US',
	'Country',
	'United States',
	'US',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.4441
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2850,
	'U.S. Virgin Islands',
	Null,
	'VI',
	'Region',
	'U.S. Virgin Islands',
	'VI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.4511
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2854,
	'Burkina Faso',
	Null,
	'BF',
	'Country',
	'Burkina Faso',
	'BF',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.4571
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2858,
	'Uruguay',
	Null,
	'UY',
	'Country',
	'Uruguay',
	'UY',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.4641
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2860,
	'Uzbekistan',
	Null,
	'UZ',
	'Country',
	'Uzbekistan',
	'UZ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.4711
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2862,
	'Venezuela',
	Null,
	'VE',
	'Country',
	'Venezuela',
	'VE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.4770
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2876,
	'Wallis and Futuna',
	Null,
	'WF',
	'Country',
	'Wallis and Futuna',
	'WF',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.4840
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2882,
	'Samoa',
	Null,
	'WS',
	'Country',
	'Samoa',
	'WS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.4900
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2887,
	'Yemen',
	Null,
	'YE',
	'Country',
	'Yemen',
	'YE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.4970
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2894,
	'Zambia',
	Null,
	'ZM',
	'Country',
	'Zambia',
	'ZM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.5040
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	2900,
	'Kosovo',
	Null,
	'XK',
	'Region',
	'Kosovo',
	'XK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.5110
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20008,
	'Salta Province,Argentina',
	'2032',
	'AR',
	'Province',
	'Salta Province',
	'AR-A',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.5169
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20009,
	'Buenos Aires Province,Argentina',
	'2032',
	'AR',
	'Province',
	'Buenos Aires Province',
	'AR-B',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.5239
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20011,
	'Entre Rios,Argentina',
	'2032',
	'AR',
	'Province',
	'Entre Rios',
	'AR-E',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.5299
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20012,
	'La Rioja Province,Argentina',
	'2032',
	'AR',
	'Province',
	'La Rioja Province',
	'AR-F',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.5359
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20013,
	'Chaco Province,Argentina',
	'2032',
	'AR',
	'Province',
	'Chaco Province',
	'AR-H',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.5419
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20015,
	'Catamarca Province,Argentina',
	'2032',
	'AR',
	'Province',
	'Catamarca Province',
	'AR-K',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.5479
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20016,
	'La Pampa,Argentina',
	'2032',
	'AR',
	'Province',
	'La Pampa',
	'AR-L',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.5549
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20017,
	'Mendoza Province,Argentina',
	'2032',
	'AR',
	'Province',
	'Mendoza Province',
	'AR-M',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.5609
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20018,
	'Misiones Province,Argentina',
	'2032',
	'AR',
	'Province',
	'Misiones Province',
	'AR-N',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.5688
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20021,
	'Santa Fe Province,Argentina',
	'2032',
	'AR',
	'Province',
	'Santa Fe Province',
	'AR-S',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.5779
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20023,
	'Chubut Province,Argentina',
	'2032',
	'AR',
	'Province',
	'Chubut Province',
	'AR-U',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.5840
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20024,
	'Tierra del Fuego Province,Argentina',
	'2032',
	'AR',
	'Province',
	'Tierra del Fuego Province',
	'AR-V',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.5910
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20025,
	'Corrientes,Argentina',
	'2032',
	'AR',
	'Province',
	'Corrientes',
	'AR-W',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.5970
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20034,
	'Australian Capital Territory,Australia',
	'2036',
	'AU',
	'State',
	'Australian Capital Territory',
	'AU-ACT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.6040
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20035,
	'New South Wales,Australia',
	'2036',
	'AU',
	'State',
	'New South Wales',
	'AU-NSW',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.6100
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20036,
	'Northern Territory,Australia',
	'2036',
	'AU',
	'State',
	'Northern Territory',
	'AU-NT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.6171
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20037,
	'Queensland,Australia',
	'2036',
	'AU',
	'State',
	'Queensland',
	'AU-QLD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.6230
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20038,
	'South Australia,Australia',
	'2036',
	'AU',
	'State',
	'South Australia',
	'AU-SA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.6291
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20039,
	'Tasmania,Australia',
	'2036',
	'AU',
	'State',
	'Tasmania',
	'AU-TAS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.6362
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20041,
	'Western Australia,Australia',
	'2036',
	'AU',
	'State',
	'Western Australia',
	'AU-WA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.6422
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20042,
	'Burgenland,Austria',
	'2040',
	'AT',
	'State',
	'Burgenland',
	'AT-1',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.6492
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20043,
	'Carinthia,Austria',
	'2040',
	'AT',
	'State',
	'Carinthia',
	'AT-2',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.6562
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20044,
	'Lower Austria,Austria',
	'2040',
	'AT',
	'State',
	'Lower Austria',
	'AT-3',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.6623
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20045,
	'Upper Austria,Austria',
	'2040',
	'AT',
	'State',
	'Upper Austria',
	'AT-4',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.6682
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20047,
	'Styria,Austria',
	'2040',
	'AT',
	'State',
	'Styria',
	'AT-6',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.6742
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20048,
	'Tyrol,Austria',
	'2040',
	'AT',
	'State',
	'Tyrol',
	'AT-7',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.6812
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20049,
	'Vorarlberg,Austria',
	'2040',
	'AT',
	'State',
	'Vorarlberg',
	'AT-8',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.6882
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20052,
	'Brussels,Belgium',
	'2056',
	'BE',
	'Province',
	'Brussels',
	'BE-BRU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.6952
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20079,
	'Republika Srpska,Bosnia and Herzegovina',
	'2070',
	'BA',
	'Region',
	'Republika Srpska',
	'BA-SRP',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.7011
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20083,
	'Cochabamba Department,Bolivia',
	'2068',
	'BO',
	'Department',
	'Cochabamba Department',
	'BO-C',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.7081
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20086,
	'State of Alagoas,Brazil',
	'2076',
	'BR',
	'State',
	'State of Alagoas',
	'BR-AL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.7141
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20087,
	'State of Amazonas,Brazil',
	'2076',
	'BR',
	'State',
	'State of Amazonas',
	'BR-AM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.7222
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20088,
	'State of Bahia,Brazil',
	'2076',
	'BR',
	'State',
	'State of Bahia',
	'BR-BA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.7282
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20090,
	'Federal District,Brazil',
	'2076',
	'BR',
	'State',
	'Federal District',
	'BR-DF',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.7352
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20094,
	'State of Minas Gerais,Brazil',
	'2076',
	'BR',
	'State',
	'State of Minas Gerais',
	'BR-MG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.7421
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20095,
	'State of Mato Grosso do Sul,Brazil',
	'2076',
	'BR',
	'State',
	'State of Mato Grosso do Sul',
	'BR-MS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.7481
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20096,
	'State of Mato Grosso,Brazil',
	'2076',
	'BR',
	'State',
	'State of Mato Grosso',
	'BR-MT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.7551
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20099,
	'State of Pernambuco,Brazil',
	'2076',
	'BR',
	'State',
	'State of Pernambuco',
	'BR-PE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.7611
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20102,
	'State of Rio de Janeiro,Brazil',
	'2076',
	'BR',
	'State',
	'State of Rio de Janeiro',
	'BR-RJ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.7681
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20103,
	'State of Rio Grande do Norte,Brazil',
	'2076',
	'BR',
	'State',
	'State of Rio Grande do Norte',
	'BR-RN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.7751
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20104,
	'State of Rio Grande do Sul,Brazil',
	'2076',
	'BR',
	'State',
	'State of Rio Grande do Sul',
	'BR-RS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.7812
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20105,
	'State of Santa Catarina,Brazil',
	'2076',
	'BR',
	'State',
	'State of Santa Catarina',
	'BR-SC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.7872
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20114,
	'British Columbia,Canada',
	'2124',
	'CA',
	'Province',
	'British Columbia',
	'CA-BC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.7942
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20115,
	'Manitoba,Canada',
	'2124',
	'CA',
	'Province',
	'Manitoba',
	'CA-MB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.8012
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20117,
	'Newfoundland and Labrador,Canada',
	'2124',
	'CA',
	'Province',
	'Newfoundland and Labrador',
	'CA-NL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.8082
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20118,
	'Nova Scotia,Canada',
	'2124',
	'CA',
	'Province',
	'Nova Scotia',
	'CA-NS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.8142
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20119,
	'Northwest Territories,Canada',
	'2124',
	'CA',
	'Territory',
	'Northwest Territories',
	'CA-NT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.8212
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20120,
	'Nunavut,Canada',
	'2124',
	'CA',
	'Territory',
	'Nunavut',
	'CA-NU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.8262
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20122,
	'Prince Edward Island,Canada',
	'2124',
	'CA',
	'Province',
	'Prince Edward Island',
	'CA-PE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.8332
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20124,
	'Saskatchewan,Canada',
	'2124',
	'CA',
	'Province',
	'Saskatchewan',
	'CA-SK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.8402
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20125,
	'Yukon Territory,Canada',
	'2124',
	'CA',
	'Territory',
	'Yukon Territory',
	'CA-YT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.8472
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20126,
	'Aargau,Switzerland',
	'2756',
	'CH',
	'Canton',
	'Aargau',
	'CH-AG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.8521
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20127,
	'Appenzell Innerrhoden,Switzerland',
	'2756',
	'CH',
	'Canton',
	'Appenzell Innerrhoden',
	'CH-AI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.8601
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20129,
	'Canton of Bern,Switzerland',
	'2756',
	'CH',
	'Canton',
	'Canton of Bern',
	'CH-BE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.8681
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20130,
	'Basel-Landschaft,Switzerland',
	'2756',
	'CH',
	'Canton',
	'Basel-Landschaft',
	'CH-BL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.8761
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20132,
	'Fribourg,Switzerland',
	'2756',
	'CH',
	'Canton',
	'Fribourg',
	'CH-FR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.8831
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20134,
	'Glarus,Switzerland',
	'2756',
	'CH',
	'Canton',
	'Glarus',
	'CH-GL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.8900
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20135,
	'Grisons,Switzerland',
	'2756',
	'CH',
	'Canton',
	'Grisons',
	'CH-GR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.8980
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20136,
	'Jura,Switzerland',
	'2756',
	'CH',
	'Canton',
	'Jura',
	'CH-JU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.9050
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20139,
	'Nidwalden,Switzerland',
	'2756',
	'CH',
	'Canton',
	'Nidwalden',
	'CH-NW',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.9120
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20140,
	'Obwalden,Switzerland',
	'2756',
	'CH',
	'Canton',
	'Obwalden',
	'CH-OW',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.9210
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20142,
	'Schaffhausen,Switzerland',
	'2756',
	'CH',
	'Canton',
	'Schaffhausen',
	'CH-SH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.9289
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20143,
	'Solothurn,Switzerland',
	'2756',
	'CH',
	'Canton',
	'Solothurn',
	'CH-SO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.9349
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20144,
	'Schwyz,Switzerland',
	'2756',
	'CH',
	'Canton',
	'Schwyz',
	'CH-SZ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.9419
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20145,
	'Thurgau,Switzerland',
	'2756',
	'CH',
	'Canton',
	'Thurgau',
	'CH-TG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.9479
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20146,
	'Ticino,Switzerland',
	'2756',
	'CH',
	'Canton',
	'Ticino',
	'CH-TI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.9539
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20147,
	'Uri,Switzerland',
	'2756',
	'CH',
	'Canton',
	'Uri',
	'CH-UR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.9609
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20148,
	'Vaud,Switzerland',
	'2756',
	'CH',
	'Canton',
	'Vaud',
	'CH-VD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.9668
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20149,
	'Valais,Switzerland',
	'2756',
	'CH',
	'Canton',
	'Valais',
	'CH-VS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.9728
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20150,
	'Canton of Zug,Switzerland',
	'2756',
	'CH',
	'Canton',
	'Canton of Zug',
	'CH-ZG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.9798
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20152,
	'Antofagasta,Chile',
	'2152',
	'CL',
	'Region',
	'Antofagasta',
	'CL-AN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.9858
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20153,
	'Araucania,Chile',
	'2152',
	'CL',
	'Region',
	'Araucania',
	'CL-AR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.9918
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20155,
	'Coquimbo,Chile',
	'2152',
	'CL',
	'Region',
	'Coquimbo',
	'CL-CO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:00.9978
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20156,
	'O''Higgins,Chile',
	'2152',
	'CL',
	'Region',
	'O''Higgins',
	'CL-LI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.0057
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20157,
	'Los Lagos,Chile',
	'2152',
	'CL',
	'Region',
	'Los Lagos',
	'CL-LL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.0107
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20159,
	'Maule,Chile',
	'2152',
	'CL',
	'Region',
	'Maule',
	'CL-ML',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.0167
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20160,
	'Santiago Metropolitan Region,Chile',
	'2152',
	'CL',
	'Region',
	'Santiago Metropolitan Region',
	'CL-RM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.0227
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20162,
	'Valparaiso,Chile',
	'2152',
	'CL',
	'Region',
	'Valparaiso',
	'CL-VS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.0307
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20166,
	'Shanxi,China',
	'2156',
	'CN',
	'Region',
	'Shanxi',
	'CN-14',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.0357
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20167,
	'Inner Mongolia,China',
	'2156',
	'CN',
	'Region',
	'Inner Mongolia',
	'CN-15',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.0406
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20168,
	'Liaoning,China',
	'2156',
	'CN',
	'Region',
	'Liaoning',
	'CN-21',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.0466
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20170,
	'Heilongjiang,China',
	'2156',
	'CN',
	'Region',
	'Heilongjiang',
	'CN-23',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.0526
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20172,
	'Jiangsu,China',
	'2156',
	'CN',
	'Region',
	'Jiangsu',
	'CN-32',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.0576
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20173,
	'Zhejiang,China',
	'2156',
	'CN',
	'Region',
	'Zhejiang',
	'CN-33',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.0646
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20174,
	'Anhui,China',
	'2156',
	'CN',
	'Region',
	'Anhui',
	'CN-34',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.0706
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20175,
	'Fujian,China',
	'2156',
	'CN',
	'Region',
	'Fujian',
	'CN-35',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.0785
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20176,
	'Jiangxi,China',
	'2156',
	'CN',
	'Region',
	'Jiangxi',
	'CN-36',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.0855
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20177,
	'Shandong,China',
	'2156',
	'CN',
	'Region',
	'Shandong',
	'CN-37',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.0947
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20179,
	'Hubei,China',
	'2156',
	'CN',
	'Region',
	'Hubei',
	'CN-42',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.1016
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20180,
	'Hunan,China',
	'2156',
	'CN',
	'Region',
	'Hunan',
	'CN-43',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.1096
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20181,
	'Guangdong,China',
	'2156',
	'CN',
	'Region',
	'Guangdong',
	'CN-44',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.1166
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20182,
	'Guangxi,China',
	'2156',
	'CN',
	'Region',
	'Guangxi',
	'CN-45',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.1226
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20183,
	'Hainan,China',
	'2156',
	'CN',
	'Region',
	'Hainan',
	'CN-46',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.1296
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20184,
	'Chongqing,China',
	'2156',
	'CN',
	'Region',
	'Chongqing',
	'CN-50',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.1366
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20185,
	'Sichuan,China',
	'2156',
	'CN',
	'Region',
	'Sichuan',
	'CN-51',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.1436
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20186,
	'Guizhou,China',
	'2156',
	'CN',
	'Region',
	'Guizhou',
	'CN-52',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.1507
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20187,
	'Yunnan,China',
	'2156',
	'CN',
	'Region',
	'Yunnan',
	'CN-53',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.1567
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20188,
	'Tibet,China',
	'2156',
	'CN',
	'Region',
	'Tibet',
	'CN-54',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.1628
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20189,
	'Shaanxi,China',
	'2156',
	'CN',
	'Region',
	'Shaanxi',
	'CN-61',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.1687
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20190,
	'Gansu,China',
	'2156',
	'CN',
	'Region',
	'Gansu',
	'CN-62',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.1748
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20191,
	'Qinghai,China',
	'2156',
	'CN',
	'Region',
	'Qinghai',
	'CN-63',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.1818
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20192,
	'Ningxia,China',
	'2156',
	'CN',
	'Region',
	'Ningxia',
	'CN-64',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.1878
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20193,
	'Xinjiang,China',
	'2156',
	'CN',
	'Region',
	'Xinjiang',
	'CN-65',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.1938
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20198,
	'Antioquia,Colombia',
	'2170',
	'CO',
	'Region',
	'Antioquia',
	'CO-ANT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.1997
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20199,
	'Atlantico,Colombia',
	'2170',
	'CO',
	'Region',
	'Atlantico',
	'CO-ATL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.2067
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20201,
	'Caldas,Colombia',
	'2170',
	'CO',
	'Region',
	'Caldas',
	'CO-CAL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.2128
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20202,
	'Cundinamarca,Colombia',
	'2170',
	'CO',
	'Region',
	'Cundinamarca',
	'CO-CUN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.2198
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20204,
	'Huila,Colombia',
	'2170',
	'CO',
	'Region',
	'Huila',
	'CO-HUI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.2258
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20205,
	'Meta,Colombia',
	'2170',
	'CO',
	'Region',
	'Meta',
	'CO-MET',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.2328
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20206,
	'Narino,Colombia',
	'2170',
	'CO',
	'Region',
	'Narino',
	'CO-NAR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.2389
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20207,
	'Putumayo,Colombia',
	'2170',
	'CO',
	'Region',
	'Putumayo',
	'CO-PUT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.2450
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20208,
	'Risaralda,Colombia',
	'2170',
	'CO',
	'Region',
	'Risaralda',
	'CO-RIS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.2511
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20210,
	'Tolima,Colombia',
	'2170',
	'CO',
	'Region',
	'Tolima',
	'CO-TOL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.2571
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20211,
	'Valle del Cauca,Colombia',
	'2170',
	'CO',
	'Region',
	'Valle del Cauca',
	'CO-VAC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.2631
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20218,
	'South Bohemian Region,Czechia',
	'2203',
	'CZ',
	'Region',
	'South Bohemian Region',
	'CZ-JC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.2691
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20219,
	'South Moravian Region,Czechia',
	'2203',
	'CZ',
	'Region',
	'South Moravian Region',
	'CZ-JM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.2771
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20229,
	'Bavaria,Germany',
	'2276',
	'DE',
	'State',
	'Bavaria',
	'DE-BY',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.2830
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20231,
	'Hesse,Germany',
	'2276',
	'DE',
	'State',
	'Hesse',
	'DE-HE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.2891
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20233,
	'Mecklenburg-Vorpommern,Germany',
	'2276',
	'DE',
	'State',
	'Mecklenburg-Vorpommern',
	'DE-MV',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.2961
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20234,
	'Lower Saxony,Germany',
	'2276',
	'DE',
	'State',
	'Lower Saxony',
	'DE-NI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.3020
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20235,
	'North Rhine-Westphalia,Germany',
	'2276',
	'DE',
	'State',
	'North Rhine-Westphalia',
	'DE-NW',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.3080
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20236,
	'Rhineland-Palatinate,Germany',
	'2276',
	'DE',
	'State',
	'Rhineland-Palatinate',
	'DE-RP',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.3140
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20237,
	'Schleswig-Holstein,Germany',
	'2276',
	'DE',
	'State',
	'Schleswig-Holstein',
	'DE-SH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.3200
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20238,
	'Saarland,Germany',
	'2276',
	'DE',
	'State',
	'Saarland',
	'DE-SL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.3270
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20239,
	'Saxony,Germany',
	'2276',
	'DE',
	'State',
	'Saxony',
	'DE-SN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.3320
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20240,
	'Saxony-Anhalt,Germany',
	'2276',
	'DE',
	'State',
	'Saxony-Anhalt',
	'DE-ST',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.3381
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20241,
	'Thuringia,Germany',
	'2276',
	'DE',
	'State',
	'Thuringia',
	'DE-TH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.3441
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20243,
	'Capital Region of Denmark,Denmark',
	'2208',
	'DK',
	'Region',
	'Capital Region of Denmark',
	'DK-84',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.3501
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20245,
	'Region Zealand,Denmark',
	'2208',
	'DK',
	'Region',
	'Region Zealand',
	'DK-85',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.3561
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20254,
	'Central Denmark Region,Denmark',
	'2208',
	'DK',
	'Region',
	'Central Denmark Region',
	'DK-82',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.3631
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20256,
	'North Denmark Region,Denmark',
	'2208',
	'DK',
	'Region',
	'North Denmark Region',
	'DK-81',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.3680
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20260,
	'Azuay,Ecuador',
	'2218',
	'EC',
	'Province',
	'Azuay',
	'EC-A',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.3750
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20261,
	'Guayas,Ecuador',
	'2218',
	'EC',
	'Province',
	'Guayas',
	'EC-G',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.3820
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20264,
	'Pichincha,Ecuador',
	'2218',
	'EC',
	'Province',
	'Pichincha',
	'EC-P',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.3880
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20265,
	'Tungurahua,Ecuador',
	'2218',
	'EC',
	'Province',
	'Tungurahua',
	'EC-T',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.3950
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20276,
	'Castile-La Mancha,Spain',
	'2724',
	'ES',
	'Autonomous Community',
	'Castile-La Mancha',
	'ES-CM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.4009
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20277,
	'Canary Islands,Spain',
	'2724',
	'ES',
	'Autonomous Community',
	'Canary Islands',
	'ES-CN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.4079
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20278,
	'Catalonia,Spain',
	'2724',
	'ES',
	'Autonomous Community',
	'Catalonia',
	'ES-CT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.4149
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20279,
	'Extremadura,Spain',
	'2724',
	'ES',
	'Autonomous Community',
	'Extremadura',
	'ES-EX',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.4209
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20280,
	'Galicia,Spain',
	'2724',
	'ES',
	'Autonomous Community',
	'Galicia',
	'ES-GA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.4269
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20282,
	'Community of Madrid,Spain',
	'2724',
	'ES',
	'Autonomous Community',
	'Community of Madrid',
	'ES-MD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.4339
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20284,
	'Region of Murcia,Spain',
	'2724',
	'ES',
	'Autonomous Community',
	'Region of Murcia',
	'ES-MC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.4408
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20287,
	'Province of Balearic Islands,Balearic Islands,Spain',
	'21387',
	'ES',
	'Province',
	'Balearic Islands',
	'ES-IB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.4478
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20289,
	'Basque Country,Spain',
	'2724',
	'ES',
	'Autonomous Community',
	'Basque Country',
	'ES-PV',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.4548
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20311,
	'Alsace,France',
	'2250',
	'FR',
	'Region',
	'Alsace',
	'FR-A',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.4608
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20313,
	'Aquitaine,France',
	'2250',
	'FR',
	'Region',
	'Aquitaine',
	'FR-B',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.4668
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20314,
	'Auvergne,France',
	'2250',
	'FR',
	'Region',
	'Auvergne',
	'FR-C',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.4728
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20315,
	'Burgundy,France',
	'2250',
	'FR',
	'Region',
	'Burgundy',
	'FR-D',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.4788
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20316,
	'Brittany,France',
	'2250',
	'FR',
	'Region',
	'Brittany',
	'FR-E',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.4847
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20317,
	'Centre-Val de Loire,France',
	'2250',
	'FR',
	'Region',
	'Centre-Val de Loire',
	'FR-F',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.4917
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20318,
	'Champagne-Ardenne,France',
	'2250',
	'FR',
	'Region',
	'Champagne-Ardenne',
	'FR-G',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.4987
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20322,
	'Languedoc-Roussillon,France',
	'2250',
	'FR',
	'Region',
	'Languedoc-Roussillon',
	'FR-K',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.5037
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20323,
	'Limousin,France',
	'2250',
	'FR',
	'Region',
	'Limousin',
	'FR-L',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.5107
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20324,
	'Lorraine,France',
	'2250',
	'FR',
	'Region',
	'Lorraine',
	'FR-M',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.5166
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20326,
	'Nord-Pas-de-Calais,France',
	'2250',
	'FR',
	'Region',
	'Nord-Pas-de-Calais',
	'FR-O',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.5226
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20327,
	'Lower Normandy,France',
	'2250',
	'FR',
	'Region',
	'Lower Normandy',
	'FR-P',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.5296
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20328,
	'Upper Normandy,France',
	'2250',
	'FR',
	'Region',
	'Upper Normandy',
	'FR-Q',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.5356
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20329,
	'Pays de la Loire,France',
	'2250',
	'FR',
	'Region',
	'Pays de la Loire',
	'FR-R',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.5416
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20330,
	'Picardy,France',
	'2250',
	'FR',
	'Region',
	'Picardy',
	'FR-S',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.5476
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20331,
	'Poitou-Charentes,France',
	'2250',
	'FR',
	'Region',
	'Poitou-Charentes',
	'FR-T',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.5525
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20333,
	'Rhone-Alpes,France',
	'2250',
	'FR',
	'Region',
	'Rhone-Alpes',
	'FR-V',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.5585
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20340,
	'Isle of Man,United Kingdom',
	'2826',
	'GB',
	'Province',
	'Isle of Man',
	'IM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.5645
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20341,
	'Northern Ireland,United Kingdom',
	'2826',
	'GB',
	'Province',
	'Northern Ireland',
	'GB-NIR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.5715
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20407,
	'Zagreb County,Croatia',
	'2191',
	'HR',
	'County',
	'Zagreb County',
	'HR-01',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.5775
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20408,
	'Primorje-Gorski Kotar County,Croatia',
	'2191',
	'HR',
	'County',
	'Primorje-Gorski Kotar County',
	'HR-08',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.5835
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20409,
	'Zadar County,Croatia',
	'2191',
	'HR',
	'County',
	'Zadar County',
	'HR-13',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.5895
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20410,
	'Osijek-Baranja County,Croatia',
	'2191',
	'HR',
	'County',
	'Osijek-Baranja County',
	'HR-14',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.5955
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20411,
	'Vukovar-Srijem County,Croatia',
	'2191',
	'HR',
	'County',
	'Vukovar-Srijem County',
	'HR-16',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.6015
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20412,
	'Split-Dalmatia County,Croatia',
	'2191',
	'HR',
	'County',
	'Split-Dalmatia County',
	'HR-17',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.6075
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20413,
	'Dubrovnik-Neretva County,Croatia',
	'2191',
	'HR',
	'County',
	'Dubrovnik-Neretva County',
	'HR-19',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.6135
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20415,
	'Baranya,Hungary',
	'2348',
	'HU',
	'County',
	'Baranya',
	'HU-BA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.6205
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20422,
	'Gyor-Moson-Sopron,Hungary',
	'2348',
	'HU',
	'County',
	'Gyor-Moson-Sopron',
	'HU-GS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.6265
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20425,
	'Heves County,Hungary',
	'2348',
	'HU',
	'County',
	'Heves County',
	'HU-HE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.6325
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20429,
	'Pest County,Hungary',
	'2348',
	'HU',
	'County',
	'Pest County',
	'HU-PE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.6386
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20430,
	'Somogy County,Hungary',
	'2348',
	'HU',
	'County',
	'Somogy County',
	'HU-SO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.6446
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20432,
	'Tolna County,Hungary',
	'2348',
	'HU',
	'County',
	'Tolna County',
	'HU-TO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.6516
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20433,
	'Vas County,Hungary',
	'2348',
	'HU',
	'County',
	'Vas County',
	'HU-VA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.6576
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20435,
	'Zala County,Hungary',
	'2348',
	'HU',
	'County',
	'Zala County',
	'HU-ZA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.6636
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20438,
	'West Java,Indonesia',
	'2360',
	'ID',
	'Province',
	'West Java',
	'ID-JB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.6696
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20439,
	'East Java,Indonesia',
	'2360',
	'ID',
	'Province',
	'East Java',
	'ID-JI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.6756
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20441,
	'Central Java,Indonesia',
	'2360',
	'ID',
	'Province',
	'Central Java',
	'ID-JT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.6816
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20442,
	'East Kalimantan,Indonesia',
	'2360',
	'ID',
	'Province',
	'East Kalimantan',
	'ID-KI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.6885
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20443,
	'South Kalimantan,Indonesia',
	'2360',
	'ID',
	'Province',
	'South Kalimantan',
	'ID-KS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.6945
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20444,
	'Lampung,Indonesia',
	'2360',
	'ID',
	'Province',
	'Lampung',
	'ID-LA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.7005
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20445,
	'West Nusa Tenggara,Indonesia',
	'2360',
	'ID',
	'Province',
	'West Nusa Tenggara',
	'ID-NB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.7065
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20446,
	'Riau,Indonesia',
	'2360',
	'ID',
	'Province',
	'Riau',
	'ID-RI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.7135
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20447,
	'North Sulawesi,Indonesia',
	'2360',
	'ID',
	'Province',
	'North Sulawesi',
	'ID-SA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.7205
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20448,
	'South Sulawesi,Indonesia',
	'2360',
	'ID',
	'Province',
	'South Sulawesi',
	'ID-SN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.7274
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20449,
	'South Sumatra,Indonesia',
	'2360',
	'ID',
	'Province',
	'South Sumatra',
	'ID-SS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.7335
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20450,
	'North Sumatra,Indonesia',
	'2360',
	'ID',
	'Province',
	'North Sumatra',
	'ID-SU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.7405
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20451,
	'Special Region of Yogyakarta,Indonesia',
	'2360',
	'ID',
	'Province',
	'Special Region of Yogyakarta',
	'ID-YO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.7464
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20452,
	'Andaman and Nicobar Islands,India',
	'2356',
	'IN',
	'Union Territory',
	'Andaman and Nicobar Islands',
	'IN-AN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.7524
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20453,
	'Andhra Pradesh,India',
	'2356',
	'IN',
	'State',
	'Andhra Pradesh',
	'IN-AP',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.7586
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20454,
	'Assam,India',
	'2356',
	'IN',
	'State',
	'Assam',
	'IN-AS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.7646
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20455,
	'Bihar,India',
	'2356',
	'IN',
	'State',
	'Bihar',
	'IN-BR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.7715
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20457,
	'Gujarat,India',
	'2356',
	'IN',
	'State',
	'Gujarat',
	'IN-GJ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.7776
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20458,
	'Haryana,India',
	'2356',
	'IN',
	'State',
	'Haryana',
	'IN-HR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.7845
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20459,
	'Jammu and Kashmir,India',
	'2356',
	'IN',
	'State',
	'Jammu and Kashmir',
	'IN-JK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.7906
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20460,
	'Karnataka,India',
	'2356',
	'IN',
	'State',
	'Karnataka',
	'IN-KA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.7976
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20461,
	'Kerala,India',
	'2356',
	'IN',
	'State',
	'Kerala',
	'IN-KL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.8036
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20462,
	'Maharashtra,India',
	'2356',
	'IN',
	'State',
	'Maharashtra',
	'IN-MH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.8106
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20463,
	'Meghalaya,India',
	'2356',
	'IN',
	'State',
	'Meghalaya',
	'IN-ML',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.8166
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20464,
	'Madhya Pradesh,India',
	'2356',
	'IN',
	'State',
	'Madhya Pradesh',
	'IN-MP',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.8326
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20465,
	'Odisha,India',
	'2356',
	'IN',
	'State',
	'Odisha',
	'IN-OR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.8395
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20467,
	'Puducherry,India',
	'2356',
	'IN',
	'Union Territory',
	'Puducherry',
	'IN-PY',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.8456
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20468,
	'Rajasthan,India',
	'2356',
	'IN',
	'State',
	'Rajasthan',
	'IN-RJ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.8525
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20469,
	'Tamil Nadu,India',
	'2356',
	'IN',
	'State',
	'Tamil Nadu',
	'IN-TN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.8596
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20470,
	'Tripura,India',
	'2356',
	'IN',
	'State',
	'Tripura',
	'IN-TR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.8666
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20471,
	'Uttar Pradesh,India',
	'2356',
	'IN',
	'State',
	'Uttar Pradesh',
	'IN-UP',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.8736
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20472,
	'West Bengal,India',
	'2356',
	'IN',
	'State',
	'West Bengal',
	'IN-WB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.8808
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20514,
	'South District,Israel',
	'2376',
	'IL',
	'Region',
	'South District',
	'IL-D',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.8868
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20515,
	'Haifa District,Israel',
	'2376',
	'IL',
	'Region',
	'Haifa District',
	'IL-HA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.8928
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20516,
	'Jerusalem District,Israel',
	'2376',
	'IL',
	'Region',
	'Jerusalem District',
	'IL-JM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.8988
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20517,
	'Center District,Israel',
	'2376',
	'IL',
	'Region',
	'Center District',
	'IL-M',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.9069
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20518,
	'Tel Aviv District,Israel',
	'2376',
	'IL',
	'Region',
	'Tel Aviv District',
	'IL-TA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.9130
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20519,
	'North District,Israel',
	'2376',
	'IL',
	'Region',
	'North District',
	'IL-Z',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.9190
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20624,
	'Hokkaido,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Hokkaido',
	'JP-01',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.9260
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20625,
	'Aomori,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Aomori',
	'JP-02',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.9340
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20626,
	'Iwate,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Iwate',
	'JP-03',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.9400
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20627,
	'Miyagi,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Miyagi',
	'JP-04',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.9470
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20628,
	'Akita,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Akita',
	'JP-05',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.9539
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20629,
	'Yamagata,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Yamagata',
	'JP-06',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.9629
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20630,
	'Fukushima,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Fukushima',
	'JP-07',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.9699
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20631,
	'Ibaraki,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Ibaraki',
	'JP-08',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.9761
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20632,
	'Tochigi,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Tochigi',
	'JP-09',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.9851
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20633,
	'Gunma,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Gunma',
	'JP-10',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:01.9931
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20634,
	'Saitama,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Saitama',
	'JP-11',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.0001
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20635,
	'Chiba,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Chiba',
	'JP-12',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.0072
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20637,
	'Kanagawa,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Kanagawa',
	'JP-14',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.0132
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20638,
	'Niigata,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Niigata',
	'JP-15',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.0203
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20639,
	'Toyama,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Toyama',
	'JP-16',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.0294
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20640,
	'Ishikawa,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Ishikawa',
	'JP-17',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.0354
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20641,
	'Fukui,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Fukui',
	'JP-18',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.0424
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20642,
	'Yamanashi,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Yamanashi',
	'JP-19',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.0504
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20643,
	'Nagano,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Nagano',
	'JP-20',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.0563
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20644,
	'Gifu,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Gifu',
	'JP-21',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.0623
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20645,
	'Shizuoka,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Shizuoka',
	'JP-22',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.0683
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20646,
	'Aichi,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Aichi',
	'JP-23',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.0743
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20647,
	'Mie,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Mie',
	'JP-24',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.0803
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20648,
	'Shiga,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Shiga',
	'JP-25',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.0873
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20649,
	'Kyoto,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Kyoto',
	'JP-26',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.0942
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20650,
	'Osaka,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Osaka',
	'JP-27',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.1102
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20651,
	'Hyogo,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Hyogo',
	'JP-28',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.1162
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20652,
	'Nara,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Nara',
	'JP-29',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.1232
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20653,
	'Wakayama,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Wakayama',
	'JP-30',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.1301
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20654,
	'Tottori,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Tottori',
	'JP-31',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.1361
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20655,
	'Shimane,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Shimane',
	'JP-32',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.1431
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20656,
	'Okayama,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Okayama',
	'JP-33',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.1491
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20657,
	'Hiroshima,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Hiroshima',
	'JP-34',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.1551
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20658,
	'Yamaguchi,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Yamaguchi',
	'JP-35',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.1611
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20659,
	'Tokushima,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Tokushima',
	'JP-36',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.1670
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20660,
	'Kagawa,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Kagawa',
	'JP-37',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.1721
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20661,
	'Ehime,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Ehime',
	'JP-38',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.1780
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20662,
	'Kochi,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Kochi',
	'JP-39',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.1850
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20663,
	'Fukuoka,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Fukuoka',
	'JP-40',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.1910
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20664,
	'Saga,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Saga',
	'JP-41',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.1970
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20665,
	'Nagasaki,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Nagasaki',
	'JP-42',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.2029
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20666,
	'Kumamoto,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Kumamoto',
	'JP-43',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.2079
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20667,
	'Oita,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Oita',
	'JP-44',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.2139
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20668,
	'Miyazaki,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Miyazaki',
	'JP-45',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.2199
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20669,
	'Kagoshima,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Kagoshima',
	'JP-46',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.2259
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20670,
	'Okinawa,Japan',
	'2392',
	'JP',
	'Prefecture',
	'Okinawa',
	'JP-47',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.2319
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20696,
	'Baja California,Mexico',
	'2484',
	'MX',
	'State',
	'Baja California',
	'MX-BCN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.2379
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20697,
	'Baja California Sur,Mexico',
	'2484',
	'MX',
	'State',
	'Baja California Sur',
	'MX-BCS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.2438
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20699,
	'Chihuahua,Mexico',
	'2484',
	'MX',
	'State',
	'Chihuahua',
	'MX-CHH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.2498
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20700,
	'Chiapas,Mexico',
	'2484',
	'MX',
	'State',
	'Chiapas',
	'MX-CHP',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.2578
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20701,
	'Coahuila,Mexico',
	'2484',
	'MX',
	'State',
	'Coahuila',
	'MX-COA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.2648
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20702,
	'Colima,Mexico',
	'2484',
	'MX',
	'State',
	'Colima',
	'MX-COL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.2708
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20703,
	'Mexico City,Mexico',
	'2484',
	'MX',
	'State',
	'Mexico City',
	'MX-DIF',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.2769
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20705,
	'Guerrero,Mexico',
	'2484',
	'MX',
	'State',
	'Guerrero',
	'MX-GRO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.2829
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20707,
	'Hidalgo,Mexico',
	'2484',
	'MX',
	'State',
	'Hidalgo',
	'MX-HID',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.2898
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20708,
	'Jalisco,Mexico',
	'2484',
	'MX',
	'State',
	'Jalisco',
	'MX-JAL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.2958
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20709,
	'State of Mexico,Mexico',
	'2484',
	'MX',
	'State',
	'State of Mexico',
	'MX-MEX',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.3028
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20711,
	'Morelos,Mexico',
	'2484',
	'MX',
	'State',
	'Morelos',
	'MX-MOR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.3099
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20712,
	'Nayarit,Mexico',
	'2484',
	'MX',
	'State',
	'Nayarit',
	'MX-NAY',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.3159
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20713,
	'Nuevo Leon,Mexico',
	'2484',
	'MX',
	'State',
	'Nuevo Leon',
	'MX-NLE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.3229
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20717,
	'Quintana Roo,Mexico',
	'2484',
	'MX',
	'State',
	'Quintana Roo',
	'MX-ROO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.3290
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20718,
	'Sinaloa,Mexico',
	'2484',
	'MX',
	'State',
	'Sinaloa',
	'MX-SIN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.3350
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20720,
	'Sonora,Mexico',
	'2484',
	'MX',
	'State',
	'Sonora',
	'MX-SON',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.3410
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20721,
	'Tabasco,Mexico',
	'2484',
	'MX',
	'State',
	'Tabasco',
	'MX-TAB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.3470
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20722,
	'Tamaulipas,Mexico',
	'2484',
	'MX',
	'State',
	'Tamaulipas',
	'MX-TAM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.3530
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20723,
	'Tlaxcala,Mexico',
	'2484',
	'MX',
	'State',
	'Tlaxcala',
	'MX-TLA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.3600
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20725,
	'Yucatan,Mexico',
	'2484',
	'MX',
	'State',
	'Yucatan',
	'MX-YUC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.3650
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20737,
	'Perak,Malaysia',
	'2458',
	'MY',
	'State',
	'Perak',
	'MY-08',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.3729
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20738,
	'Selangor,Malaysia',
	'2458',
	'MY',
	'State',
	'Selangor',
	'MY-10',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.3789
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20739,
	'Pahang,Malaysia',
	'2458',
	'MY',
	'State',
	'Pahang',
	'MY-06',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.3859
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20740,
	'Kelantan,Malaysia',
	'2458',
	'MY',
	'State',
	'Kelantan',
	'MY-03',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.3929
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20741,
	'Johor,Malaysia',
	'2458',
	'MY',
	'State',
	'Johor',
	'MY-01',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.3989
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20742,
	'Kedah,Malaysia',
	'2458',
	'MY',
	'State',
	'Kedah',
	'MY-02',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.4060
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20743,
	'Labuan Federal Territory,Malaysia',
	'2458',
	'MY',
	'State',
	'Labuan Federal Territory',
	'MY-15',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.4110
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20745,
	'Negeri Sembilan,Malaysia',
	'2458',
	'MY',
	'State',
	'Negeri Sembilan',
	'MY-05',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.4170
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20746,
	'Penang,Malaysia',
	'2458',
	'MY',
	'State',
	'Penang',
	'MY-07',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.4240
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20747,
	'Perlis,Malaysia',
	'2458',
	'MY',
	'State',
	'Perlis',
	'MY-09',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.4310
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20748,
	'Sabah,Malaysia',
	'2458',
	'MY',
	'State',
	'Sabah',
	'MY-12',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.4379
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20749,
	'Sarawak,Malaysia',
	'2458',
	'MY',
	'State',
	'Sarawak',
	'MY-13',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.4439
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20750,
	'Terengganu,Malaysia',
	'2458',
	'MY',
	'State',
	'Terengganu',
	'MY-11',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.4499
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20751,
	'Federal Territory of Kuala Lumpur,Malaysia',
	'2458',
	'MY',
	'State',
	'Federal Territory of Kuala Lumpur',
	'MY-14',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.4559
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20759,
	'Drenthe,Netherlands',
	'2528',
	'NL',
	'Province',
	'Drenthe',
	'NL-DR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.4619
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20760,
	'Flevoland,Netherlands',
	'2528',
	'NL',
	'Province',
	'Flevoland',
	'NL-FL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.4690
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20761,
	'Friesland,Netherlands',
	'2528',
	'NL',
	'Province',
	'Friesland',
	'NL-FR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.4770
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20762,
	'Gelderland,Netherlands',
	'2528',
	'NL',
	'Province',
	'Gelderland',
	'NL-GE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.4831
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20765,
	'North Brabant,Netherlands',
	'2528',
	'NL',
	'Province',
	'North Brabant',
	'NL-NB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.4891
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20766,
	'North Holland,Netherlands',
	'2528',
	'NL',
	'Province',
	'North Holland',
	'NL-NH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.4951
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20767,
	'Overijssel,Netherlands',
	'2528',
	'NL',
	'Province',
	'Overijssel',
	'NL-OV',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.5011
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20771,
	'Ostfold,Norway',
	'2578',
	'NO',
	'County',
	'Ostfold',
	'NO-01',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.5072
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20772,
	'Akershus,Norway',
	'2578',
	'NO',
	'County',
	'Akershus',
	'NO-02',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.5132
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20774,
	'Hedmark,Norway',
	'2578',
	'NO',
	'County',
	'Hedmark',
	'NO-04',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.5191
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20775,
	'Oppland,Norway',
	'2578',
	'NO',
	'County',
	'Oppland',
	'NO-05',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.5251
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20776,
	'Buskerud,Norway',
	'2578',
	'NO',
	'County',
	'Buskerud',
	'NO-06',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.5312
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20777,
	'Vestfold,Norway',
	'2578',
	'NO',
	'County',
	'Vestfold',
	'NO-07',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.5373
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20778,
	'Telemark,Norway',
	'2578',
	'NO',
	'County',
	'Telemark',
	'NO-08',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.5433
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20779,
	'Aust-Agder,Norway',
	'2578',
	'NO',
	'County',
	'Aust-Agder',
	'NO-09',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.5483
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20780,
	'Vest-Agder,Norway',
	'2578',
	'NO',
	'County',
	'Vest-Agder',
	'NO-10',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.5554
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20781,
	'Rogaland,Norway',
	'2578',
	'NO',
	'County',
	'Rogaland',
	'NO-11',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.5617
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20782,
	'Hordaland,Norway',
	'2578',
	'NO',
	'County',
	'Hordaland',
	'NO-12',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.5687
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20783,
	'Sogn og Fjordane,Norway',
	'2578',
	'NO',
	'County',
	'Sogn og Fjordane',
	'NO-14',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.5766
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20785,
	'Sor-Trondelag,Norway',
	'2578',
	'NO',
	'County',
	'Sor-Trondelag',
	'NO-16',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.5826
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20786,
	'Nord-Trondelag,Norway',
	'2578',
	'NO',
	'County',
	'Nord-Trondelag',
	'NO-17',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.5896
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20787,
	'Nordland,Norway',
	'2578',
	'NO',
	'County',
	'Nordland',
	'NO-18',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.5966
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20788,
	'Troms,Norway',
	'2578',
	'NO',
	'County',
	'Troms',
	'NO-19',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.6036
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20789,
	'Finnmark,Norway',
	'2578',
	'NO',
	'County',
	'Finnmark',
	'NO-20',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.6105
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20792,
	'Bay Of Plenty,New Zealand',
	'2554',
	'NZ',
	'Region',
	'Bay Of Plenty',
	'NZ-BOP',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.6176
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20796,
	'Ancash,Peru',
	'2604',
	'PE',
	'Region',
	'Ancash',
	'PE-ANC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.6235
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20803,
	'Huancavelica,Peru',
	'2604',
	'PE',
	'Region',
	'Huancavelica',
	'PE-HUV',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.6305
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20804,
	'Ica,Peru',
	'2604',
	'PE',
	'Region',
	'Ica',
	'PE-ICA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.6375
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20805,
	'Junin,Peru',
	'2604',
	'PE',
	'Region',
	'Junin',
	'PE-JUN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.6445
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20806,
	'La Libertad,Peru',
	'2604',
	'PE',
	'Region',
	'La Libertad',
	'PE-LAL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.6514
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20807,
	'Lambayeque,Peru',
	'2604',
	'PE',
	'Region',
	'Lambayeque',
	'PE-LAM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.6574
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20808,
	'Lima Region,Peru',
	'2604',
	'PE',
	'Region',
	'Lima Region',
	'PE-LIM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.6634
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20810,
	'Moquegua,Peru',
	'2604',
	'PE',
	'Region',
	'Moquegua',
	'PE-MOQ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.6694
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20815,
	'Tacna,Peru',
	'2604',
	'PE',
	'Region',
	'Tacna',
	'PE-TAC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.6754
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20816,
	'Ucayali,Peru',
	'2604',
	'PE',
	'Region',
	'Ucayali',
	'PE-UCA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.6814
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20835,
	'Metro Manila,Philippines',
	'2608',
	'PH',
	'Region',
	'Metro Manila',
	'PH-00',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.6883
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20847,
	'Lower Silesian Voivodeship,Poland',
	'2616',
	'PL',
	'Region',
	'Lower Silesian Voivodeship',
	'PL-DS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.6943
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20848,
	'Kuyavian-Pomeranian Voivodeship,Poland',
	'2616',
	'PL',
	'Region',
	'Kuyavian-Pomeranian Voivodeship',
	'PL-KP',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.7013
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20849,
	'Lubusz Voivodeship,Poland',
	'2616',
	'PL',
	'Region',
	'Lubusz Voivodeship',
	'PL-LB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.7073
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20851,
	'Lublin Voivodeship,Poland',
	'2616',
	'PL',
	'Region',
	'Lublin Voivodeship',
	'PL-LU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.7143
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20852,
	'Lesser Poland Voivodeship,Poland',
	'2616',
	'PL',
	'Region',
	'Lesser Poland Voivodeship',
	'PL-MA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.7212
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20853,
	'Masovian Voivodeship,Poland',
	'2616',
	'PL',
	'Region',
	'Masovian Voivodeship',
	'PL-MZ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.7282
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20854,
	'Opole Voivodeship,Poland',
	'2616',
	'PL',
	'Region',
	'Opole Voivodeship',
	'PL-OP',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.7352
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20855,
	'Podlaskie Voivodeship,Poland',
	'2616',
	'PL',
	'Region',
	'Podlaskie Voivodeship',
	'PL-PD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.7402
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20856,
	'Podkarpackie Voivodeship,Poland',
	'2616',
	'PL',
	'Region',
	'Podkarpackie Voivodeship',
	'PL-PK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.7482
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20857,
	'Pomeranian Voivodeship,Poland',
	'2616',
	'PL',
	'Region',
	'Pomeranian Voivodeship',
	'PL-PM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.7542
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20858,
	'Swietokrzyskie,Poland',
	'2616',
	'PL',
	'Region',
	'Swietokrzyskie',
	'PL-SK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.7601
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20859,
	'Silesian Voivodeship,Poland',
	'2616',
	'PL',
	'Region',
	'Silesian Voivodeship',
	'PL-SL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.7671
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20860,
	'Warmian-Masurian Voivodeship,Poland',
	'2616',
	'PL',
	'Region',
	'Warmian-Masurian Voivodeship',
	'PL-WN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.7741
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20861,
	'Greater Poland Voivodeship,Poland',
	'2616',
	'PL',
	'Region',
	'Greater Poland Voivodeship',
	'PL-WP',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.7811
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20862,
	'West Pomeranian Voivodeship,Poland',
	'2616',
	'PL',
	'Region',
	'West Pomeranian Voivodeship',
	'PL-ZP',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.7891
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20865,
	'Aveiro District,Portugal',
	'2620',
	'PT',
	'Region',
	'Aveiro District',
	'PT-01',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.7960
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20868,
	'Castelo Branco District,Portugal',
	'2620',
	'PT',
	'Region',
	'Castelo Branco District',
	'PT-05',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.8030
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20869,
	'Coimbra District,Portugal',
	'2620',
	'PT',
	'Region',
	'Coimbra District',
	'PT-06',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.8100
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20871,
	'Faro District,Portugal',
	'2620',
	'PT',
	'Region',
	'Faro District',
	'PT-08',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.8172
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20872,
	'Guarda District,Portugal',
	'2620',
	'PT',
	'Region',
	'Guarda District',
	'PT-09',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.8242
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20873,
	'Leiria District,Portugal',
	'2620',
	'PT',
	'Region',
	'Leiria District',
	'PT-10',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.8301
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20875,
	'Portalegre District,Portugal',
	'2620',
	'PT',
	'Region',
	'Portalegre District',
	'PT-12',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.8372
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20876,
	'Porto District,Portugal',
	'2620',
	'PT',
	'Region',
	'Porto District',
	'PT-13',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.8432
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20878,
	'Setubal,Portugal',
	'2620',
	'PT',
	'Region',
	'Setubal',
	'PT-15',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.8481
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20879,
	'Viana do Castelo District,Portugal',
	'2620',
	'PT',
	'Region',
	'Viana do Castelo District',
	'PT-16',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.8551
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20880,
	'Viseu District,Portugal',
	'2620',
	'PT',
	'Region',
	'Viseu District',
	'PT-18',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.8611
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20881,
	'Azores,Portugal',
	'2620',
	'PT',
	'Region',
	'Azores',
	'PT-20',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.8671
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20888,
	'Arad County,Romania',
	'2642',
	'RO',
	'County',
	'Arad County',
	'RO-AR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.8741
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20891,
	'Bihor County,Romania',
	'2642',
	'RO',
	'County',
	'Bihor County',
	'RO-BH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.8791
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20897,
	'Cluj County,Romania',
	'2642',
	'RO',
	'County',
	'Cluj County',
	'RO-CJ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.8862
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20900,
	'Dolj County,Romania',
	'2642',
	'RO',
	'County',
	'Dolj County',
	'RO-DJ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.8912
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20902,
	'Giurgiu County,Romania',
	'2642',
	'RO',
	'County',
	'Giurgiu County',
	'RO-GR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.8982
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20907,
	'Prahova,Romania',
	'2642',
	'RO',
	'County',
	'Prahova',
	'RO-PH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.9051
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20908,
	'Sibiu,Romania',
	'2642',
	'RO',
	'County',
	'Sibiu',
	'RO-SB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.9111
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20909,
	'Satu Mare County,Romania',
	'2642',
	'RO',
	'County',
	'Satu Mare County',
	'RO-SM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.9173
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20910,
	'Suceava County,Romania',
	'2642',
	'RO',
	'County',
	'Suceava County',
	'RO-SV',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.9234
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20911,
	'Tulcea County,Romania',
	'2642',
	'RO',
	'County',
	'Tulcea County',
	'RO-TL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.9295
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20913,
	'Vrancea County,Romania',
	'2642',
	'RO',
	'County',
	'Vrancea County',
	'RO-VN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.9355
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20914,
	'Altai Republic,Russia',
	'2643',
	'RU',
	'Region',
	'Altai Republic',
	'RU-AL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.9425
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20915,
	'Amur Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Amur Oblast',
	'RU-AMU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.9494
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20916,
	'Arkhangelsk Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Arkhangelsk Oblast',
	'RU-ARK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.9555
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20917,
	'Astrakhan Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Astrakhan Oblast',
	'RU-AST',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.9624
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20918,
	'Republic of Bashkortostan,Russia',
	'2643',
	'RU',
	'Region',
	'Republic of Bashkortostan',
	'RU-BA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.9684
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20919,
	'Belgorod Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Belgorod Oblast',
	'RU-BEL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.9745
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20920,
	'Bryansk Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Bryansk Oblast',
	'RU-BRY',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.9804
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20921,
	'Buryatia,Russia',
	'2643',
	'RU',
	'Region',
	'Buryatia',
	'RU-BU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.9865
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20922,
	'Chelyabinsk Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Chelyabinsk Oblast',
	'RU-CHE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.9914
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20924,
	'Chukotka Autonomous Okrug,Russia',
	'2643',
	'RU',
	'Region',
	'Chukotka Autonomous Okrug',
	'RU-CHU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:02.9974
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20925,
	'Dagestan Republic,Russia',
	'2643',
	'RU',
	'Region',
	'Dagestan Republic',
	'RU-DA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.0034
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20926,
	'Irkutsk Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Irkutsk Oblast',
	'RU-IRK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.0095
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20927,
	'Ivanovo Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Ivanovo Oblast',
	'RU-IVA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.0165
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20928,
	'Kamchatka Krai,Russia',
	'2643',
	'RU',
	'Region',
	'Kamchatka Krai',
	'RU-KAM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.0225
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20929,
	'Kabardino-Balkaria,Russia',
	'2643',
	'RU',
	'Region',
	'Kabardino-Balkaria',
	'RU-KB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.0295
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20930,
	'Karachay-Cherkessia,Russia',
	'2643',
	'RU',
	'Region',
	'Karachay-Cherkessia',
	'RU-KC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.0355
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20931,
	'Krasnodar Krai,Russia',
	'2643',
	'RU',
	'Region',
	'Krasnodar Krai',
	'RU-KDA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.0414
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20932,
	'Kemerovo Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Kemerovo Oblast',
	'RU-KEM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.0474
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20933,
	'Kaliningrad Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Kaliningrad Oblast',
	'RU-KGD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.0534
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20934,
	'Kurgan Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Kurgan Oblast',
	'RU-KGN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.0584
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20935,
	'Khabarovsk Krai,Russia',
	'2643',
	'RU',
	'Region',
	'Khabarovsk Krai',
	'RU-KHA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.0654
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20936,
	'Khanty-Mansi Autonomous Okrug,Russia',
	'2643',
	'RU',
	'Region',
	'Khanty-Mansi Autonomous Okrug',
	'RU-KHM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.0725
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20937,
	'Kirov Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Kirov Oblast',
	'RU-KIR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.0785
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20938,
	'Republic of Khakassia,Russia',
	'2643',
	'RU',
	'Region',
	'Republic of Khakassia',
	'RU-KK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.0855
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20939,
	'Kaluga Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Kaluga Oblast',
	'RU-KLU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.0905
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20940,
	'Komi Republic,Russia',
	'2643',
	'RU',
	'Region',
	'Komi Republic',
	'RU-KO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.0965
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20941,
	'Kostroma Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Kostroma Oblast',
	'RU-KOS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.1025
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20942,
	'Republic of Karelia,Russia',
	'2643',
	'RU',
	'Region',
	'Republic of Karelia',
	'RU-KR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.1095
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20943,
	'Kursk Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Kursk Oblast',
	'RU-KRS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.1154
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20944,
	'Krasnoyarsk Krai,Russia',
	'2643',
	'RU',
	'Region',
	'Krasnoyarsk Krai',
	'RU-KYA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.1216
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20945,
	'Leningrad Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Leningrad Oblast',
	'RU-LEN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.1287
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20946,
	'Lipetsk Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Lipetsk Oblast',
	'RU-LIP',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.1357
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20947,
	'Magadan Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Magadan Oblast',
	'RU-MAG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.1418
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20948,
	'Mordovia,Russia',
	'2643',
	'RU',
	'Region',
	'Mordovia',
	'RU-MO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.1479
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20949,
	'Moscow Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Moscow Oblast',
	'RU-MOS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.1540
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20951,
	'Murmansk Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Murmansk Oblast',
	'RU-MUR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.1610
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20952,
	'Novgorod Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Novgorod Oblast',
	'RU-NGR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.1671
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20953,
	'Nizhny Novgorod Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Nizhny Novgorod Oblast',
	'RU-NIZ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.1722
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20954,
	'Novosibirsk Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Novosibirsk Oblast',
	'RU-NVS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.1792
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20955,
	'Omsk Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Omsk Oblast',
	'RU-OMS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.1872
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20956,
	'Orenburg Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Orenburg Oblast',
	'RU-ORE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.1932
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20957,
	'Perm Krai,Russia',
	'2643',
	'RU',
	'Region',
	'Perm Krai',
	'RU-PER',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.2002
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20958,
	'Penza Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Penza Oblast',
	'RU-PNZ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.2062
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20959,
	'Primorsky Krai,Russia',
	'2643',
	'RU',
	'Region',
	'Primorsky Krai',
	'RU-PRI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.2123
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20960,
	'Pskov Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Pskov Oblast',
	'RU-PSK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.2194
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20961,
	'Rostov Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Rostov Oblast',
	'RU-ROS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.2254
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20962,
	'Ryazan Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Ryazan Oblast',
	'RU-RYA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.2326
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20963,
	'Sakha Republic,Russia',
	'2643',
	'RU',
	'Region',
	'Sakha Republic',
	'RU-SA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.2396
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20964,
	'Sakhalin Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Sakhalin Oblast',
	'RU-SAK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.2466
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20965,
	'Samara Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Samara Oblast',
	'RU-SAM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.2527
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20966,
	'Saratov Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Saratov Oblast',
	'RU-SAR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.2588
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20967,
	'Smolensk Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Smolensk Oblast',
	'RU-SMO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.2648
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20969,
	'Stavropol Krai,Russia',
	'2643',
	'RU',
	'Region',
	'Stavropol Krai',
	'RU-STA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.2708
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20970,
	'Sverdlovsk Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Sverdlovsk Oblast',
	'RU-SVE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.2778
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20971,
	'Tatarstan,Russia',
	'2643',
	'RU',
	'Region',
	'Tatarstan',
	'RU-TA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.2869
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20972,
	'Tambov Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Tambov Oblast',
	'RU-TAM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.2929
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20974,
	'Tomsk Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Tomsk Oblast',
	'RU-TOM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.3029
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20975,
	'Tula Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Tula Oblast',
	'RU-TUL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.3089
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20976,
	'Tver Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Tver Oblast',
	'RU-TVE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.3179
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20977,
	'Tyumen Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Tyumen Oblast',
	'RU-TYU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.3259
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20978,
	'Udmurt Republic,Russia',
	'2643',
	'RU',
	'Region',
	'Udmurt Republic',
	'RU-UD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.3319
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20979,
	'Ulyanovsk Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Ulyanovsk Oblast',
	'RU-ULY',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.3378
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20980,
	'Volgograd Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Volgograd Oblast',
	'RU-VGG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.3438
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20981,
	'Vladimir Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Vladimir Oblast',
	'RU-VLA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.3498
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20982,
	'Vologda Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Vologda Oblast',
	'RU-VLG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.3568
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20983,
	'Voronezh Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Voronezh Oblast',
	'RU-VOR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.3648
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20984,
	'Yamalo-Nenets Autonomous Okrug,Russia',
	'2643',
	'RU',
	'Region',
	'Yamalo-Nenets Autonomous Okrug',
	'RU-YAN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.3708
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20985,
	'Yaroslavl Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Yaroslavl Oblast',
	'RU-YAR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.3767
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20987,
	'Riyadh Province,Saudi Arabia',
	'2682',
	'SA',
	'Province',
	'Riyadh Province',
	'SA-01',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.3837
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20988,
	'Makkah Province,Saudi Arabia',
	'2682',
	'SA',
	'Province',
	'Makkah Province',
	'SA-02',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.3897
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	20989,
	'Eastern Province,Saudi Arabia',
	'2682',
	'SA',
	'Province',
	'Eastern Province',
	'LK-5',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.3957
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21000,
	'Stockholm County,Sweden',
	'2752',
	'SE',
	'County',
	'Stockholm County',
	'SE-AB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.4027
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21002,
	'Norrbotten County,Sweden',
	'2752',
	'SE',
	'County',
	'Norrbotten County',
	'SE-BD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.4087
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21003,
	'Uppsala County,Sweden',
	'2752',
	'SE',
	'County',
	'Uppsala County',
	'SE-C',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.4146
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21006,
	'Jonkoping County,Sweden',
	'2752',
	'SE',
	'County',
	'Jonkoping County',
	'SE-F',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.4206
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21007,
	'Kronoberg County,Sweden',
	'2752',
	'SE',
	'County',
	'Kronoberg County',
	'SE-G',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.4256
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21008,
	'Kalmar County,Sweden',
	'2752',
	'SE',
	'County',
	'Kalmar County',
	'SE-H',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.4306
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21009,
	'Gotland County,Sweden',
	'2752',
	'SE',
	'County',
	'Gotland County',
	'SE-I',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.4366
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21010,
	'Blekinge County,Sweden',
	'2752',
	'SE',
	'County',
	'Blekinge County',
	'SE-K',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.4436
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21012,
	'Halland County,Sweden',
	'2752',
	'SE',
	'County',
	'Halland County',
	'SE-N',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.4485
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21014,
	'Varmland County,Sweden',
	'2752',
	'SE',
	'County',
	'Varmland County',
	'SE-S',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.4545
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21017,
	'Dalarna County,Sweden',
	'2752',
	'SE',
	'County',
	'Dalarna County',
	'SE-W',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.4605
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21018,
	'Gavleborg County,Sweden',
	'2752',
	'SE',
	'County',
	'Gavleborg County',
	'SE-X',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.4665
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21020,
	'Jamtland County,Sweden',
	'2752',
	'SE',
	'County',
	'Jamtland County',
	'SE-Z',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.4715
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21027,
	'Pathum Thani,Thailand',
	'2764',
	'TH',
	'Province',
	'Pathum Thani',
	'TH-13',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.4765
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21029,
	'Saraburi,Thailand',
	'2764',
	'TH',
	'Province',
	'Saraburi',
	'TH-19',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.4865
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21030,
	'Chon Buri,Thailand',
	'2764',
	'TH',
	'Province',
	'Chon Buri',
	'TH-20',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.4935
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21031,
	'Chachoengsao,Thailand',
	'2764',
	'TH',
	'Province',
	'Chachoengsao',
	'TH-24',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.4994
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21034,
	'Yasothon,Thailand',
	'2764',
	'TH',
	'Province',
	'Yasothon',
	'TH-35',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.5055
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21035,
	'Khon Kaen,Thailand',
	'2764',
	'TH',
	'Province',
	'Khon Kaen',
	'TH-40',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.5115
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21038,
	'Phrae,Thailand',
	'2764',
	'TH',
	'Province',
	'Phrae',
	'TH-54',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.5186
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21039,
	'Chiang Rai,Thailand',
	'2764',
	'TH',
	'Province',
	'Chiang Rai',
	'TH-57',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.5248
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21041,
	'Nakhon Si Thammarat,Thailand',
	'2764',
	'TH',
	'Province',
	'Nakhon Si Thammarat',
	'TH-80',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.5318
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21043,
	'Ranong,Thailand',
	'2764',
	'TH',
	'Province',
	'Ranong',
	'TH-85',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.5389
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21046,
	'Pattani,Thailand',
	'2764',
	'TH',
	'Province',
	'Pattani',
	'TH-94',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.5450
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21053,
	'Afyonkarahisar Province,Turkey',
	'2792',
	'TR',
	'Province',
	'Afyonkarahisar Province',
	'TR-03',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.5510
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21056,
	'Antalya,Turkey',
	'2792',
	'TR',
	'Province',
	'Antalya',
	'TR-07',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.5569
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21059,
	'Bolu,Turkey',
	'2792',
	'TR',
	'Province',
	'Bolu',
	'TR-14',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.5631
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21066,
	'Gaziantep,Turkey',
	'2792',
	'TR',
	'Province',
	'Gaziantep',
	'TR-27',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.5691
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21067,
	'Hatay,Turkey',
	'2792',
	'TR',
	'Province',
	'Hatay',
	'TR-31',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.5751
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21068,
	'Mersin Province,Turkey',
	'2792',
	'TR',
	'Province',
	'Mersin Province',
	'TR-33',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.5842
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21072,
	'Kastamonu,Turkey',
	'2792',
	'TR',
	'Province',
	'Kastamonu',
	'TR-37',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.5922
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21073,
	'Kayseri Province,Turkey',
	'2792',
	'TR',
	'Province',
	'Kayseri Province',
	'TR-38',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.5982
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21074,
	'Kocaeli,Turkey',
	'2792',
	'TR',
	'Province',
	'Kocaeli',
	'TR-41',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.6052
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21075,
	'Konya,Turkey',
	'2792',
	'TR',
	'Province',
	'Konya',
	'TR-42',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.6112
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21078,
	'Manisa,Turkey',
	'2792',
	'TR',
	'Province',
	'Manisa',
	'TR-45',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.6172
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21085,
	'Samsun,Turkey',
	'2792',
	'TR',
	'Province',
	'Samsun',
	'TR-55',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.6231
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21094,
	'Yalova Province,Turkey',
	'2792',
	'TR',
	'Province',
	'Yalova Province',
	'TR-77',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.6301
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21102,
	'Taoyuan City,Taiwan',
	'2158',
	'TW',
	'County',
	'Taoyuan City',
	'TW-TAO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.6351
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21112,
	'Dnipropetrovsk Oblast,Ukraine',
	'2804',
	'UA',
	'Region',
	'Dnipropetrovsk Oblast',
	'UA-12',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.6411
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21113,
	'Donetsk Oblast,Ukraine',
	'2804',
	'UA',
	'Region',
	'Donetsk Oblast',
	'UA-14',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.6471
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21118,
	'Kyiv city,Ukraine',
	'2804',
	'UA',
	'Region',
	'Kyiv city',
	'UA-30',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.6541
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21121,
	'Lviv Oblast,Ukraine',
	'2804',
	'UA',
	'Region',
	'Lviv Oblast',
	'UA-46',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.6591
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21122,
	'Odessa Oblast,Ukraine',
	'2804',
	'UA',
	'Region',
	'Odessa Oblast',
	'UA-51',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.6662
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21125,
	'Kharkiv Oblast,Ukraine',
	'2804',
	'UA',
	'Region',
	'Kharkiv Oblast',
	'UA-63',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.6722
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21132,
	'Alaska,United States',
	'2840',
	'US',
	'State',
	'Alaska',
	'US-AK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.6782
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21133,
	'Alabama,United States',
	'2840',
	'US',
	'State',
	'Alabama',
	'US-AL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.6832
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21135,
	'Arkansas,United States',
	'2840',
	'US',
	'State',
	'Arkansas',
	'US-AR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.6903
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21136,
	'Arizona,United States',
	'2840',
	'US',
	'State',
	'Arizona',
	'US-AZ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.6965
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21138,
	'Colorado,United States',
	'2840',
	'US',
	'State',
	'Colorado',
	'US-CO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.7035
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21139,
	'Connecticut,United States',
	'2840',
	'US',
	'State',
	'Connecticut',
	'US-CT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.7096
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21140,
	'District of Columbia,United States',
	'2840',
	'US',
	'State',
	'District of Columbia',
	'US-DC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.7155
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21144,
	'Hawaii,United States',
	'2840',
	'US',
	'State',
	'Hawaii',
	'US-HI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.7226
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21145,
	'Iowa,United States',
	'2840',
	'US',
	'State',
	'Iowa',
	'US-IA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.7276
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21146,
	'Idaho,United States',
	'2840',
	'US',
	'State',
	'Idaho',
	'US-ID',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.7346
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21147,
	'Illinois,United States',
	'2840',
	'US',
	'State',
	'Illinois',
	'US-IL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.7406
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21148,
	'Indiana,United States',
	'2840',
	'US',
	'State',
	'Indiana',
	'US-IN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.7477
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21150,
	'Kentucky,United States',
	'2840',
	'US',
	'State',
	'Kentucky',
	'US-KY',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.7537
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21151,
	'Louisiana,United States',
	'2840',
	'US',
	'State',
	'Louisiana',
	'US-LA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.7597
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21152,
	'Massachusetts,United States',
	'2840',
	'US',
	'State',
	'Massachusetts',
	'US-MA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.7656
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21153,
	'Maryland,United States',
	'2840',
	'US',
	'State',
	'Maryland',
	'US-MD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.7726
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21154,
	'Maine,United States',
	'2840',
	'US',
	'State',
	'Maine',
	'US-ME',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.7797
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21155,
	'Michigan,United States',
	'2840',
	'US',
	'State',
	'Michigan',
	'US-MI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.7857
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21156,
	'Minnesota,United States',
	'2840',
	'US',
	'State',
	'Minnesota',
	'US-MN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.7927
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21157,
	'Missouri,United States',
	'2840',
	'US',
	'State',
	'Missouri',
	'US-MO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.7988
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21158,
	'Mississippi,United States',
	'2840',
	'US',
	'State',
	'Mississippi',
	'US-MS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.8037
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21160,
	'North Carolina,United States',
	'2840',
	'US',
	'State',
	'North Carolina',
	'US-NC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.8097
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21161,
	'North Dakota,United States',
	'2840',
	'US',
	'State',
	'North Dakota',
	'US-ND',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.8167
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21162,
	'Nebraska,United States',
	'2840',
	'US',
	'State',
	'Nebraska',
	'US-NE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.8237
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21163,
	'New Hampshire,United States',
	'2840',
	'US',
	'State',
	'New Hampshire',
	'US-NH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.8297
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21164,
	'New Jersey,United States',
	'2840',
	'US',
	'State',
	'New Jersey',
	'US-NJ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.8377
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21165,
	'New Mexico,United States',
	'2840',
	'US',
	'State',
	'New Mexico',
	'US-NM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.8436
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21168,
	'Ohio,United States',
	'2840',
	'US',
	'State',
	'Ohio',
	'US-OH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.8496
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21169,
	'Oklahoma,United States',
	'2840',
	'US',
	'State',
	'Oklahoma',
	'US-OK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.8556
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21170,
	'Oregon,United States',
	'2840',
	'US',
	'State',
	'Oregon',
	'US-OR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.8626
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21171,
	'Pennsylvania,United States',
	'2840',
	'US',
	'State',
	'Pennsylvania',
	'US-PA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.8696
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21172,
	'Rhode Island,United States',
	'2840',
	'US',
	'State',
	'Rhode Island',
	'US-RI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.8765
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21173,
	'South Carolina,United States',
	'2840',
	'US',
	'State',
	'South Carolina',
	'US-SC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.8855
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21174,
	'South Dakota,United States',
	'2840',
	'US',
	'State',
	'South Dakota',
	'US-SD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.8925
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21175,
	'Tennessee,United States',
	'2840',
	'US',
	'State',
	'Tennessee',
	'US-TN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.8985
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21176,
	'Texas,United States',
	'2840',
	'US',
	'State',
	'Texas',
	'US-TX',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.9045
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21177,
	'Utah,United States',
	'2840',
	'US',
	'State',
	'Utah',
	'US-UT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.9115
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21179,
	'Vermont,United States',
	'2840',
	'US',
	'State',
	'Vermont',
	'US-VT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.9174
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21182,
	'Wisconsin,United States',
	'2840',
	'US',
	'State',
	'Wisconsin',
	'US-WI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.9244
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21183,
	'West Virginia,United States',
	'2840',
	'US',
	'State',
	'West Virginia',
	'US-WV',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.9314
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21188,
	'Capital District,Venezuela',
	'2862',
	'VE',
	'Province',
	'Capital District',
	'VE-A',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.9384
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21189,
	'Anzoategui,Venezuela',
	'2862',
	'VE',
	'Province',
	'Anzoategui',
	'VE-B',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.9444
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21190,
	'Apure,Venezuela',
	'2862',
	'VE',
	'Province',
	'Apure',
	'VE-C',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.9503
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21191,
	'Aragua,Venezuela',
	'2862',
	'VE',
	'Province',
	'Aragua',
	'VE-D',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.9563
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21194,
	'Carabobo,Venezuela',
	'2862',
	'VE',
	'Province',
	'Carabobo',
	'VE-G',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.9633
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21197,
	'Lara,Venezuela',
	'2862',
	'VE',
	'Province',
	'Lara',
	'VE-K',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.9693
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21199,
	'Miranda,Venezuela',
	'2862',
	'VE',
	'Province',
	'Miranda',
	'VE-M',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.9753
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21200,
	'Monagas,Venezuela',
	'2862',
	'VE',
	'Province',
	'Monagas',
	'VE-N',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.9813
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21201,
	'Nueva Esparta,Venezuela',
	'2862',
	'VE',
	'Province',
	'Nueva Esparta',
	'VE-O',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.9882
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21202,
	'Portuguesa,Venezuela',
	'2862',
	'VE',
	'Province',
	'Portuguesa',
	'VE-P',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:03.9952
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21206,
	'Zulia,Venezuela',
	'2862',
	'VE',
	'Province',
	'Zulia',
	'VE-V',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.0022
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21213,
	'Vojvodina,Serbia',
	'2688',
	'RS',
	'Province',
	'Vojvodina',
	'RS-VO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.0082
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21214,
	'Eastern Cape,South Africa',
	'2710',
	'ZA',
	'Province',
	'Eastern Cape',
	'ZA-EC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.0142
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21215,
	'Free State,South Africa',
	'2710',
	'ZA',
	'Province',
	'Free State',
	'ZA-FS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.0212
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21216,
	'Gauteng,South Africa',
	'2710',
	'ZA',
	'Province',
	'Gauteng',
	'ZA-GT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.0271
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21217,
	'Mpumalanga,South Africa',
	'2710',
	'ZA',
	'Province',
	'Mpumalanga',
	'ZA-MP',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.0341
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21218,
	'Northern Cape,South Africa',
	'2710',
	'ZA',
	'Province',
	'Northern Cape',
	'ZA-NC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.0412
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21219,
	'KwaZulu-Natal,South Africa',
	'2710',
	'ZA',
	'Province',
	'KwaZulu-Natal',
	'ZA-NL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.0481
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21220,
	'Limpopo,South Africa',
	'2710',
	'ZA',
	'Province',
	'Limpopo',
	'ZA-LP',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.0542
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21221,
	'North West,South Africa',
	'2710',
	'ZA',
	'Province',
	'North West',
	'ZA-NW',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.0601
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21222,
	'Western Cape,South Africa',
	'2710',
	'ZA',
	'Province',
	'Western Cape',
	'ZA-WC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.0672
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21228,
	'State of Roraima,Brazil',
	'2076',
	'BR',
	'State',
	'State of Roraima',
	'BR-RR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.0722
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21229,
	'State of Sergipe,Brazil',
	'2076',
	'BR',
	'State',
	'State of Sergipe',
	'BR-SE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.0794
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21230,
	'State of Tocantins,Brazil',
	'2076',
	'BR',
	'State',
	'State of Tocantins',
	'BR-TO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.0854
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21232,
	'State of Acre,Brazil',
	'2076',
	'BR',
	'State',
	'State of Acre',
	'BR-AC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.0916
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21234,
	'Mari El Republic,Russia',
	'2643',
	'RU',
	'Region',
	'Mari El Republic',
	'RU-ME',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.0977
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21243,
	'Kalmykia,Russia',
	'2643',
	'RU',
	'Region',
	'Kalmykia',
	'RU-KL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.1047
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21244,
	'Alba County,Romania',
	'2642',
	'RO',
	'County',
	'Alba County',
	'RO-AB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.1107
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21245,
	'Covasna County,Romania',
	'2642',
	'RO',
	'County',
	'Covasna County',
	'RO-CV',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.1166
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21248,
	'Isparta Province,Turkey',
	'2792',
	'TR',
	'Province',
	'Isparta Province',
	'TR-32',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.1226
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21249,
	'Tokat,Turkey',
	'2792',
	'TR',
	'Province',
	'Tokat',
	'TR-60',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.1297
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21250,
	'Adygea,Russia',
	'2643',
	'RU',
	'Region',
	'Adygea',
	'RU-AD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.1367
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21251,
	'Ingushetia,Russia',
	'2643',
	'RU',
	'Region',
	'Ingushetia',
	'RU-IN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.1427
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21252,
	'North Ossetia鈥揂lania,Russia',
	'2643',
	'RU',
	'Region',
	'North Ossetia鈥揂lania',
	'RU-SE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.1487
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21255,
	'Beja District,Portugal',
	'2620',
	'PT',
	'Region',
	'Beja District',
	'PT-02',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.1547
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21256,
	'Oryol Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Oryol Oblast',
	'RU-ORL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.1618
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21258,
	'Hunedoara County,Romania',
	'2642',
	'RO',
	'County',
	'Hunedoara County',
	'RO-HD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.1678
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21261,
	'Gorj County,Romania',
	'2642',
	'RO',
	'County',
	'Gorj County',
	'RO-GJ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.1739
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21262,
	'Harghita County,Romania',
	'2642',
	'RO',
	'County',
	'Harghita County',
	'RO-HR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.1799
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21264,
	'Vaslui County,Romania',
	'2642',
	'RO',
	'County',
	'Vaslui County',
	'RO-VS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.1859
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21268,
	'Goa,India',
	'2356',
	'IN',
	'State',
	'Goa',
	'IN-GA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.1919
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21269,
	'Famagusta,Cyprus',
	'2196',
	'CY',
	'District',
	'Famagusta',
	'CY-04',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.1979
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21271,
	'Ngazidja,Comoros',
	'2174',
	'KM',
	'Province',
	'Ngazidja',
	'KM-G',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.2039
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21275,
	'Olt County,Romania',
	'2642',
	'RO',
	'County',
	'Olt County',
	'RO-OT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.2110
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21276,
	'Teleorman County,Romania',
	'2642',
	'RO',
	'County',
	'Teleorman County',
	'RO-TR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.2170
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21277,
	'Jewish Autonomous Oblast,Russia',
	'2643',
	'RU',
	'Region',
	'Jewish Autonomous Oblast',
	'RU-YEV',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.2230
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21288,
	'El Oro,Ecuador',
	'2218',
	'EC',
	'Province',
	'El Oro',
	'EC-O',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.2290
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21289,
	'Arunachal Pradesh,India',
	'2356',
	'IN',
	'State',
	'Arunachal Pradesh',
	'IN-AR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.2350
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21291,
	'Aceh,Indonesia',
	'2360',
	'ID',
	'Province',
	'Aceh',
	'ID-AC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.2420
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21293,
	'Yaracuy,Venezuela',
	'2862',
	'VE',
	'Province',
	'Yaracuy',
	'VE-U',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.2479
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21297,
	'San Luis Province,Argentina',
	'2032',
	'AR',
	'Province',
	'San Luis Province',
	'AR-D',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.2540
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21298,
	'Santiago del Estero Province,Argentina',
	'2032',
	'AR',
	'Province',
	'Santiago del Estero Province',
	'AR-G',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.2599
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21299,
	'Formosa Province,Argentina',
	'2032',
	'AR',
	'Province',
	'Formosa Province',
	'AR-P',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.2660
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21304,
	'Hawke''s Bay,New Zealand',
	'2554',
	'NZ',
	'Region',
	'Hawke''s Bay',
	'NZ-HKB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.2719
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21305,
	'Manawatu-Wanganui,New Zealand',
	'2554',
	'NZ',
	'Region',
	'Manawatu-Wanganui',
	'NZ-MWT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.2789
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21308,
	'Northland,New Zealand',
	'2554',
	'NZ',
	'Region',
	'Northland',
	'NZ-NTL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.2851
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21309,
	'Otago,New Zealand',
	'2554',
	'NZ',
	'Region',
	'Otago',
	'NZ-OTA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.2922
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21310,
	'Southland,New Zealand',
	'2554',
	'NZ',
	'Region',
	'Southland',
	'NZ-STL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.2982
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21311,
	'Taranaki,New Zealand',
	'2554',
	'NZ',
	'Region',
	'Taranaki',
	'NZ-TKI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.3044
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21312,
	'Tasman,New Zealand',
	'2554',
	'NZ',
	'Region',
	'Tasman',
	'NZ-TAS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.3114
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21313,
	'Waikato,New Zealand',
	'2554',
	'NZ',
	'Region',
	'Waikato',
	'NZ-WKO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.3185
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21315,
	'West Coast,New Zealand',
	'2554',
	'NZ',
	'Region',
	'West Coast',
	'NZ-WTC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.3245
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21320,
	'Daegu,South Korea',
	'2410',
	'KR',
	'Region',
	'Daegu',
	'KR-27',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.3315
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21323,
	'Daejeon,South Korea',
	'2410',
	'KR',
	'Region',
	'Daejeon',
	'KR-30',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.3375
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21325,
	'Gyeonggi-do,South Korea',
	'2410',
	'KR',
	'Region',
	'Gyeonggi-do',
	'KR-41',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.3446
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21326,
	'Gangwon-do,South Korea',
	'2410',
	'KR',
	'Region',
	'Gangwon-do',
	'KR-42',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.3506
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21327,
	'Chungcheongbuk-do,South Korea',
	'2410',
	'KR',
	'Region',
	'Chungcheongbuk-do',
	'KR-43',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.3576
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21328,
	'Chungcheongnam-do,South Korea',
	'2410',
	'KR',
	'Region',
	'Chungcheongnam-do',
	'KR-44',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.3646
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21329,
	'Jeollabuk-do,South Korea',
	'2410',
	'KR',
	'Region',
	'Jeollabuk-do',
	'KR-45',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.3706
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21330,
	'Jeollanam-do,South Korea',
	'2410',
	'KR',
	'Region',
	'Jeollanam-do',
	'KR-46',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.3776
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21331,
	'Gyeongsangbuk-do,South Korea',
	'2410',
	'KR',
	'Region',
	'Gyeongsangbuk-do',
	'KR-47',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.3827
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21332,
	'Gyeongsangnam-do,South Korea',
	'2410',
	'KR',
	'Region',
	'Gyeongsangnam-do',
	'KR-48',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.3897
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21333,
	'Jeju-do,South Korea',
	'2410',
	'KR',
	'Region',
	'Jeju-do',
	'KR-49',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.3956
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21334,
	'Chhattisgarh,India',
	'2356',
	'IN',
	'State',
	'Chhattisgarh',
	'IN-CT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.4036
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21335,
	'Himachal Pradesh,India',
	'2356',
	'IN',
	'State',
	'Himachal Pradesh',
	'IN-HP',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.4097
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21336,
	'Jharkhand,India',
	'2356',
	'IN',
	'State',
	'Jharkhand',
	'IN-JH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.4157
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21337,
	'Manipur,India',
	'2356',
	'IN',
	'State',
	'Manipur',
	'IN-MN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.4227
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21338,
	'Mizoram,India',
	'2356',
	'IN',
	'State',
	'Mizoram',
	'IN-MZ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.4287
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21339,
	'Nagaland,India',
	'2356',
	'IN',
	'State',
	'Nagaland',
	'IN-NL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.4348
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21340,
	'Sikkim,India',
	'2356',
	'IN',
	'State',
	'Sikkim',
	'IN-SK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.4408
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21341,
	'Uttarakhand,India',
	'2356',
	'IN',
	'State',
	'Uttarakhand',
	'IN-UT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.4478
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21342,
	'Chandigarh,India',
	'2356',
	'IN',
	'Union Territory',
	'Chandigarh',
	'IN-CH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.4538
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21343,
	'Dadra and Nagar Haveli,India',
	'2356',
	'IN',
	'Union Territory',
	'Dadra and Nagar Haveli',
	'IN-DN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.4598
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21344,
	'Daman and Diu,India',
	'2356',
	'IN',
	'Union Territory',
	'Daman and Diu',
	'IN-DD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.4668
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21345,
	'Lakshadweep,India',
	'2356',
	'IN',
	'Union Territory',
	'Lakshadweep',
	'IN-LD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.4728
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21346,
	'Tuva,Russia',
	'2643',
	'RU',
	'Region',
	'Tuva',
	'RU-TY',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.4788
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21347,
	'Chechnya,Russia',
	'2643',
	'RU',
	'Region',
	'Chechnya',
	'RU-CE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.4848
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21348,
	'Chuvashia Republic,Russia',
	'2643',
	'RU',
	'Region',
	'Chuvashia Republic',
	'RU-CU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.4908
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21351,
	'Nenets Autonomous Okrug,Russia',
	'2643',
	'RU',
	'Region',
	'Nenets Autonomous Okrug',
	'RU-NEN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.4968
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21355,
	'Amasya Province,Turkey',
	'2792',
	'TR',
	'Province',
	'Amasya Province',
	'TR-05',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.5028
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21357,
	'Bilecik,Turkey',
	'2792',
	'TR',
	'Province',
	'Bilecik',
	'TR-11',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.5088
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21360,
	'Burdur Province,Turkey',
	'2792',
	'TR',
	'Province',
	'Burdur Province',
	'TR-15',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.5148
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21371,
	'Ordu,Turkey',
	'2792',
	'TR',
	'Province',
	'Ordu',
	'TR-52',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.5218
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21372,
	'Rize,Turkey',
	'2792',
	'TR',
	'Province',
	'Rize',
	'TR-53',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.5278
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21373,
	'Siirt Province,Turkey',
	'2792',
	'TR',
	'Province',
	'Siirt Province',
	'TR-56',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.5348
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21374,
	'Sinop Province,Turkey',
	'2792',
	'TR',
	'Province',
	'Sinop Province',
	'TR-57',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.5408
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21382,
	'Ardahan Province,Turkey',
	'2792',
	'TR',
	'Province',
	'Ardahan Province',
	'TR-75',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.5468
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21385,
	'Altai Krai,Russia',
	'2643',
	'RU',
	'Region',
	'Altai Krai',
	'RU-ALT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.5528
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21386,
	'Aragon,Spain',
	'2724',
	'ES',
	'Autonomous Community',
	'Aragon',
	'ES-AR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.5597
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21388,
	'Valencian Community,Spain',
	'2724',
	'ES',
	'Autonomous Community',
	'Valencian Community',
	'ES-VC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.5687
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21440,
	'Bjelovar-Bilogora County,Croatia',
	'2191',
	'HR',
	'County',
	'Bjelovar-Bilogora County',
	'HR-07',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.5757
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21441,
	'Karlovac County,Croatia',
	'2191',
	'HR',
	'County',
	'Karlovac County',
	'HR-04',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.5817
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21443,
	'Istria County,Croatia',
	'2191',
	'HR',
	'County',
	'Istria County',
	'HR-18',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.5877
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21445,
	'Sisak-Moslavina County,Croatia',
	'2191',
	'HR',
	'County',
	'Sisak-Moslavina County',
	'HR-03',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.5947
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21446,
	'Brod-Posavina County,Croatia',
	'2191',
	'HR',
	'County',
	'Brod-Posavina County',
	'HR-12',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.6006
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21448,
	'Al Madinah Province,Saudi Arabia',
	'2682',
	'SA',
	'Province',
	'Al Madinah Province',
	'SA-03',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.6076
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21449,
	'Al Qassim,Saudi Arabia',
	'2682',
	'SA',
	'Province',
	'Al Qassim',
	'SA-05',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.6136
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21450,
	'Hail Province,Saudi Arabia',
	'2682',
	'SA',
	'Province',
	'Hail Province',
	'SA-06',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.6196
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21451,
	'Tabuk Province,Saudi Arabia',
	'2682',
	'SA',
	'Province',
	'Tabuk Province',
	'SA-07',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.6256
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21452,
	'Northern Borders Province,Saudi Arabia',
	'2682',
	'SA',
	'Province',
	'Northern Borders Province',
	'SA-08',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.6316
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21455,
	'Al Bahah Province,Saudi Arabia',
	'2682',
	'SA',
	'Province',
	'Al Bahah Province',
	'SA-11',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.6365
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21456,
	'Al Jowf,Saudi Arabia',
	'2682',
	'SA',
	'Province',
	'Al Jowf',
	'SA-12',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.6435
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21457,
	'Aseer Province,Saudi Arabia',
	'2682',
	'SA',
	'Province',
	'Aseer Province',
	'SA-14',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.6485
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21458,
	'Dakahlia Governorate,Egypt',
	'2818',
	'EG',
	'Governorate',
	'Dakahlia Governorate',
	'EG-DK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.6555
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21459,
	'Red Sea Governorate,Egypt',
	'2818',
	'EG',
	'Governorate',
	'Red Sea Governorate',
	'EG-BA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.6625
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21460,
	'El Beheira Governorate,Egypt',
	'2818',
	'EG',
	'Governorate',
	'El Beheira Governorate',
	'EG-BH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.6695
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21461,
	'Faiyum Governorate,Egypt',
	'2818',
	'EG',
	'Governorate',
	'Faiyum Governorate',
	'EG-FYM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.6744
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21462,
	'Gharbia Governorate,Egypt',
	'2818',
	'EG',
	'Governorate',
	'Gharbia Governorate',
	'EG-GH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.6804
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21463,
	'Alexandria Governorate,Egypt',
	'2818',
	'EG',
	'Governorate',
	'Alexandria Governorate',
	'EG-ALX',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.6864
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21464,
	'Ismailia Governorate,Egypt',
	'2818',
	'EG',
	'Governorate',
	'Ismailia Governorate',
	'EG-IS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.6914
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21465,
	'Giza Governorate,Egypt',
	'2818',
	'EG',
	'Governorate',
	'Giza Governorate',
	'EG-GZ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.6974
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21466,
	'Menofia Governorate,Egypt',
	'2818',
	'EG',
	'Governorate',
	'Menofia Governorate',
	'EG-MNF',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.7034
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21467,
	'Menia Governorate,Egypt',
	'2818',
	'EG',
	'Governorate',
	'Menia Governorate',
	'EG-MN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.7093
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21468,
	'Cairo Governorate,Egypt',
	'2818',
	'EG',
	'Governorate',
	'Cairo Governorate',
	'EG-C',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.7143
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21469,
	'Al Qalyubia Governorate,Egypt',
	'2818',
	'EG',
	'Governorate',
	'Al Qalyubia Governorate',
	'EG-KB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.7203
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21470,
	'Luxor Governorate,Egypt',
	'2818',
	'EG',
	'Governorate',
	'Luxor Governorate',
	'EG-LX',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.7263
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21472,
	'New Valley Governorate,Egypt',
	'2818',
	'EG',
	'Governorate',
	'New Valley Governorate',
	'EG-WAD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.7333
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21473,
	'Suez Governorate,Egypt',
	'2818',
	'EG',
	'Governorate',
	'Suez Governorate',
	'EG-SUZ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.7393
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21474,
	'Ash Sharqia Governorate,Egypt',
	'2818',
	'EG',
	'Governorate',
	'Ash Sharqia Governorate',
	'EG-SHR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.7453
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21475,
	'Aswan Governorate,Egypt',
	'2818',
	'EG',
	'Governorate',
	'Aswan Governorate',
	'EG-ASN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.7514
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21476,
	'Assiut Governorate,Egypt',
	'2818',
	'EG',
	'Governorate',
	'Assiut Governorate',
	'EG-AST',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.7574
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21477,
	'Beni Suef Governorate,Egypt',
	'2818',
	'EG',
	'Governorate',
	'Beni Suef Governorate',
	'EG-BNS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.7634
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21478,
	'Port Said Governorate,Egypt',
	'2818',
	'EG',
	'Governorate',
	'Port Said Governorate',
	'EG-PTS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.7703
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21479,
	'Damietta Governorate,Egypt',
	'2818',
	'EG',
	'Governorate',
	'Damietta Governorate',
	'EG-DT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.7763
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21480,
	'South Sinai Governorate,Egypt',
	'2818',
	'EG',
	'Governorate',
	'South Sinai Governorate',
	'EG-JS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.7825
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21481,
	'Kafr El Sheikh Governorate,Egypt',
	'2818',
	'EG',
	'Governorate',
	'Kafr El Sheikh Governorate',
	'EG-KFS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.7885
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21482,
	'Matrouh Governorate,Egypt',
	'2818',
	'EG',
	'Governorate',
	'Matrouh Governorate',
	'EG-MT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.7945
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21483,
	'Qena Governorate,Egypt',
	'2818',
	'EG',
	'Governorate',
	'Qena Governorate',
	'EG-KN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.8007
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21484,
	'North Sinai Governorate,Egypt',
	'2818',
	'EG',
	'Governorate',
	'North Sinai Governorate',
	'EG-SIN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.8077
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21485,
	'Sohag Governorate,Egypt',
	'2818',
	'EG',
	'Governorate',
	'Sohag Governorate',
	'EG-SHG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.8137
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21486,
	'Prachuap Khiri Khan,Thailand',
	'2764',
	'TH',
	'Province',
	'Prachuap Khiri Khan',
	'TH-77',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.8197
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21489,
	'Krapina-Zagorje County,Croatia',
	'2191',
	'HR',
	'County',
	'Krapina-Zagorje County',
	'HR-02',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.8277
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21490,
	'Lika-Senj County,Croatia',
	'2191',
	'HR',
	'County',
	'Lika-Senj County',
	'HR-09',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.8337
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21491,
	'Virovitica-Podravina County,Croatia',
	'2191',
	'HR',
	'County',
	'Virovitica-Podravina County',
	'HR-10',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.8407
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21493,
	'City of Zagreb,Croatia',
	'2191',
	'HR',
	'County',
	'City of Zagreb',
	'HR-21',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.8467
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21494,
	'Central Bohemian Region,Czechia',
	'2203',
	'CZ',
	'Region',
	'Central Bohemian Region',
	'CZ-ST',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.8537
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21496,
	'Karlovy Vary Region,Czechia',
	'2203',
	'CZ',
	'Region',
	'Karlovy Vary Region',
	'CZ-KA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.8597
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21497,
	'Liberec Region,Czechia',
	'2203',
	'CZ',
	'Region',
	'Liberec Region',
	'CZ-LI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.8656
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21498,
	'Moravian-Silesian Region,Czechia',
	'2203',
	'CZ',
	'Region',
	'Moravian-Silesian Region',
	'CZ-MO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.8726
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21499,
	'Olomouc Region,Czechia',
	'2203',
	'CZ',
	'Region',
	'Olomouc Region',
	'CZ-OL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.8796
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21500,
	'Pardubice Region,Czechia',
	'2203',
	'CZ',
	'Region',
	'Pardubice Region',
	'CZ-PA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.8868
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21504,
	'Vysocina Region,Czechia',
	'2203',
	'CZ',
	'Region',
	'Vysocina Region',
	'CZ-VY',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.8939
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21505,
	'Zlin Region,Czechia',
	'2203',
	'CZ',
	'Region',
	'Zlin Region',
	'CZ-ZL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.8999
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21506,
	'Amazonas Department,Colombia',
	'2170',
	'CO',
	'Region',
	'Amazonas Department',
	'CO-AMA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.9069
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21508,
	'Boyaca,Colombia',
	'2170',
	'CO',
	'Region',
	'Boyaca',
	'CO-BOY',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.9129
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21510,
	'Casanare,Colombia',
	'2170',
	'CO',
	'Region',
	'Casanare',
	'CO-CAS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.9189
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21511,
	'Cauca,Colombia',
	'2170',
	'CO',
	'Region',
	'Cauca',
	'CO-CAU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.9260
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21512,
	'Cesar,Colombia',
	'2170',
	'CO',
	'Region',
	'Cesar',
	'CO-CES',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.9320
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21513,
	'Choco,Colombia',
	'2170',
	'CO',
	'Region',
	'Choco',
	'CO-CHO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.9389
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21515,
	'Guainia,Colombia',
	'2170',
	'CO',
	'Region',
	'Guainia',
	'CO-GUA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.9450
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21516,
	'Guaviare,Colombia',
	'2170',
	'CO',
	'Region',
	'Guaviare',
	'CO-GUV',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.9509
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21517,
	'La Guajira,Colombia',
	'2170',
	'CO',
	'Region',
	'La Guajira',
	'CO-LAG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.9570
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21518,
	'North Santander,Colombia',
	'2170',
	'CO',
	'Region',
	'North Santander',
	'CO-SAN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.9629
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21519,
	'Quindio,Colombia',
	'2170',
	'CO',
	'Region',
	'Quindio',
	'CO-QUI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.9690
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21522,
	'Vaupes,Colombia',
	'2170',
	'CO',
	'Region',
	'Vaupes',
	'CO-VAU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.9750
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21523,
	'Vichada,Colombia',
	'2170',
	'CO',
	'Region',
	'Vichada',
	'CO-VID',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.9820
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21524,
	'Chaouia-Ouardigha,Morocco',
	'2504',
	'MA',
	'Region',
	'Chaouia-Ouardigha',
	'MA-09',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.9880
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21525,
	'Doukkala-Abda,Morocco',
	'2504',
	'MA',
	'Region',
	'Doukkala-Abda',
	'MA-10',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:04.9940
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21526,
	'Fes-Boulemane,Morocco',
	'2504',
	'MA',
	'Region',
	'Fes-Boulemane',
	'MA-05',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.0003
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21527,
	'Gharb-Chrarda-Beni Hssen,Morocco',
	'2504',
	'MA',
	'Region',
	'Gharb-Chrarda-Beni Hssen',
	'MA-02',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.0072
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21528,
	'Grand Casablanca,Morocco',
	'2504',
	'MA',
	'Region',
	'Grand Casablanca',
	'MA-08',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.0132
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21530,
	'Laayoune-Boujdour-Sakia El Hamra,Morocco',
	'2504',
	'MA',
	'Region',
	'Laayoune-Boujdour-Sakia El Hamra',
	'MA-15',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.0202
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21531,
	'Marrakesh-Tensift-El Haouz,Morocco',
	'2504',
	'MA',
	'Region',
	'Marrakesh-Tensift-El Haouz',
	'MA-11',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.0262
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21532,
	'Meknes-Tafilalet,Morocco',
	'2504',
	'MA',
	'Region',
	'Meknes-Tafilalet',
	'MA-06',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.0322
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21534,
	'Rabat-Sale-Zemmour-Zaer,Morocco',
	'2504',
	'MA',
	'Region',
	'Rabat-Sale-Zemmour-Zaer',
	'MA-07',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.0382
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21535,
	'Souss-Massa-Draa,Morocco',
	'2504',
	'MA',
	'Region',
	'Souss-Massa-Draa',
	'MA-13',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.0441
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21536,
	'Tadla-Azilal,Morocco',
	'2504',
	'MA',
	'Region',
	'Tadla-Azilal',
	'MA-12',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.0511
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21537,
	'Tangier-Tetouan,Morocco',
	'2504',
	'MA',
	'Region',
	'Tangier-Tetouan',
	'MA-01',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.0591
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21538,
	'Taza-Al Hoceima-Taounate,Morocco',
	'2504',
	'MA',
	'Region',
	'Taza-Al Hoceima-Taounate',
	'MA-03',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.0651
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21539,
	'Putrajaya,Malaysia',
	'2458',
	'MY',
	'State',
	'Putrajaya',
	'MY-16',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.0721
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21540,
	'Abia,Nigeria',
	'2566',
	'NG',
	'State',
	'Abia',
	'NG-AB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.0781
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21541,
	'Federal Capital Territory,Nigeria',
	'2566',
	'NG',
	'State',
	'Federal Capital Territory',
	'NG-FC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.0840
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21543,
	'Akwa Ibom,Nigeria',
	'2566',
	'NG',
	'State',
	'Akwa Ibom',
	'NG-AK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.0890
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21544,
	'Anambra,Nigeria',
	'2566',
	'NG',
	'State',
	'Anambra',
	'NG-AN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.0960
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21546,
	'Bayelsa,Nigeria',
	'2566',
	'NG',
	'State',
	'Bayelsa',
	'NG-BY',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.1020
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21547,
	'Benue,Nigeria',
	'2566',
	'NG',
	'State',
	'Benue',
	'NG-BE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.1080
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21548,
	'Borno,Nigeria',
	'2566',
	'NG',
	'State',
	'Borno',
	'NG-BO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.1140
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21550,
	'Delta,Nigeria',
	'2566',
	'NG',
	'State',
	'Delta',
	'NG-DE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.1199
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21551,
	'Ebonyi,Nigeria',
	'2566',
	'NG',
	'State',
	'Ebonyi',
	'NG-EB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.1259
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21552,
	'Edo,Nigeria',
	'2566',
	'NG',
	'State',
	'Edo',
	'NG-ED',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.1319
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21553,
	'Ekiti,Nigeria',
	'2566',
	'NG',
	'State',
	'Ekiti',
	'NG-EK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.1379
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21556,
	'Imo,Nigeria',
	'2566',
	'NG',
	'State',
	'Imo',
	'NG-IM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.1449
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21557,
	'Jigawa,Nigeria',
	'2566',
	'NG',
	'State',
	'Jigawa',
	'NG-JI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.1509
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21558,
	'Kaduna,Nigeria',
	'2566',
	'NG',
	'State',
	'Kaduna',
	'NG-KD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.1568
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21561,
	'Kebbi,Nigeria',
	'2566',
	'NG',
	'State',
	'Kebbi',
	'NG-KE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.1638
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21562,
	'Kogi,Nigeria',
	'2566',
	'NG',
	'State',
	'Kogi',
	'NG-KO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.1698
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21563,
	'Kwara,Nigeria',
	'2566',
	'NG',
	'State',
	'Kwara',
	'NG-KW',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.1758
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21565,
	'Nasarawa,Nigeria',
	'2566',
	'NG',
	'State',
	'Nasarawa',
	'NG-NA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.1828
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21566,
	'Niger,Nigeria',
	'2566',
	'NG',
	'State',
	'Niger',
	'NG-NI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.1888
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21567,
	'Ogun State,Nigeria',
	'2566',
	'NG',
	'State',
	'Ogun State',
	'NG-OG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.1947
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21569,
	'Osun,Nigeria',
	'2566',
	'NG',
	'State',
	'Osun',
	'NG-OS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.2007
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21571,
	'Plateau,Nigeria',
	'2566',
	'NG',
	'State',
	'Plateau',
	'NG-PL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.2077
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21574,
	'Taraba,Nigeria',
	'2566',
	'NG',
	'State',
	'Taraba',
	'NG-TA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.2137
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21575,
	'Yobe,Nigeria',
	'2566',
	'NG',
	'State',
	'Yobe',
	'NG-YO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.2197
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	21576,
	'Zamfara,Nigeria',
	'2566',
	'NG',
	'State',
	'Zamfara',
	'NG-ZA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.2267
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1000011,
	'Ajman,Ajman,United Arab Emirates',
	'9047096',
	'AE',
	'City',
	'Ajman',
	'AE-AJ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.2326
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1000099,
	'Neuquen,Neuquen,Argentina',
	'20019',
	'AR',
	'City',
	'Neuquen',
	'AR-Q',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.2386
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1000133,
	'Jujuy,Jujuy,Argentina',
	'20027',
	'AR',
	'City',
	'Jujuy',
	'AR-Y',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.2446
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1000878,
	'Salzburg,Salzburg,Austria',
	'20046',
	'AT',
	'City',
	'Salzburg',
	'AT-5',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.2516
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1001460,
	'Montana,Montana Province,Bulgaria',
	'9040146',
	'BG',
	'City',
	'Montana',
	'US-MT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.2566
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1001461,
	'Vidin,Vidin,Bulgaria',
	'9040145',
	'BG',
	'City',
	'Vidin',
	'BG-05',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.2636
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1001467,
	'Ruse,Ruse,Bulgaria',
	'9040137',
	'BG',
	'City',
	'Ruse',
	'BG-18',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.2695
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1001775,
	'Sao Vicente,State of Sao Paulo,Brazil',
	'20106',
	'BR',
	'City',
	'Sao Vicente',
	'CV-SV',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.2765
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1002024,
	'Rivers,Manitoba,Canada',
	'20115',
	'CA',
	'City',
	'Rivers',
	'NG-RI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.2835
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1002614,
	'Piedmont,Quebec,Canada',
	'20123',
	'CA',
	'City',
	'Piedmont',
	'IT-21',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.2905
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1003113,
	'St. Gallen,St. Gallen,Switzerland',
	'20141',
	'CH',
	'City',
	'St. Gallen',
	'CH-SG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.2965
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1003297,
	'Zurich,Zurich,Switzerland',
	'20151',
	'CH',
	'City',
	'Zurich',
	'CH-ZH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.3035
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1003302,
	'Concepcion,Bio Bio,Chile',
	'20154',
	'CL',
	'City',
	'Concepcion',
	'PY-1',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.3105
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1003334,
	'Beijing,Beijing,China',
	'20163',
	'CN',
	'City',
	'Beijing',
	'CN-11',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.3165
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1003338,
	'Tianjin,Tianjin,China',
	'20164',
	'CN',
	'City',
	'Tianjin',
	'CN-12',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.3225
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1003384,
	'Jilin,Jilin,China',
	'20169',
	'CN',
	'City',
	'Jilin',
	'CN-22',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.3295
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1003401,
	'Shanghai,Shanghai,China',
	'20171',
	'CN',
	'City',
	'Shanghai',
	'CN-31',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.3365
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1003648,
	'Kinshasa,Democratic Republic of the Congo',
	'2180',
	'CD',
	'City',
	'Kinshasa',
	'CD-KN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.3434
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1003673,
	'Alajuela,Alajuela Province,Costa Rica',
	'9070299',
	'CR',
	'City',
	'Alajuela',
	'CR-A',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.3494
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1003698,
	'Limassol,Limassol,Cyprus',
	'20215',
	'CY',
	'City',
	'Limassol',
	'CY-02',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.3554
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1003701,
	'Larnaca,Larnaca,Cyprus',
	'20216',
	'CY',
	'City',
	'Larnaca',
	'CY-03',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.3615
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1003703,
	'Paphos,Paphos,Cyprus',
	'20217',
	'CY',
	'City',
	'Paphos',
	'CY-05',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.3674
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1003857,
	'Brandenburg,Brandenburg,Germany',
	'20227',
	'DE',
	'City',
	'Brandenburg',
	'DE-BB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.3734
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1004397,
	'Limburg,Hesse,Germany',
	'20231',
	'DE',
	'City',
	'Limburg',
	'NL-LI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.3796
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1005003,
	'Djibouti,Djibouti,Djibouti',
	'9075483',
	'DJ',
	'City',
	'Djibouti',
	'DJ-DJ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.3857
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1005367,
	'Biskra,Biskra Province,Algeria',
	'9069723',
	'DZ',
	'City',
	'Biskra',
	'DZ-07',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.3917
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1005370,
	'Setif,Setif Province,Algeria',
	'9069720',
	'DZ',
	'City',
	'Setif',
	'DZ-19',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.3977
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1005414,
	'Granada,Andalusia,Spain',
	'20269',
	'ES',
	'City',
	'Granada',
	'NI-GR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.4036
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1005447,
	'Ceuta,Ceuta,Spain',
	'20274',
	'ES',
	'City',
	'Ceuta',
	'ES-CE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.4098
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1005452,
	'Leon,Castile and Leon,Spain',
	'20275',
	'ES',
	'City',
	'Leon',
	'NI-LE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.4271
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1005473,
	'Santa Barbara,Catalonia,Spain',
	'20278',
	'ES',
	'City',
	'Santa Barbara',
	'HN-SB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.4331
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1005498,
	'Melilla,Melilla,Spain',
	'20283',
	'ES',
	'City',
	'Melilla',
	'ES-ML',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.4401
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1007633,
	'Budapest,Budapest,Hungary',
	'20418',
	'HU',
	'City',
	'Budapest',
	'HU-BU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.4461
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1007699,
	'Jambi,Jambi,Indonesia',
	'20437',
	'ID',
	'City',
	'Jambi',
	'ID-JA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.4521
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1007949,
	'Baghdad,Baghdad Governorate,Iraq',
	'9069853',
	'IQ',
	'City',
	'Baghdad',
	'IQ-BG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.4590
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1008047,
	'Aosta,Aosta,Italy',
	'9053424',
	'IT',
	'City',
	'Aosta',
	'IT-23',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.4651
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1009317,
	'Tokyo,Tokyo,Japan',
	'20636',
	'JP',
	'City',
	'Tokyo',
	'JP-13',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.4731
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1009738,
	'Takeo,Saga,Japan',
	'20664',
	'JP',
	'City',
	'Takeo',
	'KH-21',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.4801
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1009846,
	'Incheon,Incheon,South Korea',
	'21321',
	'KR',
	'City',
	'Incheon',
	'KR-28',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.4861
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1009856,
	'Gwangju,Gwangju,South Korea',
	'21322',
	'KR',
	'City',
	'Gwangju',
	'KR-29',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.4922
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1009866,
	'Busan,Busan,South Korea',
	'21319',
	'KR',
	'City',
	'Busan',
	'KR-26',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.4983
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1009871,
	'Seoul,Seoul,South Korea',
	'21318',
	'KR',
	'City',
	'Seoul',
	'KR-11',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.5053
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1009887,
	'Ulsan,Ulsan,South Korea',
	'21324',
	'KR',
	'City',
	'Ulsan',
	'KR-31',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.5113
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1009899,
	'Vientiane,Vientiane Prefecture,Laos',
	'9070201',
	'LA',
	'City',
	'Vientiane',
	'LA-VI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.5183
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1009901,
	'Beirut,Beirut Governorate,Lebanon',
	'9070354',
	'LB',
	'City',
	'Beirut',
	'LB-BA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.5244
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1009916,
	'Schaan,Liechtenstein',
	'2438',
	'LI',
	'Province',
	'Schaan',
	'LI-07',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.5313
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1009990,
	'Bender,Bender,Moldova',
	'9069917',
	'MD',
	'City',
	'Bender',
	'MD-BD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.5383
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1009994,
	'Antananarivo,Antananarivo Province,Madagascar',
	'9070179',
	'MG',
	'City',
	'Antananarivo',
	'MG-T',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.5443
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1009996,
	'Mahajanga,Mahajanga Province,Madagascar',
	'9070178',
	'MG',
	'City',
	'Mahajanga',
	'MG-M',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.5513
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1009998,
	'Toamasina,Toamasina Province,Madagascar',
	'9070176',
	'MG',
	'City',
	'Toamasina',
	'MG-A',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.5573
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1010000,
	'Aguascalientes,Aguascalientes,Mexico',
	'20694',
	'MX',
	'City',
	'Aguascalientes',
	'MX-AGU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.5633
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1010016,
	'Campeche,Campeche,Mexico',
	'20698',
	'MX',
	'City',
	'Campeche',
	'MX-CAM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.5693
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1010039,
	'San Pedro,Coahuila,Mexico',
	'20701',
	'MX',
	'City',
	'San Pedro',
	'PY-2',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.5752
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1010056,
	'Guanajuato,Guanajuato,Mexico',
	'20706',
	'MX',
	'City',
	'Guanajuato',
	'MX-GUA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.5813
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1010137,
	'Oaxaca,Oaxaca,Mexico',
	'20714',
	'MX',
	'Municipality',
	'Oaxaca',
	'MX-OAX',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.5874
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1010163,
	'San Luis Potosi,San Luis Potosi,Mexico',
	'20719',
	'MX',
	'City',
	'San Luis Potosi',
	'MX-SLP',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.5935
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1010197,
	'Cordoba,Veracruz,Mexico',
	'20724',
	'MX',
	'City',
	'Cordoba',
	'CO-COR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.5994
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1010217,
	'Ulaanbaatar,Ulaanbaatar,Mongolia',
	'9075799',
	'MN',
	'City',
	'Ulaanbaatar',
	'MN-1',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.6064
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1010224,
	'Port Louis,Port Louis District,Mauritius',
	'9069998',
	'MU',
	'City',
	'Port Louis',
	'MU-PL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.6134
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1010257,
	'Malacca,Malacca,Malaysia',
	'20744',
	'MY',
	'City',
	'Malacca',
	'MY-04',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.6195
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1010282,
	'Niamey,Niamey Urban Community,Niger',
	'9070454',
	'NE',
	'City',
	'Niamey',
	'NE-8',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.6255
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1010287,
	'Enugu,Enugu,Nigeria',
	'21554',
	'NG',
	'City',
	'Enugu',
	'NG-EN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.6316
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1010293,
	'Kano,Kano,Nigeria',
	'21559',
	'NG',
	'City',
	'Kano',
	'NG-KN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.6376
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1010294,
	'Lagos,Lagos,Nigeria',
	'21564',
	'NG',
	'City',
	'Lagos',
	'NG-LA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.6436
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1010298,
	'Sokoto,Sokoto,Nigeria',
	'21573',
	'NG',
	'City',
	'Sokoto',
	'NG-SO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.6495
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1010302,
	'Managua,Managua,Nicaragua',
	'9070006',
	'NI',
	'City',
	'Managua',
	'NI-MN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.6555
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1010659,
	'Utrecht,Utrecht,Utrecht,Netherlands',
	'20768',
	'NL',
	'City',
	'Utrecht',
	'NL-UT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.6625
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1011036,
	'Auckland,Auckland,New Zealand',
	'20791',
	'NZ',
	'City',
	'Auckland',
	'NZ-AUK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.6685
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1011037,
	'Gisborne,Gisborne,New Zealand',
	'21303',
	'NZ',
	'City',
	'Gisborne',
	'NZ-GIS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.6745
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1011072,
	'Nelson,Nelson,New Zealand',
	'21307',
	'NZ',
	'City',
	'Nelson',
	'NZ-NSN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.6815
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1011109,
	'Ayacucho,Ayacucho,Peru',
	'20798',
	'PE',
	'City',
	'Ayacucho',
	'PE-AYA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.6874
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1011110,
	'Cajamarca,Cajamarca,Cajamarca,Peru',
	'20799',
	'PE',
	'City',
	'Cajamarca',
	'PE-CAJ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.6934
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1011138,
	'Piura,Piura,Piura,Peru',
	'20812',
	'PE',
	'City',
	'Piura',
	'PE-PIU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.7004
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1011189,
	'Koror,Palau',
	'2585',
	'PW',
	'City',
	'Koror',
	'PW-150',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.7054
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1011713,
	'Beja,Beja District,Portugal',
	'21255',
	'PT',
	'City',
	'Beja',
	'TN-31',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.7124
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1011714,
	'Braga,Braga,Portugal',
	'20866',
	'PT',
	'City',
	'Braga',
	'PT-03',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.7184
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1011795,
	'Bucharest,Bucharest,Romania',
	'20889',
	'RO',
	'City',
	'Bucharest',
	'RO-B',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.7254
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1011809,
	'Calarasi,Calarasi County,Romania',
	'20898',
	'RO',
	'City',
	'Calarasi',
	'MD-CL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.7314
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1011969,
	'Moscow,Moscow,Russia',
	'20950',
	'RU',
	'City',
	'Moscow',
	'RU-MOW',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.7384
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1012040,
	'Saint Petersburg,Saint Petersburg,Russia',
	'20968',
	'RU',
	'City',
	'Saint Petersburg',
	'RU-SPE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.7454
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1012087,
	'Kigali,Kigali City,Rwanda',
	'9070449',
	'RW',
	'City',
	'Kigali',
	'RW-01',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.7514
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1012108,
	'San Salvador,San Salvador Department,El Salvador',
	'9069897',
	'SV',
	'City',
	'San Salvador',
	'SV-SS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.7584
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1012109,
	'Santa Ana,Santa Ana Department,El Salvador',
	'9069900',
	'SV',
	'City',
	'Santa Ana',
	'SV-SA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.7654
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1012113,
	'Sao Tome,Sao Tome and Principe',
	'2678',
	'ST',
	'City',
	'Sao Tome',
	'ST-S',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.7713
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1012114,
	'Paramaribo,Suriname',
	'2740',
	'SR',
	'City',
	'Paramaribo',
	'SR-PR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.7783
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1012519,
	'Henan,Vastra Gotaland County,Sweden',
	'21013',
	'SE',
	'City',
	'Henan',
	'CN-41',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.7853
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1012719,
	'Manzini,Manzini,Swaziland',
	'9070268',
	'SZ',
	'City',
	'Manzini',
	'SZ-MA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.7913
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1012726,
	'N''Djamena,Chad',
	'2148',
	'TD',
	'City',
	'N''Djamena',
	'TD-ND',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.7973
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1012728,
	'Bangkok,Bangkok,Thailand',
	'21025',
	'TH',
	'City',
	'Bangkok',
	'TH-10',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.8043
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1012753,
	'Tambon Yala,Yala,Thailand',
	'21047',
	'TH',
	'City',
	'Yala',
	'TH-95',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.8102
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1012755,
	'Ashgabat,Ashgabat,Turkmenistan',
	'9075932',
	'TM',
	'City',
	'Ashgabat',
	'TM-S',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.8172
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1012759,
	'Gabes,Gabes,Tunisia',
	'9075940',
	'TN',
	'City',
	'Gabes',
	'TN-81',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.8232
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1012760,
	'Tunis,Tunis,Tunisia',
	'9075946',
	'TN',
	'City',
	'Tunis',
	'TN-11',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.8302
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1012761,
	'Adana,Adana,Turkey',
	'21052',
	'TR',
	'City',
	'Adana',
	'TR-01',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.8372
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1012763,
	'Ankara,Ankara,Turkey',
	'21055',
	'TR',
	'City',
	'Ankara',
	'TR-06',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.8432
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1012769,
	'Bursa,Bursa,Turkey',
	'21060',
	'TR',
	'City',
	'Bursa',
	'TR-16',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.8491
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1012774,
	'Erzincan,Erzincan,Turkey',
	'21287',
	'TR',
	'City',
	'Erzincan',
	'TR-24',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.8551
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1012775,
	'Erzurum,Erzurum,Turkey',
	'21064',
	'TR',
	'City',
	'Erzurum',
	'TR-25',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.8621
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1012784,
	'Kars,Kars,Turkey',
	'21071',
	'TR',
	'City',
	'Kars',
	'TR-36',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.8681
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1012790,
	'Malatya,Malatya,Turkey',
	'21077',
	'TR',
	'City',
	'Malatya',
	'TR-44',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.8741
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1012798,
	'Sivas,Sivas,Turkey',
	'21086',
	'TR',
	'City',
	'Sivas',
	'TR-58',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.8801
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1012801,
	'Trabzon,Trabzon,Turkey',
	'21088',
	'TR',
	'City',
	'Trabzon',
	'TR-61',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.8850
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1012802,
	'Van,Van,Turkey',
	'21090',
	'TR',
	'City',
	'Van',
	'TR-65',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.8910
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1012803,
	'Yozgat,Yozgat,Turkey',
	'21257',
	'TR',
	'City',
	'Yozgat',
	'TR-66',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.8970
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1012804,
	'Zonguldak,Zonguldak,Turkey',
	'21091',
	'TR',
	'City',
	'Zonguldak',
	'TR-67',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.9020
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1012805,
	'Karaman,Karaman,Turkey',
	'21092',
	'TR',
	'City',
	'Karaman',
	'TR-70',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.9080
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1012818,
	'Tainan City,Taiwan',
	'2158',
	'TW',
	'City',
	'Tainan City',
	'TW-TNN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.9140
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1012825,
	'New Taipei City,Taiwan',
	'2158',
	'TW',
	'City',
	'New Taipei City',
	'TW-TPQ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.9180
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1012830,
	'Arusha,Arusha Region,Tanzania',
	'9070385',
	'TZ',
	'City',
	'Arusha',
	'TZ-01',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.9239
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1012832,
	'Mwanza,Mwanza Region,Tanzania',
	'9069891',
	'TZ',
	'City',
	'Mwanza',
	'TZ-18',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.9289
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1012872,
	'Montevideo,Uruguay',
	'2858',
	'UY',
	'City',
	'Montevideo',
	'UY-MO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.9409
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1012937,
	'Andalusia,Alabama,United States',
	'21133',
	'US',
	'City',
	'Andalusia',
	'ES-AN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.9479
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1012968,
	'Centre,Alabama,United States',
	'21133',
	'US',
	'City',
	'Centre',
	'HT-CE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.9539
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1013170,
	'Bay,Arkansas,United States',
	'21135',
	'US',
	'City',
	'Bay',
	'SO-BY',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.9598
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1013214,
	'England,Arkansas,United States',
	'21135',
	'US',
	'City',
	'England',
	'GB-ENG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.9658
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1013493,
	'Saint David,Arizona,United States',
	'21136',
	'US',
	'City',
	'Saint David',
	'VC-03',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.9738
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1014238,
	'San Miguel,California,United States',
	'21137',
	'US',
	'City',
	'San Miguel',
	'SV-SM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.9798
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1014300,
	'Saint Helena,California,United States',
	'21137',
	'US',
	'City',
	'Saint Helena',
	'SH-HL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.9868
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1014760,
	'Marlborough,Connecticut,United States',
	'21139',
	'US',
	'City',
	'Marlborough',
	'NZ-MBH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.9918
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1014827,
	'Scotland,Connecticut,United States',
	'21139',
	'US',
	'City',
	'Scotland',
	'GB-SCT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:05.9987
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1015046,
	'Havana,Florida,United States',
	'21142',
	'US',
	'City',
	'Havana',
	'CU-03',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.0047
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1015691,
	'Cascade,Iowa,United States',
	'21145',
	'US',
	'City',
	'Cascade',
	'SC-11',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.0107
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1015819,
	'Hamburg,Iowa,United States',
	'21145',
	'US',
	'City',
	'Hamburg',
	'DE-HH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.0157
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1015940,
	'Nevada,Iowa,United States',
	'21145',
	'US',
	'City',
	'Nevada',
	'US-NV',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.0227
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1016070,
	'Tripoli,Iowa,United States',
	'21145',
	'US',
	'City',
	'Tripoli',
	'LY-TB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.0277
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1016212,
	'Paul,Idaho,United States',
	'21146',
	'US',
	'City',
	'Paul',
	'CV-PA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.0356
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1016397,
	'Cuba,Illinois,United States',
	'21147',
	'US',
	'City',
	'Cuba',
	'CU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.0406
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1016592,
	'Kansas,Illinois,United States',
	'21147',
	'US',
	'City',
	'Kansas',
	'US-KS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.0476
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1016857,
	'South Holland,Illinois,United States',
	'21147',
	'US',
	'City',
	'South Holland',
	'NL-ZH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.0526
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1017186,
	'Lucerne,Indiana,United States',
	'21148',
	'US',
	'City',
	'Lucerne',
	'CH-LU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.0596
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1017662,
	'Victoria,Kansas,United States',
	'21149',
	'US',
	'City',
	'Victoria',
	'AU-VIC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.0656
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1017665,
	'Washington,Kansas,United States',
	'21149',
	'US',
	'City',
	'Washington',
	'US-WA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.0715
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1018064,
	'Saint Joseph,Louisiana,United States',
	'21151',
	'US',
	'City',
	'Saint Joseph',
	'BB-06',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.0785
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1018120,
	'Berlin,Massachusetts,United States',
	'21152',
	'US',
	'City',
	'Berlin',
	'DE-BE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.0845
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1018531,
	'California,Maryland,United States',
	'21153',
	'US',
	'City',
	'California',
	'US-CA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.0895
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1018640,
	'Lisbon,Maryland,United States',
	'21153',
	'US',
	'City',
	'Lisbon',
	'PT-11',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.0965
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1018732,
	'Vienna,Maryland,United States',
	'21153',
	'US',
	'City',
	'Vienna',
	'AT-9',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.1025
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1018784,
	'Bremen,Maine,United States',
	'21154',
	'US',
	'City',
	'Bremen',
	'DE-HB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.1084
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1018927,
	'Long Island,Maine,United States',
	'21154',
	'US',
	'City',
	'Long Island',
	'BS-LI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.1144
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1019208,
	'Charlotte,Michigan,United States',
	'21155',
	'US',
	'City',
	'Charlotte',
	'VC-01',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.1204
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1019225,
	'Colon,Michigan,United States',
	'21155',
	'US',
	'City',
	'Colon',
	'HN-CL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.1264
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1019231,
	'Constantine,Michigan,United States',
	'21155',
	'US',
	'City',
	'Constantine',
	'DZ-25',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.1374
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1019673,
	'Zeeland,Michigan,United States',
	'21155',
	'US',
	'City',
	'Zeeland',
	'NL-ZE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.1434
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1019681,
	'Alberta,Minnesota,United States',
	'21156',
	'US',
	'City',
	'Alberta',
	'CA-AB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.1493
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1020015,
	'Oslo,Minnesota,United States',
	'21156',
	'US',
	'City',
	'Oslo',
	'NO-03',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.1553
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1020088,
	'Saint James,Minnesota,United States',
	'21156',
	'US',
	'City',
	'Saint James',
	'BB-04',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.1613
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1020090,
	'Saint Paul,Minnesota,United States',
	'21156',
	'US',
	'City',
	'Saint Paul',
	'AG-06',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.1683
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1020120,
	'Virginia,Minnesota,United States',
	'21156',
	'US',
	'City',
	'Virginia',
	'US-VA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.1733
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1020204,
	'Bolivar,Missouri,United States',
	'21157',
	'US',
	'City',
	'Bolivar',
	'CO-BOL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.1803
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1020234,
	'Callao,Missouri,United States',
	'21157',
	'US',
	'City',
	'Callao',
	'PE-CAL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.1863
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1021549,
	'Saint John,North Dakota,United States',
	'21161',
	'US',
	'City',
	'Saint John',
	'GD-04',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.1923
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1021661,
	'Geneva,Nebraska,United States',
	'21162',
	'US',
	'City',
	'Geneva',
	'CH-GE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.1982
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1021819,
	'Canterbury,New Hampshire,United States',
	'21163',
	'US',
	'City',
	'Canterbury',
	'NZ-CAN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.2052
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1022040,
	'Bogota,New Jersey,United States',
	'21164',
	'US',
	'City',
	'Bogota',
	'CO-DC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.2102
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1022083,
	'Colonia,New Jersey,United States',
	'21164',
	'US',
	'Neighborhood',
	'Colonia',
	'UY-CO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.2162
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1022294,
	'New Brunswick,New Jersey,United States',
	'21164',
	'US',
	'City',
	'New Brunswick',
	'CA-NB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.2222
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1022556,
	'Magdalena,New Mexico,United States',
	'21165',
	'US',
	'City',
	'Magdalena',
	'CO-MAG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.2292
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1022848,
	'Cross River,New York,United States',
	'21167',
	'US',
	'City',
	'Cross River',
	'NG-CR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.2353
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1022856,
	'Delhi,New York,United States',
	'21167',
	'US',
	'City',
	'Delhi',
	'IN-DL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.2423
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1023191,
	'New York,New York,United States',
	'21167',
	'US',
	'City',
	'New York',
	'US-NY',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.2483
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1023228,
	'Ontario,New York,United States',
	'21167',
	'US',
	'City',
	'Ontario',
	'CA-ON',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.2563
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1023660,
	'Delaware,Ohio,United States',
	'21168',
	'US',
	'City',
	'Delaware',
	'US-DE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.2672
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1023980,
	'Sardinia,Ohio,United States',
	'21168',
	'US',
	'City',
	'Sardinia',
	'IT-88',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.2782
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1024071,
	'Wellington,Ohio,United States',
	'21168',
	'US',
	'City',
	'Wellington',
	'NZ-WGN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.2852
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1024170,
	'Copan,Oklahoma,United States',
	'21169',
	'US',
	'City',
	'Copan',
	'HN-CP',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.2922
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1024790,
	'Dauphin,Pennsylvania,United States',
	'21171',
	'US',
	'City',
	'Dauphin',
	'LC-04',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.2992
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1025312,
	'Saint Michael,Pennsylvania,United States',
	'21171',
	'US',
	'City',
	'Saint Michael',
	'BB-08',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.3061
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1025626,
	'North,South Carolina,United States',
	'21173',
	'US',
	'City',
	'North',
	'LB-AS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.3121
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1025732,
	'Corsica,South Dakota,United States',
	'21174',
	'US',
	'City',
	'Corsica',
	'FR-H',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.3191
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1026824,
	'Sudan,Texas,United States',
	'21176',
	'US',
	'City',
	'Sudan',
	'SD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.3251
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1027704,
	'Pasco,Washington,United States',
	'21180',
	'US',
	'City',
	'Pasco',
	'PE-PAS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.3321
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1028258,
	'Wales,Wisconsin,United States',
	'21182',
	'US',
	'City',
	'Wales',
	'GB-WLS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.3380
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1028538,
	'Barinas,Barinas,Venezuela',
	'21192',
	'VE',
	'City',
	'Barinas',
	'VE-E',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.3450
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1028580,
	'Hanoi,Hanoi,Vietnam',
	'9040331',
	'VN',
	'City',
	'Hanoi',
	'VN-HN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.3510
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1028585,
	'Sana''a,Capital Municipality,Yemen',
	'9070451',
	'YE',
	'City',
	'Sana''a',
	'YE-SN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.3580
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1028586,
	'Bar,Bar Municipality,Montenegro',
	'9075711',
	'ME',
	'City',
	'Bar',
	'ME-02',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.3640
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1028587,
	'Budva,Budva Municipality,Montenegro',
	'9070421',
	'ME',
	'City',
	'Budva',
	'ME-05',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.3710
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1028588,
	'Herceg Novi,Herceg Novi Municipality,Montenegro',
	'9070030',
	'ME',
	'City',
	'Herceg Novi',
	'ME-08',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.3769
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1028591,
	'Podgorica,Podgorica Municipality,Montenegro',
	'9070032',
	'ME',
	'City',
	'Podgorica',
	'ME-16',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.3829
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1028592,
	'Ulcinj,Ulcinj Municipality,Montenegro',
	'9070420',
	'ME',
	'City',
	'Ulcinj',
	'ME-20',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.3899
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1028770,
	'Lusaka,Lusaka Province,Zambia',
	'9070044',
	'ZM',
	'City',
	'Lusaka',
	'ZM-09',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.3969
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1028772,
	'Bulawayo,Bulawayo Province,Zimbabwe',
	'9070026',
	'ZW',
	'City',
	'Bulawayo',
	'ZW-BU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.4039
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1028774,
	'Harare,Harare Province,Zimbabwe',
	'9070024',
	'ZW',
	'City',
	'Harare',
	'ZW-HA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.4109
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1028775,
	'Masvingo,Masvingo Province,Zimbabwe',
	'9070021',
	'ZW',
	'City',
	'Masvingo',
	'ZW-MV',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.4168
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1028796,
	'Delchevo,Municipality of Delcevo,Macedonia (FYROM)',
	'9075767',
	'MK',
	'City',
	'Delchevo',
	'MK-23',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.4228
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1028809,
	'Da Nang,Da Nang,Vietnam',
	'9047170',
	'VN',
	'City',
	'Da Nang',
	'VN-DN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.4298
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1028878,
	'Najran,Najran,Saudi Arabia',
	'21454',
	'SA',
	'City',
	'Najran',
	'SA-10',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.4358
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1028880,
	'Jazan,Jazan,Saudi Arabia',
	'21453',
	'SA',
	'City',
	'Jazan',
	'SA-09',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.4428
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1029269,
	'Arauca,Arauca,Colombia',
	'21507',
	'CO',
	'City',
	'Arauca',
	'CO-ARA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.4497
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1029300,
	'Corozal,Sucre,Colombia',
	'21521',
	'CO',
	'City',
	'Corozal',
	'BZ-CZL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.4557
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1029314,
	'Florida,Valle del Cauca,Colombia',
	'20211',
	'CO',
	'City',
	'Florida',
	'US-FL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.4617
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1029341,
	'La Union,Narino,Colombia',
	'20206',
	'CO',
	'City',
	'La Union',
	'SV-UN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.4677
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1029422,
	'Sucre,Sucre,Colombia',
	'21521',
	'CO',
	'City',
	'Sucre',
	'VE-R',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.4737
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1029583,
	'Bali,Taraba,Nigeria',
	'21574',
	'NG',
	'City',
	'Bali',
	'ID-BA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.4797
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1029587,
	'Bauchi,Bauchi,Nigeria',
	'21545',
	'NG',
	'City',
	'Bauchi',
	'NG-BA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.4876
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1029612,
	'Donga,Taraba,Nigeria',
	'21574',
	'NG',
	'City',
	'Donga',
	'BJ-DO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.4936
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1029629,
	'Gombe,Gombe,Nigeria',
	'21555',
	'NG',
	'City',
	'Gombe',
	'NG-GO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.5006
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1029684,
	'Katsina,Katsina,Nigeria',
	'21560',
	'NG',
	'City',
	'Katsina',
	'NG-KT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.5056
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1029728,
	'Ondo,Ondo,Nigeria',
	'21568',
	'NG',
	'City',
	'Ondo',
	'NG-ON',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.5116
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1029733,
	'Oyo,Oyo,Nigeria',
	'21570',
	'NG',
	'City',
	'Oyo',
	'NG-OY',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.5176
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1029886,
	'Valle,Aust-Agder,Norway',
	'20779',
	'NO',
	'City',
	'Valle',
	'HN-VA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.5236
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1031172,
	'Ascension,Chihuahua,Mexico',
	'20699',
	'MX',
	'City',
	'Ascension',
	'SH-AC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.5295
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1031267,
	'Loreto,Baja California Sur,Mexico',
	'20697',
	'MX',
	'City',
	'Loreto',
	'PE-LOR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.5355
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1031329,
	'San Fernando,Tamaulipas,Mexico',
	'20722',
	'MX',
	'City',
	'San Fernando',
	'TT-SFO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.5416
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1031488,
	'Boa Vista,State of Roraima,Brazil',
	'21228',
	'BR',
	'City',
	'Boa Vista',
	'CV-BV',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.5475
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1031672,
	'Guaira,State of Parana,Brazil',
	'20101',
	'BR',
	'City',
	'Guaira',
	'PY-4',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.5536
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1031984,
	'Salto,State of Sao Paulo,Brazil',
	'20106',
	'BR',
	'City',
	'Salto',
	'UY-SA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.5596
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	1032105,
	'Toledo,State of Parana,Brazil',
	'20101',
	'BR',
	'City',
	'Toledo',
	'BZ-TOL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.5655
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040100,
	'Kyustendil Province,Bulgaria',
	'2100',
	'BG',
	'City',
	'Kyustendil Province',
	'BG-10',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.5715
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040101,
	'Pernik,Bulgaria',
	'2100',
	'BG',
	'Province',
	'Pernik',
	'BG-14',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.5776
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040105,
	'Blagoevgrad Province,Bulgaria',
	'2100',
	'BG',
	'Province',
	'Blagoevgrad Province',
	'BG-01',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.5836
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040106,
	'Smoljan,Bulgaria',
	'2100',
	'BG',
	'Province',
	'Smoljan',
	'BG-21',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.5897
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040109,
	'Plovdiv Province,Bulgaria',
	'2100',
	'BG',
	'City',
	'Plovdiv Province',
	'BG-16',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.5967
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040111,
	'Kardzhali Province,Bulgaria',
	'2100',
	'BG',
	'City',
	'Kardzhali Province',
	'BG-09',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.6047
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040114,
	'Haskovo Province,Bulgaria',
	'2100',
	'BG',
	'City',
	'Haskovo Province',
	'BG-26',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.6107
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040115,
	'Dobrich Province,Bulgaria',
	'2100',
	'BG',
	'Province',
	'Dobrich Province',
	'BG-08',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.6167
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040116,
	'Shumen Province,Bulgaria',
	'2100',
	'BG',
	'Province',
	'Shumen Province',
	'BG-27',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.6226
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040118,
	'Varna,Bulgaria',
	'2100',
	'BG',
	'City',
	'Varna',
	'BG-03',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.6296
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040119,
	'Sliven Province,Bulgaria',
	'2100',
	'BG',
	'Province',
	'Sliven Province',
	'BG-20',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.6346
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040121,
	'Burgas,Bulgaria',
	'2100',
	'BG',
	'Province',
	'Burgas',
	'BG-02',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.6416
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040122,
	'Jambol,Bulgaria',
	'2100',
	'BG',
	'Province',
	'Jambol',
	'BG-28',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.6486
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040123,
	'Stara Zagora,Bulgaria',
	'2100',
	'BG',
	'Province',
	'Stara Zagora',
	'BG-24',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.6546
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040125,
	'Gabrovo,Bulgaria',
	'2100',
	'BG',
	'Province',
	'Gabrovo',
	'BG-07',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.6655
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040127,
	'Veliko Tarnovo Province,Bulgaria',
	'2100',
	'BG',
	'City',
	'Veliko Tarnovo Province',
	'BG-04',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.6735
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040130,
	'Sofia Province,Bulgaria',
	'2100',
	'BG',
	'City',
	'Sofia Province',
	'BG-23',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.6825
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040131,
	'Vraca,Bulgaria',
	'2100',
	'BG',
	'Province',
	'Vraca',
	'BG-06',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.6895
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040133,
	'Lovec,Bulgaria',
	'2100',
	'BG',
	'Province',
	'Lovec',
	'BG-11',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.6964
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040134,
	'Pleven Province,Bulgaria',
	'2100',
	'BG',
	'City',
	'Pleven Province',
	'BG-15',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.7024
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040139,
	'Targovishte Province,Bulgaria',
	'2100',
	'BG',
	'City',
	'Targovishte Province',
	'BG-25',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.7084
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040140,
	'Razgrad,Bulgaria',
	'2100',
	'BG',
	'Province',
	'Razgrad',
	'BG-17',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.7194
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040142,
	'Silistra,Bulgaria',
	'2100',
	'BG',
	'Province',
	'Silistra',
	'BG-19',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.7264
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040146,
	'Montana Province,Bulgaria',
	'2100',
	'BG',
	'Province',
	'Montana Province',
	'BG-12',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.7333
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040314,
	'Kaohsiung City,Taiwan',
	'2158',
	'TW',
	'City',
	'Kaohsiung City',
	'TW-KHH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.7393
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040323,
	'Kien Giang,Vietnam',
	'2704',
	'VN',
	'Province',
	'Kien Giang',
	'VN-47',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.7463
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040324,
	'An Giang Province,Vietnam',
	'2704',
	'VN',
	'Province',
	'An Giang Province',
	'VN-44',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.7523
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040329,
	'Tien Giang,Vietnam',
	'2704',
	'VN',
	'Province',
	'Tien Giang',
	'VN-46',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.7583
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040330,
	'Thai Nguyen,Vietnam',
	'2704',
	'VN',
	'Province',
	'Thai Nguyen',
	'VN-69',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.7633
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040335,
	'Thai Binh,Thai Binh,Vietnam',
	'9040336',
	'VN',
	'City',
	'Thai Binh',
	'VN-20',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.7682
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040337,
	'Nam Dinh,Vietnam',
	'2704',
	'VN',
	'Province',
	'Nam Dinh',
	'VN-67',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.7742
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040341,
	'Thanh Hoa,Thanh Hoa,Vietnam',
	'9040340',
	'VN',
	'City',
	'Thanh Hoa',
	'VN-21',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.7812
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040342,
	'Ha Tinh,Vietnam',
	'2704',
	'VN',
	'Province',
	'Ha Tinh',
	'VN-23',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.7882
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040344,
	'Quang Binh Province,Vietnam',
	'2704',
	'VN',
	'Province',
	'Quang Binh Province',
	'VN-24',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.7932
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040345,
	'Nghe An,Vietnam',
	'2704',
	'VN',
	'Province',
	'Nghe An',
	'VN-22',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.8022
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040349,
	'Thua Thien Hue,Vietnam',
	'2704',
	'VN',
	'Province',
	'Thua Thien Hue',
	'VN-26',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.8091
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040351,
	'Quang Nam Province,Vietnam',
	'2704',
	'VN',
	'Province',
	'Quang Nam Province',
	'VN-27',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.8151
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040354,
	'Hai Duong,Vietnam',
	'2704',
	'VN',
	'Province',
	'Hai Duong',
	'VN-61',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.8241
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040355,
	'Quang Ngai,Vietnam',
	'2704',
	'VN',
	'Province',
	'Quang Ngai',
	'VN-29',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.8301
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040356,
	'Kon Tum Province,Vietnam',
	'2704',
	'VN',
	'Province',
	'Kon Tum Province',
	'VN-28',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.8361
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040359,
	'Gia Lai,Vietnam',
	'2704',
	'VN',
	'Province',
	'Gia Lai',
	'VN-30',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.8441
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040360,
	'Binh Dinh Province,Vietnam',
	'2704',
	'VN',
	'Province',
	'Binh Dinh Province',
	'VN-31',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.8501
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040364,
	'Khanh Hoa Province,Vietnam',
	'2704',
	'VN',
	'Province',
	'Khanh Hoa Province',
	'VN-34',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.8570
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040365,
	'Ninh Thuan Province,Vietnam',
	'2704',
	'VN',
	'Province',
	'Ninh Thuan Province',
	'VN-36',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.8630
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040372,
	'Dong Nai,Vietnam',
	'2704',
	'VN',
	'Province',
	'Dong Nai',
	'VN-39',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.8712
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040373,
	'Ho Chi Minh City,Vietnam',
	'2704',
	'VN',
	'Province',
	'Ho Chi Minh City',
	'VN-SG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.8772
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040374,
	'Ba Ria - Vung Tau,Vietnam',
	'2704',
	'VN',
	'Province',
	'Ba Ria - Vung Tau',
	'VN-43',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.8832
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040376,
	'Binh Thuan Province,Vietnam',
	'2704',
	'VN',
	'Province',
	'Binh Thuan Province',
	'VN-40',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.8922
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040377,
	'Can Tho,Vietnam',
	'2704',
	'VN',
	'Province',
	'Can Tho',
	'VN-CT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.8982
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040379,
	'Taipei City,Taiwan',
	'2158',
	'TW',
	'City',
	'Taipei City',
	'TW-TPE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.9052
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040380,
	'Taichung City,Taiwan',
	'2158',
	'TW',
	'City',
	'Taichung City',
	'TW-TXG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.9112
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9040859,
	'Nord,Hauts-de-France,France',
	'9068898',
	'FR',
	'Department',
	'Nord',
	'HT-ND',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.9172
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9041082,
	'Abu Dhabi,United Arab Emirates',
	'2784',
	'AE',
	'Province',
	'Abu Dhabi',
	'AE-AZ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.9232
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9041083,
	'Dubai,United Arab Emirates',
	'2784',
	'AE',
	'Province',
	'Dubai',
	'AE-DU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.9302
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047015,
	'Midlands,England,United Kingdom',
	'20339',
	'GB',
	'TV Region',
	'Midlands',
	'ZW-MI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.9362
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047040,
	'Province of Asturias,Asturias,Spain',
	'20286',
	'ES',
	'Province',
	'Asturias',
	'ES-AS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.9421
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047050,
	'Province of Cantabria,Cantabria,Spain',
	'20290',
	'ES',
	'Province',
	'Cantabria',
	'ES-CB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.9491
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047051,
	'Province of Navarre,Navarre,Spain',
	'20285',
	'ES',
	'Province',
	'Navarre',
	'ES-NC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.9551
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047053,
	'Province of La Rioja,La Rioja,Spain',
	'20281',
	'ES',
	'Province',
	'La Rioja',
	'ES-RI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.9611
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047098,
	'Ras al Khaimah,United Arab Emirates',
	'2784',
	'AE',
	'Province',
	'Ras al Khaimah',
	'AE-RK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.9681
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047101,
	'Sofia City Province,Bulgaria',
	'2100',
	'BG',
	'Province',
	'Sofia City Province',
	'BG-22',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.9751
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047103,
	'Arica y Parinacota,Chile',
	'2152',
	'CL',
	'Region',
	'Arica y Parinacota',
	'CL-AP',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.9811
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047104,
	'Atacama,Chile',
	'2152',
	'CL',
	'Region',
	'Atacama',
	'CL-AT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.9871
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047106,
	'Chatham Islands,New Zealand',
	'2554',
	'NZ',
	'Region',
	'Chatham Islands',
	'NZ-CIT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:06.9941
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047107,
	'Vila Real District,Portugal',
	'2620',
	'PT',
	'Region',
	'Vila Real District',
	'PT-17',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.0011
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047108,
	'Ilfov County,Romania',
	'2642',
	'RO',
	'County',
	'Ilfov County',
	'RO-IF',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.0072
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047109,
	'Zabaykalsky Krai,Russia',
	'2643',
	'RU',
	'Region',
	'Zabaykalsky Krai',
	'RU-ZAB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.0132
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047110,
	'Amnat Charoen,Thailand',
	'2764',
	'TH',
	'Province',
	'Amnat Charoen',
	'TH-37',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.0202
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047111,
	'Ang Thong,Thailand',
	'2764',
	'TH',
	'Province',
	'Ang Thong',
	'TH-15',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.0272
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047112,
	'Bueng Kan,Thailand',
	'2764',
	'TH',
	'Province',
	'Bueng Kan',
	'TH-38',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.0331
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047113,
	'Buri Ram,Thailand',
	'2764',
	'TH',
	'Province',
	'Buri Ram',
	'TH-31',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.0411
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047114,
	'Chai Nat,Thailand',
	'2764',
	'TH',
	'Province',
	'Chai Nat',
	'TH-18',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.0481
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047117,
	'Chumphon,Thailand',
	'2764',
	'TH',
	'Province',
	'Chumphon',
	'TH-86',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.0541
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047118,
	'Kalasin,Thailand',
	'2764',
	'TH',
	'Province',
	'Kalasin',
	'TH-46',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.0601
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047119,
	'Kamphaeng Phet,Thailand',
	'2764',
	'TH',
	'Province',
	'Kamphaeng Phet',
	'TH-62',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.0670
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047120,
	'Kanchanaburi,Thailand',
	'2764',
	'TH',
	'Province',
	'Kanchanaburi',
	'TH-71',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.0720
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047121,
	'Krabi,Thailand',
	'2764',
	'TH',
	'Province',
	'Krabi',
	'TH-81',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.0780
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047122,
	'Lamphun,Thailand',
	'2764',
	'TH',
	'Province',
	'Lamphun',
	'TH-51',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.0830
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047123,
	'Loei,Thailand',
	'2764',
	'TH',
	'Province',
	'Loei',
	'TH-42',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.0890
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047124,
	'Lopburi,Thailand',
	'2764',
	'TH',
	'Province',
	'Lopburi',
	'TH-16',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.0950
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047125,
	'Mae Hong Son,Thailand',
	'2764',
	'TH',
	'Province',
	'Mae Hong Son',
	'TH-58',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.1010
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047126,
	'Maha Sarakham,Thailand',
	'2764',
	'TH',
	'Province',
	'Maha Sarakham',
	'TH-44',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.1069
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047127,
	'Mukdahan,Thailand',
	'2764',
	'TH',
	'Province',
	'Mukdahan',
	'TH-49',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.1129
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047128,
	'Nakhon Nayok,Thailand',
	'2764',
	'TH',
	'Province',
	'Nakhon Nayok',
	'TH-26',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.1189
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047129,
	'Nakhon Phanom,Thailand',
	'2764',
	'TH',
	'Province',
	'Nakhon Phanom',
	'TH-48',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.1249
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047131,
	'Nan,Thailand',
	'2764',
	'TH',
	'Province',
	'Nan',
	'TH-55',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.1309
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047132,
	'Narathiwat,Thailand',
	'2764',
	'TH',
	'Province',
	'Narathiwat',
	'TH-96',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.1359
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047133,
	'Nong Bua Lam Phu,Thailand',
	'2764',
	'TH',
	'Province',
	'Nong Bua Lam Phu',
	'TH-39',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.1418
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047134,
	'Nong Khai,Thailand',
	'2764',
	'TH',
	'Province',
	'Nong Khai',
	'TH-43',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.1468
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047135,
	'Phang-nga,Thailand',
	'2764',
	'TH',
	'Province',
	'Phang-nga',
	'TH-82',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.1548
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047136,
	'Phatthalung,Thailand',
	'2764',
	'TH',
	'Province',
	'Phatthalung',
	'TH-93',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.1618
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047137,
	'Phayao,Thailand',
	'2764',
	'TH',
	'Province',
	'Phayao',
	'TH-56',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.1668
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047138,
	'Phetchabun,Thailand',
	'2764',
	'TH',
	'Province',
	'Phetchabun',
	'TH-67',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.1738
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047139,
	'Phetchaburi,Thailand',
	'2764',
	'TH',
	'Province',
	'Phetchaburi',
	'TH-76',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.1797
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047140,
	'Phichit,Thailand',
	'2764',
	'TH',
	'Province',
	'Phichit',
	'TH-66',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.1857
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047141,
	'Prachin Buri,Thailand',
	'2764',
	'TH',
	'Province',
	'Prachin Buri',
	'TH-25',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.1917
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047142,
	'Ratchaburi,Thailand',
	'2764',
	'TH',
	'Province',
	'Ratchaburi',
	'TH-70',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.1978
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047144,
	'Roi Et,Thailand',
	'2764',
	'TH',
	'Province',
	'Roi Et',
	'TH-45',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.2047
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047145,
	'Sa Kaeo,Thailand',
	'2764',
	'TH',
	'Province',
	'Sa Kaeo',
	'TH-27',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.2107
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047146,
	'Sakon Nakhon,Thailand',
	'2764',
	'TH',
	'Province',
	'Sakon Nakhon',
	'TH-47',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.2177
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047147,
	'Samut Prakan,Thailand',
	'2764',
	'TH',
	'Province',
	'Samut Prakan',
	'TH-11',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.2237
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047148,
	'Samut Songkhram,Thailand',
	'2764',
	'TH',
	'Province',
	'Samut Songkhram',
	'TH-75',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.2297
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047149,
	'Satun,Thailand',
	'2764',
	'TH',
	'Province',
	'Satun',
	'TH-91',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.2357
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047150,
	'Si Sa Ket,Thailand',
	'2764',
	'TH',
	'Province',
	'Si Sa Ket',
	'TH-33',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.2416
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047151,
	'Sing Buri,Thailand',
	'2764',
	'TH',
	'Province',
	'Sing Buri',
	'TH-17',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.2476
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047152,
	'Sukhothai,Thailand',
	'2764',
	'TH',
	'Province',
	'Sukhothai',
	'TH-64',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.2547
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047153,
	'Suphan Buri,Thailand',
	'2764',
	'TH',
	'Province',
	'Suphan Buri',
	'TH-72',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.2606
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047155,
	'Surin,Thailand',
	'2764',
	'TH',
	'Province',
	'Surin',
	'TH-32',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.2666
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047156,
	'Tak,Thailand',
	'2764',
	'TH',
	'Province',
	'Tak',
	'TH-63',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.2736
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047157,
	'Trat,Thailand',
	'2764',
	'TH',
	'Province',
	'Trat',
	'TH-23',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.2796
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047159,
	'Uthai Thani,Thailand',
	'2764',
	'TH',
	'Province',
	'Uthai Thani',
	'TH-61',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.2856
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047160,
	'Uttaradit,Thailand',
	'2764',
	'TH',
	'Province',
	'Uttaradit',
	'TH-53',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.2937
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047161,
	'Bac Giang,Vietnam',
	'2704',
	'VN',
	'Province',
	'Bac Giang',
	'VN-54',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.2987
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047163,
	'Bac Lieu,Vietnam',
	'2704',
	'VN',
	'Province',
	'Bac Lieu',
	'VN-55',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.3048
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047164,
	'Bac Ninh Province,Vietnam',
	'2704',
	'VN',
	'Province',
	'Bac Ninh Province',
	'VN-56',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.3117
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047165,
	'Ben Tre,Vietnam',
	'2704',
	'VN',
	'Province',
	'Ben Tre',
	'VN-50',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.3177
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047166,
	'Binh Duong,Vietnam',
	'2704',
	'VN',
	'Province',
	'Binh Duong',
	'VN-57',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.3247
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047167,
	'Binh Phuoc,Vietnam',
	'2704',
	'VN',
	'Province',
	'Binh Phuoc',
	'VN-58',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.3317
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047171,
	'Dak Nong,Vietnam',
	'2704',
	'VN',
	'Province',
	'Dak Nong',
	'VN-72',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.3377
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047172,
	'Dien Bien,Vietnam',
	'2704',
	'VN',
	'Province',
	'Dien Bien',
	'VN-71',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.3457
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047173,
	'Ha Giang,Vietnam',
	'2704',
	'VN',
	'Province',
	'Ha Giang',
	'VN-03',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.3526
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047175,
	'Hau Giang,Vietnam',
	'2704',
	'VN',
	'Province',
	'Hau Giang',
	'VN-73',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.3576
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047176,
	'Hoa Binh,Vietnam',
	'2704',
	'VN',
	'Province',
	'Hoa Binh',
	'VN-14',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.3656
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047177,
	'Hung Yen,Vietnam',
	'2704',
	'VN',
	'Province',
	'Hung Yen',
	'VN-66',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.3726
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047179,
	'Lang Son,Vietnam',
	'2704',
	'VN',
	'Province',
	'Lang Son',
	'VN-09',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.3796
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047181,
	'Long An Province,Vietnam',
	'2704',
	'VN',
	'Province',
	'Long An Province',
	'VN-41',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.3867
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047182,
	'Phu Tho Province,Vietnam',
	'2704',
	'VN',
	'Province',
	'Phu Tho Province',
	'VN-68',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.3928
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047186,
	'Tra Vinh,Vietnam',
	'2704',
	'VN',
	'Province',
	'Tra Vinh',
	'VN-51',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.3988
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047188,
	'Vinh Long,Vietnam',
	'2704',
	'VN',
	'Province',
	'Vinh Long',
	'VN-49',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.4058
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047189,
	'Vinh Phuc Province,Vietnam',
	'2704',
	'VN',
	'Province',
	'Vinh Phuc Province',
	'VN-70',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.4118
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047190,
	'Yen Bai,Vietnam',
	'2704',
	'VN',
	'Province',
	'Yen Bai',
	'VN-06',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.4188
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047577,
	'Fujairah,Fujairah,United Arab Emirates',
	'9047097',
	'AE',
	'City',
	'Fujairah',
	'AE-FU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.4248
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047580,
	'Sharjah,Sharjah,United Arab Emirates',
	'9047099',
	'AE',
	'City',
	'Sharjah',
	'AE-SH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.4318
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047807,
	'Santa Cruz,State of Rio Grande do Norte,Brazil',
	'20103',
	'BR',
	'City',
	'Santa Cruz',
	'CV-CR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.4398
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9047817,
	'Sao Miguel,State of Rio Grande do Norte,Brazil',
	'20103',
	'BR',
	'City',
	'Sao Miguel',
	'CV-SM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.4457
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9049055,
	'Durango,Basque Country,Spain',
	'20289',
	'ES',
	'City',
	'Durango',
	'MX-DUR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.4537
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9049116,
	'Loja,Andalusia,Spain',
	'20269',
	'ES',
	'City',
	'Loja',
	'EC-L',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.4617
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9049426,
	'Castries,Occitanie,France',
	'9068897',
	'FR',
	'City',
	'Castries',
	'LC-02',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.4687
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9050548,
	'Bari,Apulia,Italy',
	'9053425',
	'IT',
	'Municipality',
	'Bari',
	'SO-BR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.4757
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9051588,
	'Berea,South Carolina,United States',
	'21173',
	'US',
	'City',
	'Berea',
	'LS-D',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.4816
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9051710,
	'Central,Louisiana,United States',
	'21151',
	'US',
	'City',
	'Central',
	'ZM-02',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.4876
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9051994,
	'Freeport,Maine,United States',
	'21154',
	'US',
	'City',
	'Freeport',
	'BS-FP',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.4936
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9052366,
	'Madeira,Ohio,United States',
	'21168',
	'US',
	'City',
	'Madeira',
	'PT-30',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.5007
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9052484,
	'Mount Lebanon,Pennsylvania,United States',
	'21171',
	'US',
	'City',
	'Mount Lebanon',
	'LB-JL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.5067
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9052713,
	'Prague,Oklahoma,United States',
	'21169',
	'US',
	'City',
	'Prague',
	'CZ-PR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.5126
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9053219,
	'Wyoming,Ohio,United States',
	'21168',
	'US',
	'City',
	'Wyoming',
	'US-WY',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.5186
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9053232,
	'Ca Mau,Ca Mau,Vietnam',
	'9047168',
	'VN',
	'City',
	'Ca Mau',
	'VN-59',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.5246
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9053236,
	'Soc Trang,Soc Trang,Vietnam',
	'9047183',
	'VN',
	'City',
	'Soc Trang',
	'VN-52',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.5306
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9053417,
	'Yoro,Gifu,Japan',
	'20644',
	'JP',
	'City',
	'Yoro',
	'HN-YO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.5376
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9053423,
	'Abruzzo,Italy',
	'2380',
	'IT',
	'Region',
	'Abruzzo',
	'IT-65',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.5446
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9053425,
	'Apulia,Italy',
	'2380',
	'IT',
	'Region',
	'Apulia',
	'IT-75',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.5515
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9053426,
	'Basilicata,Italy',
	'2380',
	'IT',
	'Region',
	'Basilicata',
	'IT-77',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.5585
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9053427,
	'Calabria,Italy',
	'2380',
	'IT',
	'Region',
	'Calabria',
	'IT-78',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.5655
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9053428,
	'Campania,Italy',
	'2380',
	'IT',
	'Region',
	'Campania',
	'IT-72',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.5725
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9053429,
	'Emilia-Romagna,Italy',
	'2380',
	'IT',
	'Region',
	'Emilia-Romagna',
	'IT-45',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.5785
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9053430,
	'Friuli-Venezia Giulia,Italy',
	'2380',
	'IT',
	'Region',
	'Friuli-Venezia Giulia',
	'IT-36',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.5855
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9053431,
	'Lazio,Italy',
	'2380',
	'IT',
	'Region',
	'Lazio',
	'IT-62',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.5914
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9053432,
	'Liguria,Italy',
	'2380',
	'IT',
	'Region',
	'Liguria',
	'IT-42',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.5984
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9053433,
	'Lombardy,Italy',
	'2380',
	'IT',
	'Region',
	'Lombardy',
	'IT-25',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.6044
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9053434,
	'Marche,Italy',
	'2380',
	'IT',
	'Region',
	'Marche',
	'IT-57',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.6104
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9053435,
	'Molise,Italy',
	'2380',
	'IT',
	'Region',
	'Molise',
	'IT-67',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.6164
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9053438,
	'Sicily,Italy',
	'2380',
	'IT',
	'Region',
	'Sicily',
	'IT-82',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.6243
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9053440,
	'Tuscany,Italy',
	'2380',
	'IT',
	'Region',
	'Tuscany',
	'IT-52',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.6303
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9053441,
	'Umbria,Italy',
	'2380',
	'IT',
	'Region',
	'Umbria',
	'IT-55',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.6363
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9053442,
	'Veneto,Italy',
	'2380',
	'IT',
	'Region',
	'Veneto',
	'IT-34',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.6423
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9056636,
	'Bangka Belitung Islands,Indonesia',
	'2360',
	'ID',
	'Province',
	'Bangka Belitung Islands',
	'ID-BB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.6483
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9056637,
	'Banten,Indonesia',
	'2360',
	'ID',
	'Province',
	'Banten',
	'ID-BT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.6533
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9056638,
	'Bengkulu,Indonesia',
	'2360',
	'ID',
	'Province',
	'Bengkulu',
	'ID-BE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.6593
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9056639,
	'Central Kalimantan,Indonesia',
	'2360',
	'ID',
	'Province',
	'Central Kalimantan',
	'ID-KT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.6642
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9056640,
	'Central Sulawesi,Indonesia',
	'2360',
	'ID',
	'Province',
	'Central Sulawesi',
	'ID-ST',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.6712
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9056641,
	'East Nusa Tenggara,Indonesia',
	'2360',
	'ID',
	'Province',
	'East Nusa Tenggara',
	'ID-NT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.6772
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9056643,
	'Maluku,Indonesia',
	'2360',
	'ID',
	'Province',
	'Maluku',
	'ID-MA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.6832
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9056644,
	'North Maluku,Indonesia',
	'2360',
	'ID',
	'Province',
	'North Maluku',
	'ID-MU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.6892
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9056645,
	'Papua,Indonesia',
	'2360',
	'ID',
	'Province',
	'Papua',
	'ID-PA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.6952
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9056646,
	'Riau Islands,Indonesia',
	'2360',
	'ID',
	'Province',
	'Riau Islands',
	'ID-KR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.7021
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9056647,
	'South East Sulawesi,Indonesia',
	'2360',
	'ID',
	'Province',
	'South East Sulawesi',
	'ID-SG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.7081
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9056648,
	'West Kalimantan,Indonesia',
	'2360',
	'ID',
	'Province',
	'West Kalimantan',
	'ID-KB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.7151
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9056649,
	'West Papua,Indonesia',
	'2360',
	'ID',
	'Province',
	'West Papua',
	'ID-PB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.7201
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9056650,
	'West Sulawesi,Indonesia',
	'2360',
	'ID',
	'Province',
	'West Sulawesi',
	'ID-SR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.7261
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9056651,
	'West Sumatra,Indonesia',
	'2360',
	'ID',
	'Province',
	'West Sumatra',
	'ID-SB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.7321
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9056656,
	'Gorontalo,Gorontalo,Indonesia',
	'9056642',
	'ID',
	'City',
	'Gorontalo',
	'ID-GO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.7370
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9056711,
	'Aksaray,Aksaray,Turkey',
	'21377',
	'TR',
	'City',
	'Aksaray',
	'TR-68',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.7430
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9056724,
	'Artvin,Artvin,Turkey',
	'21356',
	'TR',
	'City',
	'Artvin',
	'TR-08',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.7490
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9056729,
	'Batman,Batman,Turkey',
	'21379',
	'TR',
	'City',
	'Batman',
	'TR-72',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.7540
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9056730,
	'Bayburt,Bayburt,Turkey',
	'21378',
	'TR',
	'City',
	'Bayburt',
	'TR-69',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.7600
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9056736,
	'Bitlis,Bitlis,Turkey',
	'21359',
	'TR',
	'City',
	'Bitlis',
	'TR-13',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.7660
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9056761,
	'Denizli,Denizli,Turkey',
	'21363',
	'TR',
	'City',
	'Denizli',
	'TR-20',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.7710
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9056770,
	'Edirne,Edirne,Turkey',
	'21364',
	'TR',
	'City',
	'Edirne',
	'TR-22',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.7779
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9056783,
	'Giresun,Giresun,Turkey',
	'21365',
	'TR',
	'City',
	'Giresun',
	'TR-28',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.7839
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9056815,
	'Kilis,Kilis,Turkey',
	'21579',
	'TR',
	'City',
	'Kilis',
	'TR-79',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.7899
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9056828,
	'Mardin,Mardin,Turkey',
	'21370',
	'TR',
	'City',
	'Mardin',
	'TR-47',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.7959
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9056849,
	'Osmaniye,Osmaniye,Turkey',
	'21384',
	'TR',
	'City',
	'Osmaniye',
	'TR-80',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.8139
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9056855,
	'Sakarya,Sakarya,Turkey',
	'21084',
	'TR',
	'City',
	'Sakarya',
	'TR-54',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.8200
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9056879,
	'Talas,Kayseri Province,Turkey',
	'21073',
	'TR',
	'City',
	'Talas',
	'KG-T',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.8270
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9056888,
	'Tunceli,Tunceli,Turkey',
	'21375',
	'TR',
	'City',
	'Tunceli',
	'TR-62',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.8332
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9060923,
	'Arequipa,Arequipa,Peru',
	'20797',
	'PE',
	'Province',
	'Arequipa',
	'PE-ARE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.8401
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9060925,
	'Trujillo,La Libertad,Peru',
	'20806',
	'PE',
	'Province',
	'Trujillo',
	'VE-T',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.8452
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9060927,
	'Cusco,Cusco,Peru',
	'20801',
	'PE',
	'Province',
	'Cusco',
	'PE-CUS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.8523
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9060935,
	'Puno,Puno,Peru',
	'20813',
	'PE',
	'Province',
	'Puno',
	'PE-PUN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.8583
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9060937,
	'Huanuco,Huanuco,Peru',
	'20802',
	'PE',
	'Province',
	'Huanuco',
	'PE-HUC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.8643
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9060939,
	'San Martin,San Martin,Peru',
	'20814',
	'PE',
	'Province',
	'San Martin',
	'PE-SAM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.8703
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9061085,
	'Bel Air,California,United States',
	'21137',
	'US',
	'Neighborhood',
	'Bel Air',
	'SC-09',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.8783
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9061336,
	'Apurimac,Peru',
	'2604',
	'PE',
	'Region',
	'Apurimac',
	'PE-APU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.8843
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9061337,
	'Madre de Dios,Peru',
	'2604',
	'PE',
	'Region',
	'Madre de Dios',
	'PE-MDD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.8903
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9061339,
	'Tumbes,Peru',
	'2604',
	'PE',
	'Region',
	'Tumbes',
	'PE-TUM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.8973
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9061340,
	'Calabarzon,Philippines',
	'2608',
	'PH',
	'Region',
	'Calabarzon',
	'PH-40',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.9033
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9061341,
	'Central Luzon,Philippines',
	'2608',
	'PH',
	'Region',
	'Central Luzon',
	'PH-03',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.9093
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9061342,
	'Western Visayas,Philippines',
	'2608',
	'PH',
	'Region',
	'Western Visayas',
	'PH-06',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.9163
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9061343,
	'Davao Region,Philippines',
	'2608',
	'PH',
	'Region',
	'Davao Region',
	'PH-11',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.9243
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9061344,
	'Central Visayas,Philippines',
	'2608',
	'PH',
	'Region',
	'Central Visayas',
	'PH-07',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.9303
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9061345,
	'Ilocos Region,Philippines',
	'2608',
	'PH',
	'Region',
	'Ilocos Region',
	'PH-01',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.9373
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9061346,
	'Northern Mindanao,Philippines',
	'2608',
	'PH',
	'Region',
	'Northern Mindanao',
	'PH-10',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.9433
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9061347,
	'Region XII,Philippines',
	'2608',
	'PH',
	'Region',
	'Region XII',
	'PH-12',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.9493
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9061348,
	'Cagayan Valley,Philippines',
	'2608',
	'PH',
	'Region',
	'Cagayan Valley',
	'PH-02',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.9552
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9061349,
	'Bicol,Philippines',
	'2608',
	'PH',
	'Region',
	'Bicol',
	'PH-05',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.9613
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9061350,
	'MIMAROPA,Philippines',
	'2608',
	'PH',
	'Region',
	'MIMAROPA',
	'PH-41',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.9692
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9061351,
	'Eastern Visayas,Philippines',
	'2608',
	'PH',
	'Region',
	'Eastern Visayas',
	'PH-08',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.9762
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9061352,
	'Caraga,Philippines',
	'2608',
	'PH',
	'Region',
	'Caraga',
	'PH-13',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.9822
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9061353,
	'Cordillera Administrative Region,Philippines',
	'2608',
	'PH',
	'Region',
	'Cordillera Administrative Region',
	'PH-15',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.9883
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9061354,
	'Zamboanga Peninsula,Philippines',
	'2608',
	'PH',
	'Region',
	'Zamboanga Peninsula',
	'PH-09',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:07.9943
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9061355,
	'Autonomous Region in Muslim Mindanao,Philippines',
	'2608',
	'PH',
	'Region',
	'Autonomous Region in Muslim Mindanao',
	'PH-14',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.0013
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9061356,
	'Sindh,Pakistan',
	'2586',
	'PK',
	'Region',
	'Sindh',
	'PK-SD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.0073
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9061357,
	'Punjab,Pakistan',
	'2586',
	'PK',
	'Region',
	'Punjab',
	'PK-PB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.0143
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9061358,
	'Khyber Pakhtunkhwa,Pakistan',
	'2586',
	'PK',
	'Region',
	'Khyber Pakhtunkhwa',
	'PK-KP',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.0213
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9061359,
	'Balochistan,Pakistan',
	'2586',
	'PK',
	'Region',
	'Balochistan',
	'PK-BA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.0273
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9061360,
	'Islamabad Capital Territory,Pakistan',
	'2586',
	'PK',
	'Region',
	'Islamabad Capital Territory',
	'PK-IS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.0333
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9061361,
	'Azad Jammu and Kashmir,Pakistan',
	'2586',
	'PK',
	'Region',
	'Azad Jammu and Kashmir',
	'PK-JK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.0394
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9061363,
	'Gilgit-Baltistan,Pakistan',
	'2586',
	'PK',
	'Region',
	'Gilgit-Baltistan',
	'PK-GB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.0464
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9061496,
	'Hebei,Tianjin,China',
	'20164',
	'CN',
	'District',
	'Hebei',
	'CN-13',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.0524
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9061562,
	'Harju County,Estonia',
	'2233',
	'EE',
	'County',
	'Harju County',
	'EE-37',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.0585
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9061564,
	'Ida-Viru County,Estonia',
	'2233',
	'EE',
	'County',
	'Ida-Viru County',
	'EE-44',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.0644
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9061565,
	'Tartu County,Estonia',
	'2233',
	'EE',
	'County',
	'Tartu County',
	'EE-78',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.0704
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9061566,
	'Viljandi County,Estonia',
	'2233',
	'EE',
	'County',
	'Viljandi County',
	'EE-84',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.0764
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9061568,
	'Saare County,Estonia',
	'2233',
	'EE',
	'County',
	'Saare County',
	'EE-74',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.0834
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9061642,
	'Telangana,India',
	'2356',
	'IN',
	'State',
	'Telangana',
	'IN-TG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.0904
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9062293,
	'Utena County,Lithuania',
	'2440',
	'LT',
	'County',
	'Utena County',
	'LT-UT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.0975
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9062294,
	'Vilnius County,Lithuania',
	'2440',
	'LT',
	'County',
	'Vilnius County',
	'LT-VL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.1035
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9062295,
	'Alytus County,Lithuania',
	'2440',
	'LT',
	'County',
	'Alytus County',
	'LT-AL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.1094
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9062300,
	'Kaunas County,Lithuania',
	'2440',
	'LT',
	'County',
	'Kaunas County',
	'LT-KU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.1154
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9062303,
	'Daugavpils,Daugavpils,Latvia',
	'9062313',
	'LV',
	'City',
	'Daugavpils',
	'LV-DGV',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.1204
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9062310,
	'Ventspils,Ventspils,Latvia',
	'9062320',
	'LV',
	'City',
	'Ventspils',
	'LV-VEN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.1264
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9062314,
	'Jelgava,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Jelgava',
	'LV-JEL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.1324
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9062315,
	'Ogre Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Ogre Municipality',
	'LV-067',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.1394
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9062317,
	'Valmiera,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Valmiera',
	'LV-VMR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.1464
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9062318,
	'Riga,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Riga',
	'LV-RIX',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.1524
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9062585,
	'Nitra Region,Slovakia',
	'2703',
	'SK',
	'Region',
	'Nitra Region',
	'SK-NI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.1584
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9062586,
	'Bratislava Region,Slovakia',
	'2703',
	'SK',
	'Region',
	'Bratislava Region',
	'SK-BL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.1644
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9062587,
	'Trnava Region,Slovakia',
	'2703',
	'SK',
	'Region',
	'Trnava Region',
	'SK-TA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.1704
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9063100,
	'Balti,Balti,Moldova',
	'9069925',
	'MD',
	'City',
	'Balti',
	'MD-BA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.1764
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9063483,
	'Groningen,Groningen,Netherlands',
	'20763',
	'NL',
	'Municipality',
	'Groningen',
	'NL-GR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.1825
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9065322,
	'Rakhine,Myanmar (Burma)',
	'2104',
	'MM',
	'City',
	'Rakhine',
	'MM-16',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.1885
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9065341,
	'Ayeyarwady,Myanmar (Burma)',
	'2104',
	'MM',
	'City',
	'Ayeyarwady',
	'MM-07',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.1955
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9065345,
	'Kachin,Myanmar (Burma)',
	'2104',
	'MM',
	'Region',
	'Kachin',
	'MM-11',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.2015
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9065347,
	'Magway Region,Myanmar (Burma)',
	'2104',
	'MM',
	'Region',
	'Magway Region',
	'MM-03',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.2085
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9065348,
	'Tanintharyi Region,Myanmar (Burma)',
	'2104',
	'MM',
	'Region',
	'Tanintharyi Region',
	'MM-05',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.2155
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9065349,
	'Naypyidaw Union Territory,Myanmar (Burma)',
	'2104',
	'MM',
	'Region',
	'Naypyidaw Union Territory',
	'MM-18',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.2216
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9065350,
	'Bago Region,Myanmar (Burma)',
	'2104',
	'MM',
	'Region',
	'Bago Region',
	'MM-02',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.2286
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9065351,
	'Sagaing Region,Myanmar (Burma)',
	'2104',
	'MM',
	'Region',
	'Sagaing Region',
	'MM-01',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.2336
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9065352,
	'Mandalay Region,Myanmar (Burma)',
	'2104',
	'MM',
	'Region',
	'Mandalay Region',
	'MM-04',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.2406
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9065353,
	'Yangon Region,Myanmar (Burma)',
	'2104',
	'MM',
	'Region',
	'Yangon Region',
	'MM-06',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.2475
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9065354,
	'Mon State,Myanmar (Burma)',
	'2104',
	'MM',
	'State',
	'Mon State',
	'MM-15',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.2545
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9065355,
	'Kayin State,Myanmar (Burma)',
	'2104',
	'MM',
	'State',
	'Kayin State',
	'MM-13',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.2615
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9065356,
	'Shan,Myanmar (Burma)',
	'2104',
	'MM',
	'State',
	'Shan',
	'MM-17',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.2675
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9067175,
	'Maysan,Metro Manila,Philippines',
	'20835',
	'PH',
	'Neighborhood',
	'Maysan',
	'IQ-MA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.2745
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9067657,
	'Conakry,Conakry,Guinea',
	'9075549',
	'GN',
	'City',
	'Conakry',
	'GN-C',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.2805
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9067672,
	'Arta,Arta,Decentralized Administration of Epirus and Western Macedonia,Greece',
	'9075558',
	'GR',
	'City',
	'Arta',
	'DJ-AR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.2864
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9067734,
	'Maseru,Maseru,Lesotho',
	'9075597',
	'LS',
	'City',
	'Maseru',
	'LS-A',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.2924
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9067752,
	'Nouakchott,Mauritania',
	'2478',
	'MR',
	'City',
	'Nouakchott',
	'MR-NKC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.3004
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9067845,
	'Saint Louis,Saint-Louis Region,Senegal',
	'9070071',
	'SN',
	'City',
	'Saint Louis',
	'SC-22',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.3074
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9067846,
	'Dakar,Dakar Region,Senegal',
	'9070424',
	'SN',
	'City',
	'Dakar',
	'SN-DK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.3124
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9068562,
	'San Vicente,San Jose Province,Costa Rica',
	'9070296',
	'CR',
	'City',
	'San Vicente',
	'SV-SV',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.3203
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9068833,
	'Berat,Occitanie,France',
	'9068897',
	'FR',
	'City',
	'Berat',
	'AL-01',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.3273
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9068854,
	'Plaisance,Occitanie,France',
	'9068897',
	'FR',
	'City',
	'Plaisance',
	'SC-19',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.3353
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069523,
	'Flanders,Belgium',
	'2056',
	'BE',
	'Region',
	'Flanders',
	'BE-VLG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.3403
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069524,
	'Walloon Region,Belgium',
	'2056',
	'BE',
	'Region',
	'Walloon Region',
	'BE-WAL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.3463
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069533,
	'North Kalimantan,Indonesia',
	'2360',
	'ID',
	'Province',
	'North Kalimantan',
	'ID-KU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.3523
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069541,
	'Al Daayen,Qatar',
	'2634',
	'QA',
	'Municipality',
	'Al Daayen',
	'QA-ZA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.3592
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069542,
	'Doha,Qatar',
	'2634',
	'QA',
	'Municipality',
	'Doha',
	'QA-DA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.3652
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069543,
	'Madinat ash Shamal,Qatar',
	'2634',
	'QA',
	'Municipality',
	'Madinat ash Shamal',
	'QA-MS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.3712
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069544,
	'Al Wakrah Municipality,Qatar',
	'2634',
	'QA',
	'Municipality',
	'Al Wakrah Municipality',
	'QA-WA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.3762
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069545,
	'Umm Salal Municipality,Qatar',
	'2634',
	'QA',
	'Municipality',
	'Umm Salal Municipality',
	'QA-US',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.3832
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069546,
	'Al Khor,Qatar',
	'2634',
	'QA',
	'Municipality',
	'Al Khor',
	'QA-KH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.3882
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069547,
	'Al Farwaniyah Governorate,Kuwait',
	'2414',
	'KW',
	'Governorate',
	'Al Farwaniyah Governorate',
	'KW-FA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.3951
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069548,
	'Mubarak Al-Kabeer Governorate,Kuwait',
	'2414',
	'KW',
	'Governorate',
	'Mubarak Al-Kabeer Governorate',
	'KW-MU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.4011
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069549,
	'Hawalli Governorate,Kuwait',
	'2414',
	'KW',
	'Governorate',
	'Hawalli Governorate',
	'KW-HA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.4081
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069550,
	'Al Ahmadi Governorate,Kuwait',
	'2414',
	'KW',
	'Governorate',
	'Al Ahmadi Governorate',
	'KW-AH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.4141
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069551,
	'Al Asimah Governate,Kuwait',
	'2414',
	'KW',
	'Governorate',
	'Al Asimah Governate',
	'KW-KU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.4211
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069552,
	'Al Jahra Governorate,Kuwait',
	'2414',
	'KW',
	'Governorate',
	'Al Jahra Governorate',
	'KW-JA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.4271
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069554,
	'Pastaza Province,Ecuador',
	'2218',
	'EC',
	'Province',
	'Pastaza Province',
	'EC-Y',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.4330
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069555,
	'Chimborazo Province,Ecuador',
	'2218',
	'EC',
	'Province',
	'Chimborazo Province',
	'EC-H',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.4400
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069556,
	'Cotopaxi Province,Ecuador',
	'2218',
	'EC',
	'Province',
	'Cotopaxi Province',
	'EC-X',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.4460
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069557,
	'Imbabura Province,Ecuador',
	'2218',
	'EC',
	'Province',
	'Imbabura Province',
	'EC-I',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.4520
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069561,
	'Carchi Province,Ecuador',
	'2218',
	'EC',
	'Province',
	'Carchi Province',
	'EC-C',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.4580
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069562,
	'Santa Elena Province,Ecuador',
	'2218',
	'EC',
	'Province',
	'Santa Elena Province',
	'EC-SE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.4640
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069566,
	'Orellana Province,Ecuador',
	'2218',
	'EC',
	'Province',
	'Orellana Province',
	'EC-D',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.4709
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069567,
	'Esmeraldas Province,Ecuador',
	'2218',
	'EC',
	'Province',
	'Esmeraldas Province',
	'EC-E',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.4769
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069568,
	'Provincia de Napo,Ecuador',
	'2218',
	'EC',
	'Province',
	'Provincia de Napo',
	'EC-N',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.4839
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069569,
	'Morona-Santiago Province,Ecuador',
	'2218',
	'EC',
	'Province',
	'Morona-Santiago Province',
	'EC-S',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.4899
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069570,
	'Zamora-Chinchipe Province,Ecuador',
	'2218',
	'EC',
	'Province',
	'Zamora-Chinchipe Province',
	'EC-Z',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.4959
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069571,
	'Pavlodar Province,Kazakhstan',
	'2398',
	'KZ',
	'Region',
	'Pavlodar Province',
	'KZ-PAV',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.5019
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069572,
	'West Kazakhstan Province,Kazakhstan',
	'2398',
	'KZ',
	'Region',
	'West Kazakhstan Province',
	'KZ-ZAP',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.5098
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069573,
	'Kyzylorda Province,Kazakhstan',
	'2398',
	'KZ',
	'Region',
	'Kyzylorda Province',
	'KZ-KZY',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.5158
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069574,
	'Mogilev Region,Belarus',
	'2112',
	'BY',
	'Region',
	'Mogilev Region',
	'BY-MA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.5228
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069575,
	'Brest Region,Belarus',
	'2112',
	'BY',
	'Region',
	'Brest Region',
	'BY-BR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.5288
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069576,
	'Gomel Region,Belarus',
	'2112',
	'BY',
	'Region',
	'Gomel Region',
	'BY-HO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.5358
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069577,
	'Vitebsk Region,Belarus',
	'2112',
	'BY',
	'Region',
	'Vitebsk Region',
	'BY-VI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.5428
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069578,
	'Minsk Region,Belarus',
	'2112',
	'BY',
	'Region',
	'Minsk Region',
	'BY-MI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.5487
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069579,
	'Hrodna Region,Belarus',
	'2112',
	'BY',
	'Region',
	'Hrodna Region',
	'BY-HR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.5557
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069657,
	'Rapla County,Estonia',
	'2233',
	'EE',
	'County',
	'Rapla County',
	'EE-70',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.5627
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069661,
	'Valga County,Estonia',
	'2233',
	'EE',
	'County',
	'Valga County',
	'EE-82',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.5687
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069663,
	'Hiiu County,Estonia',
	'2233',
	'EE',
	'County',
	'Hiiu County',
	'EE-39',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.5777
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069664,
	'Federal Dependencies of Venezuela,Venezuela',
	'2862',
	'VE',
	'State',
	'Federal Dependencies of Venezuela',
	'VE-W',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.5836
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069665,
	'Cojedes,Venezuela',
	'2862',
	'VE',
	'State',
	'Cojedes',
	'VE-H',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.5886
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069666,
	'Delta Amacuro,Venezuela',
	'2862',
	'VE',
	'State',
	'Delta Amacuro',
	'VE-Y',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.5956
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069667,
	'Amazonas,Venezuela',
	'2862',
	'VE',
	'State',
	'Amazonas',
	'VE-Z',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.6016
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069669,
	'Flores Department,Uruguay',
	'2858',
	'UY',
	'Department',
	'Flores Department',
	'UY-FS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.6076
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069670,
	'Canelones Department,Uruguay',
	'2858',
	'UY',
	'Department',
	'Canelones Department',
	'UY-CA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.6136
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069671,
	'Florida Department,Uruguay',
	'2858',
	'UY',
	'Department',
	'Florida Department',
	'UY-FD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.6196
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069673,
	'Artigas Department,Uruguay',
	'2858',
	'UY',
	'Department',
	'Artigas Department',
	'UY-AR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.6255
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069675,
	'Soriano Department,Uruguay',
	'2858',
	'UY',
	'Department',
	'Soriano Department',
	'UY-SO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.6326
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069677,
	'Rocha Department,Uruguay',
	'2858',
	'UY',
	'Department',
	'Rocha Department',
	'UY-RO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.6385
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069678,
	'Treinta y Tres Department,Uruguay',
	'2858',
	'UY',
	'Department',
	'Treinta y Tres Department',
	'UY-TT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.6448
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069679,
	'Lavalleja Department,Uruguay',
	'2858',
	'UY',
	'Department',
	'Lavalleja Department',
	'UY-LA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.6507
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069680,
	'Durazno Department,Uruguay',
	'2858',
	'UY',
	'Department',
	'Durazno Department',
	'UY-DU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.6567
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069681,
	'Bordj Bou Arreridj Province,Algeria',
	'2012',
	'DZ',
	'Province',
	'Bordj Bou Arreridj Province',
	'DZ-34',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.6637
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069683,
	'Oran Province,Algeria',
	'2012',
	'DZ',
	'Province',
	'Oran Province',
	'DZ-31',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.6697
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069686,
	'Skikda Province,Algeria',
	'2012',
	'DZ',
	'Province',
	'Skikda Province',
	'DZ-21',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.6757
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069687,
	'Ouargla Province,Algeria',
	'2012',
	'DZ',
	'Province',
	'Ouargla Province',
	'DZ-30',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.6827
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069688,
	'Mila Province,Algeria',
	'2012',
	'DZ',
	'Province',
	'Mila Province',
	'DZ-43',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.6897
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069689,
	'Djelfa Province,Algeria',
	'2012',
	'DZ',
	'Province',
	'Djelfa Province',
	'DZ-17',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.6967
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069690,
	'Tlemcen Province,Algeria',
	'2012',
	'DZ',
	'Province',
	'Tlemcen Province',
	'DZ-13',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.7027
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069692,
	'Souk Ahras Province,Algeria',
	'2012',
	'DZ',
	'Province',
	'Souk Ahras Province',
	'DZ-41',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.7096
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069694,
	'Adrar Province,Algeria',
	'2012',
	'DZ',
	'Province',
	'Adrar Province',
	'DZ-01',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.7156
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069695,
	'Tizi Ouzou Province,Algeria',
	'2012',
	'DZ',
	'Province',
	'Tizi Ouzou Province',
	'DZ-15',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.7226
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069696,
	'M''Sila Province,Algeria',
	'2012',
	'DZ',
	'Province',
	'M''Sila Province',
	'DZ-28',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.7286
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069697,
	'El Oued Province,Algeria',
	'2012',
	'DZ',
	'Province',
	'El Oued Province',
	'DZ-39',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.7356
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069698,
	'Batna Province,Algeria',
	'2012',
	'DZ',
	'Province',
	'Batna Province',
	'DZ-05',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.7426
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069699,
	'Ghardaia Province,Algeria',
	'2012',
	'DZ',
	'Province',
	'Ghardaia Province',
	'DZ-47',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.7486
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069700,
	'Oum El Bouaghi Province,Algeria',
	'2012',
	'DZ',
	'Province',
	'Oum El Bouaghi Province',
	'DZ-04',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.7546
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069701,
	'Tiaret Province,Algeria',
	'2012',
	'DZ',
	'Province',
	'Tiaret Province',
	'DZ-14',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.7606
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069702,
	'Chlef Province,Algeria',
	'2012',
	'DZ',
	'Province',
	'Chlef Province',
	'DZ-02',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.7675
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069703,
	'Khenchela Province,Algeria',
	'2012',
	'DZ',
	'Province',
	'Khenchela Province',
	'DZ-40',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.7745
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069704,
	'El Bayadh Province,Algeria',
	'2012',
	'DZ',
	'Province',
	'El Bayadh Province',
	'DZ-32',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.7815
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069705,
	'Relizane Province,Algeria',
	'2012',
	'DZ',
	'Province',
	'Relizane Province',
	'DZ-48',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.7875
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069706,
	'Guelma Province,Algeria',
	'2012',
	'DZ',
	'Province',
	'Guelma Province',
	'DZ-24',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.7935
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069707,
	'Illizi Province,Algeria',
	'2012',
	'DZ',
	'Province',
	'Illizi Province',
	'DZ-33',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.7995
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069708,
	'Mostaganem Province,Algeria',
	'2012',
	'DZ',
	'Province',
	'Mostaganem Province',
	'DZ-27',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.8054
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069709,
	'Annaba Province,Algeria',
	'2012',
	'DZ',
	'Province',
	'Annaba Province',
	'DZ-23',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.8114
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069710,
	'Jijel Province,Algeria',
	'2012',
	'DZ',
	'Province',
	'Jijel Province',
	'DZ-18',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.8184
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069711,
	'Tipaza Province,Algeria',
	'2012',
	'DZ',
	'Province',
	'Tipaza Province',
	'DZ-42',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.8244
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069712,
	'Mascara Province,Algeria',
	'2012',
	'DZ',
	'Province',
	'Mascara Province',
	'DZ-29',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.8304
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069713,
	'Blida Province,Algeria',
	'2012',
	'DZ',
	'Province',
	'Blida Province',
	'DZ-09',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.8364
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069714,
	'Laghouat Province,Algeria',
	'2012',
	'DZ',
	'Province',
	'Laghouat Province',
	'DZ-03',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.8413
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069716,
	'Algiers Province,Algeria',
	'2012',
	'DZ',
	'Province',
	'Algiers Province',
	'DZ-16',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.8473
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069719,
	'Tissemsilt Province,Algeria',
	'2012',
	'DZ',
	'Province',
	'Tissemsilt Province',
	'DZ-38',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.8543
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069724,
	'Tebessa Province,Algeria',
	'2012',
	'DZ',
	'Province',
	'Tebessa Province',
	'DZ-12',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.8603
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069726,
	'Dhofar Governorate,Oman',
	'2512',
	'OM',
	'Governorate',
	'Dhofar Governorate',
	'OM-ZU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.8673
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069728,
	'Ash Sharqiyah South Governorate,Oman',
	'2512',
	'OM',
	'Governorate',
	'Ash Sharqiyah South Governorate',
	'OM-SJ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.8723
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069729,
	'Al Buraymi Governorate,Oman',
	'2512',
	'OM',
	'Governorate',
	'Al Buraymi Governorate',
	'OM-BU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.8782
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069730,
	'Musandam Governorate,Oman',
	'2512',
	'OM',
	'Governorate',
	'Musandam Governorate',
	'OM-MU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.9869
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069731,
	'Al Batinah North Governorate,Oman',
	'2512',
	'OM',
	'Governorate',
	'Al Batinah North Governorate',
	'OM-BS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:08.9959
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069732,
	'Ash Sharqiyah North Governorate,Oman',
	'2512',
	'OM',
	'Governorate',
	'Ash Sharqiyah North Governorate',
	'OM-SS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.0019
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069733,
	'Al Batinah South Governorate,Oman',
	'2512',
	'OM',
	'Governorate',
	'Al Batinah South Governorate',
	'OM-BJ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.0079
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069734,
	'Ad Dhahirah Governorate,Oman',
	'2512',
	'OM',
	'Governorate',
	'Ad Dhahirah Governorate',
	'OM-ZA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.0139
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069735,
	'Muscat Governorate,Oman',
	'2512',
	'OM',
	'Governorate',
	'Muscat Governorate',
	'OM-MA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.0199
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069736,
	'Al Wusta Governorate,Oman',
	'2512',
	'OM',
	'Governorate',
	'Al Wusta Governorate',
	'OM-WU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.0259
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069740,
	'Bocas del Toro Province,Panama',
	'2591',
	'PA',
	'Province',
	'Bocas del Toro Province',
	'PA-1',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.0328
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069742,
	'Veraguas Province,Panama',
	'2591',
	'PA',
	'Province',
	'Veraguas Province',
	'PA-9',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.0398
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069743,
	'Herrera Province,Panama',
	'2591',
	'PA',
	'Province',
	'Herrera Province',
	'PA-6',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.0459
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069745,
	'Guna Yala Comarca,Panama',
	'2591',
	'PA',
	'Province',
	'Guna Yala Comarca',
	'PA-KY',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.0529
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069750,
	'Los Santos Province,Panama',
	'2591',
	'PA',
	'Province',
	'Los Santos Province',
	'PA-7',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.0579
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069752,
	'Nabatiyeh Governorate,Lebanon',
	'2422',
	'LB',
	'Governorate',
	'Nabatiyeh Governorate',
	'LB-NA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.0659
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069753,
	'Duarte Province,Dominican Republic',
	'2214',
	'DO',
	'Province',
	'Duarte Province',
	'DO-06',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.0728
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069755,
	'San Juan Province,Dominican Republic',
	'2214',
	'DO',
	'Province',
	'San Juan Province',
	'DO-22',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.0788
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069756,
	'La Vega Province,Dominican Republic',
	'2214',
	'DO',
	'Province',
	'La Vega Province',
	'DO-13',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.0848
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069757,
	'Baoruco Province,Dominican Republic',
	'2214',
	'DO',
	'Province',
	'Baoruco Province',
	'DO-03',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.0918
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069759,
	'Monte Cristi Province,Dominican Republic',
	'2214',
	'DO',
	'Province',
	'Monte Cristi Province',
	'DO-15',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.0988
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069760,
	'Distrito Nacional,Dominican Republic',
	'2214',
	'DO',
	'Province',
	'Distrito Nacional',
	'DO-01',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.1047
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069761,
	'Independencia Province,Dominican Republic',
	'2214',
	'DO',
	'Province',
	'Independencia Province',
	'DO-10',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.1097
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069762,
	'Hermanas Mirabal Province,Dominican Republic',
	'2214',
	'DO',
	'Province',
	'Hermanas Mirabal Province',
	'DO-19',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.1157
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069763,
	'Puerto Plata Province,Dominican Republic',
	'2214',
	'DO',
	'Province',
	'Puerto Plata Province',
	'DO-18',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.1217
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069764,
	'Santiago Province,Dominican Republic',
	'2214',
	'DO',
	'Province',
	'Santiago Province',
	'DO-25',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.1277
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069765,
	'Pedernales Province,Dominican Republic',
	'2214',
	'DO',
	'Province',
	'Pedernales Province',
	'DO-16',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.1357
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069767,
	'Espaillat Province,Dominican Republic',
	'2214',
	'DO',
	'Province',
	'Espaillat Province',
	'DO-09',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.1426
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069769,
	'Barahona Province,Dominican Republic',
	'2214',
	'DO',
	'Province',
	'Barahona Province',
	'DO-04',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.1486
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069772,
	'Santo Domingo Province,Dominican Republic',
	'2214',
	'DO',
	'Province',
	'Santo Domingo Province',
	'DO-32',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.1566
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069773,
	'La Romana Province,Dominican Republic',
	'2214',
	'DO',
	'Province',
	'La Romana Province',
	'DO-12',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.1626
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069774,
	'Monte Plata Province,Dominican Republic',
	'2214',
	'DO',
	'Province',
	'Monte Plata Province',
	'DO-29',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.1686
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069776,
	'Peravia Province,Dominican Republic',
	'2214',
	'DO',
	'Province',
	'Peravia Province',
	'DO-17',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.1746
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069778,
	'El Seibo Province,Dominican Republic',
	'2214',
	'DO',
	'Province',
	'El Seibo Province',
	'DO-08',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.1805
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069779,
	'Hato Mayor Province,Dominican Republic',
	'2214',
	'DO',
	'Province',
	'Hato Mayor Province',
	'DO-30',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.1865
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069780,
	'Valverde Province,Dominican Republic',
	'2214',
	'DO',
	'Province',
	'Valverde Province',
	'DO-27',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.1925
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069782,
	'Azua Province,Dominican Republic',
	'2214',
	'DO',
	'Province',
	'Azua Province',
	'DO-02',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.1985
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069784,
	'Northern Province,Sri Lanka',
	'2144',
	'LK',
	'Province',
	'Northern Province',
	'LK-4',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.2045
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069786,
	'Sabaragamuwa Province,Sri Lanka',
	'2144',
	'LK',
	'Province',
	'Sabaragamuwa Province',
	'LK-9',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.2105
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069787,
	'North Central Province,Sri Lanka',
	'2144',
	'LK',
	'Province',
	'North Central Province',
	'LK-7',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.2164
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069789,
	'North Western Province,Sri Lanka',
	'2144',
	'LK',
	'Province',
	'North Western Province',
	'LK-6',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.2234
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069790,
	'Uva Province,Sri Lanka',
	'2144',
	'LK',
	'Province',
	'Uva Province',
	'LK-8',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.2294
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069791,
	'Central Province,Sri Lanka',
	'2144',
	'LK',
	'Province',
	'Central Province',
	'LK-2',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.2364
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069792,
	'Rajshahi Division,Bangladesh',
	'2050',
	'BD',
	'Province',
	'Rajshahi Division',
	'BD-E',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.2424
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069793,
	'Barisal Division,Bangladesh',
	'2050',
	'BD',
	'Province',
	'Barisal Division',
	'BD-A',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.2494
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069794,
	'Sylhet Division,Bangladesh',
	'2050',
	'BD',
	'Province',
	'Sylhet Division',
	'BD-G',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.2563
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069795,
	'El Progreso Department,Guatemala',
	'2320',
	'GT',
	'Department',
	'El Progreso Department',
	'GT-PR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.2613
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069796,
	'Quetzaltenango Department,Guatemala',
	'2320',
	'GT',
	'Department',
	'Quetzaltenango Department',
	'GT-QZ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.2673
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069797,
	'Huehuetenango Department,Guatemala',
	'2320',
	'GT',
	'Department',
	'Huehuetenango Department',
	'GT-HU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.2733
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069798,
	'Retalhuleu Department,Guatemala',
	'2320',
	'GT',
	'Department',
	'Retalhuleu Department',
	'GT-RE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.2793
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069799,
	'Sacatepequez Department,Guatemala',
	'2320',
	'GT',
	'Department',
	'Sacatepequez Department',
	'GT-SA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.2843
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069800,
	'Jalapa Department,Guatemala',
	'2320',
	'GT',
	'Department',
	'Jalapa Department',
	'GT-JA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.2902
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069801,
	'Guatemala Department,Guatemala',
	'2320',
	'GT',
	'Department',
	'Guatemala Department',
	'GT-GU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.2962
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069802,
	'Chimaltenango Department,Guatemala',
	'2320',
	'GT',
	'Department',
	'Chimaltenango Department',
	'GT-CM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.3022
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069803,
	'Jutiapa Department,Guatemala',
	'2320',
	'GT',
	'Department',
	'Jutiapa Department',
	'GT-JU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.3082
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069804,
	'Zacapa Department,Guatemala',
	'2320',
	'GT',
	'Department',
	'Zacapa Department',
	'GT-ZA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.3152
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069805,
	'Escuintla,Guatemala',
	'2320',
	'GT',
	'Department',
	'Escuintla',
	'GT-ES',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.3212
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069808,
	'Chiquimula Department,Guatemala',
	'2320',
	'GT',
	'Department',
	'Chiquimula Department',
	'GT-CQ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.3281
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069810,
	'Santa Rosa Department,Guatemala',
	'2320',
	'GT',
	'Department',
	'Santa Rosa Department',
	'GT-SR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.3351
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069811,
	'Westfjords Region,Iceland',
	'2352',
	'IS',
	'Region',
	'Westfjords Region',
	'IS-4',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.3411
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069814,
	'Southern Peninsula Region,Iceland',
	'2352',
	'IS',
	'Region',
	'Southern Peninsula Region',
	'IS-2',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.3471
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069815,
	'Capital Region,Iceland',
	'2352',
	'IS',
	'Region',
	'Capital Region',
	'IS-1',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.3541
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069816,
	'Northwestern Region,Iceland',
	'2352',
	'IS',
	'Region',
	'Northwestern Region',
	'IS-5',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.3601
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069817,
	'Jerash Governorate,Jordan',
	'2400',
	'JO',
	'Governorate',
	'Jerash Governorate',
	'JO-JA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.3670
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069818,
	'Amman Governorate,Jordan',
	'2400',
	'JO',
	'Governorate',
	'Amman Governorate',
	'JO-AM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.3730
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069819,
	'Balqa Governorate,Jordan',
	'2400',
	'JO',
	'Governorate',
	'Balqa Governorate',
	'JO-BA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.3790
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069820,
	'Karak Governorate,Jordan',
	'2400',
	'JO',
	'Governorate',
	'Karak Governorate',
	'JO-KA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.3850
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069821,
	'Tafilah Governorate,Jordan',
	'2400',
	'JO',
	'Governorate',
	'Tafilah Governorate',
	'JO-AT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.3910
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069822,
	'Zarqa Governorate,Jordan',
	'2400',
	'JO',
	'Governorate',
	'Zarqa Governorate',
	'JO-AZ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.3970
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069823,
	'Irbid Governorate,Jordan',
	'2400',
	'JO',
	'Governorate',
	'Irbid Governorate',
	'JO-IR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.4030
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069824,
	'Ajloun Governorate,Jordan',
	'2400',
	'JO',
	'Governorate',
	'Ajloun Governorate',
	'JO-AJ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.4100
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069825,
	'Aqaba Governorate,Jordan',
	'2400',
	'JO',
	'Governorate',
	'Aqaba Governorate',
	'JO-AQ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.4160
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069826,
	'Ma''an Governorate,Jordan',
	'2400',
	'JO',
	'Governorate',
	'Ma''an Governorate',
	'JO-MN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.4220
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069827,
	'Mafraq Governorate,Jordan',
	'2400',
	'JO',
	'Governorate',
	'Mafraq Governorate',
	'JO-MA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.4280
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069828,
	'Madaba Governorate,Jordan',
	'2400',
	'JO',
	'Governorate',
	'Madaba Governorate',
	'JO-MD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.4339
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069829,
	'Southern Governorate,Bahrain',
	'2048',
	'BH',
	'Governorate',
	'Southern Governorate',
	'BH-14',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.4400
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069830,
	'Capital Governorate,Bahrain',
	'2048',
	'BH',
	'Governorate',
	'Capital Governorate',
	'BH-13',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.4470
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069831,
	'Northern Governorate,Bahrain',
	'2048',
	'BH',
	'Governorate',
	'Northern Governorate',
	'BH-17',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.4531
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069832,
	'Muharraq Governorate,Bahrain',
	'2048',
	'BH',
	'Governorate',
	'Muharraq Governorate',
	'BH-15',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.4590
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069833,
	'Central Governorate,Bahrain',
	'2048',
	'BH',
	'Governorate',
	'Central Governorate',
	'BH-16',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.4660
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069834,
	'Eastern Region,Ghana',
	'2288',
	'GH',
	'Region',
	'Eastern Region',
	'UG-E',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.4720
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069836,
	'Ashanti Region,Ghana',
	'2288',
	'GH',
	'Region',
	'Ashanti Region',
	'GH-AH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.4800
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069837,
	'Greater Accra Region,Ghana',
	'2288',
	'GH',
	'Region',
	'Greater Accra Region',
	'GH-AA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.4870
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069838,
	'Shida Kartli,Georgia',
	'2268',
	'GE',
	'Region',
	'Shida Kartli',
	'GE-SK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.4939
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069839,
	'Guria,Georgia',
	'2268',
	'GE',
	'Region',
	'Guria',
	'GE-GU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.4999
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069840,
	'Abkhazia,Georgia',
	'2268',
	'GE',
	'Region',
	'Abkhazia',
	'GE-AB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.5059
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069841,
	'Tbilisi,Georgia',
	'2268',
	'GE',
	'Region',
	'Tbilisi',
	'GE-TB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.5139
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069842,
	'Adjara,Georgia',
	'2268',
	'GE',
	'Region',
	'Adjara',
	'GE-AJ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.5199
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069843,
	'Mtskheta-Mtianeti,Georgia',
	'2268',
	'GE',
	'Region',
	'Mtskheta-Mtianeti',
	'GE-MM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.5289
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069844,
	'Imereti,Georgia',
	'2268',
	'GE',
	'Region',
	'Imereti',
	'GE-IM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.5368
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069845,
	'Samtskhe-Javakheti,Georgia',
	'2268',
	'GE',
	'Region',
	'Samtskhe-Javakheti',
	'GE-SJ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.5428
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069847,
	'Racha-Lechkhumi and Lower Svaneti,Georgia',
	'2268',
	'GE',
	'Region',
	'Racha-Lechkhumi and Lower Svaneti',
	'GE-RL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.5488
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069848,
	'Kvemo Kartli,Georgia',
	'2268',
	'GE',
	'Region',
	'Kvemo Kartli',
	'GE-KK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.5548
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069849,
	'Kakheti,Georgia',
	'2268',
	'GE',
	'Region',
	'Kakheti',
	'GE-KA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.5608
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069850,
	'Al Muthanna Governorate,Iraq',
	'2368',
	'IQ',
	'Governorate',
	'Al Muthanna Governorate',
	'IQ-MU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.5658
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069852,
	'Erbil Governorate,Iraq',
	'2368',
	'IQ',
	'Governorate',
	'Erbil Governorate',
	'IQ-AR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.5717
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069855,
	'Karbala Governorate,Iraq',
	'2368',
	'IQ',
	'Governorate',
	'Karbala Governorate',
	'IQ-KA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.5777
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069856,
	'Diyala Governorate,Iraq',
	'2368',
	'IQ',
	'Governorate',
	'Diyala Governorate',
	'IQ-DI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.5827
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069857,
	'Al Anbar Governorate,Iraq',
	'2368',
	'IQ',
	'Governorate',
	'Al Anbar Governorate',
	'IQ-AN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.5887
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069859,
	'Al-Qadisiyyah Governorate,Iraq',
	'2368',
	'IQ',
	'Governorate',
	'Al-Qadisiyyah Governorate',
	'IQ-QA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.5937
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069863,
	'Kirkuk Governorate,Iraq',
	'2368',
	'IQ',
	'Governorate',
	'Kirkuk Governorate',
	'IQ-TS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.5987
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069864,
	'Duhok Governorate,Iraq',
	'2368',
	'IQ',
	'Governorate',
	'Duhok Governorate',
	'IQ-DA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.6047
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069865,
	'Sulaymaniyah Governorate,Iraq',
	'2368',
	'IQ',
	'Governorate',
	'Sulaymaniyah Governorate',
	'IQ-SU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.6106
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069866,
	'Dhi Qar Governorate,Iraq',
	'2368',
	'IQ',
	'Governorate',
	'Dhi Qar Governorate',
	'IQ-DQ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.6156
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069867,
	'Wasit Governorate,Iraq',
	'2368',
	'IQ',
	'Governorate',
	'Wasit Governorate',
	'IQ-WA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.6216
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069868,
	'Potosi Department,Bolivia',
	'2068',
	'BO',
	'Department',
	'Potosi Department',
	'BO-P',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.6276
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069869,
	'Beni Department,Bolivia',
	'2068',
	'BO',
	'Department',
	'Beni Department',
	'BO-B',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.6336
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069870,
	'Chuquisaca Department,Bolivia',
	'2068',
	'BO',
	'Department',
	'Chuquisaca Department',
	'BO-H',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.6396
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069871,
	'Pando Department,Bolivia',
	'2068',
	'BO',
	'Department',
	'Pando Department',
	'BO-N',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.6466
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069872,
	'Oruro Department,Bolivia',
	'2068',
	'BO',
	'Department',
	'Oruro Department',
	'BO-O',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.6526
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069873,
	'Hanover Parish,Jamaica',
	'2388',
	'JM',
	'Region',
	'Hanover Parish',
	'JM-09',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.6596
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069874,
	'Portland Parish,Jamaica',
	'2388',
	'JM',
	'Region',
	'Portland Parish',
	'JM-04',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.6655
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069875,
	'Kingston Parish,Jamaica',
	'2388',
	'JM',
	'Region',
	'Kingston Parish',
	'JM-01',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.6715
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069876,
	'Westmoreland Parish,Jamaica',
	'2388',
	'JM',
	'Region',
	'Westmoreland Parish',
	'JM-10',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.6911
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069877,
	'Kilimanjaro Region,Tanzania',
	'2834',
	'TZ',
	'Region',
	'Kilimanjaro Region',
	'TZ-09',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.6991
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069878,
	'Manyara Region,Tanzania',
	'2834',
	'TZ',
	'Region',
	'Manyara Region',
	'TZ-26',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.7051
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069880,
	'Kigoma Region,Tanzania',
	'2834',
	'TZ',
	'Region',
	'Kigoma Region',
	'TZ-08',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.7112
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069881,
	'Mara Region,Tanzania',
	'2834',
	'TZ',
	'Region',
	'Mara Region',
	'TZ-13',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.7174
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069882,
	'Mjini Magharibi Region,Tanzania',
	'2834',
	'TZ',
	'Region',
	'Mjini Magharibi Region',
	'TZ-15',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.7234
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069883,
	'Unguja South Region,Tanzania',
	'2834',
	'TZ',
	'Region',
	'Unguja South Region',
	'TZ-11',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.7295
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069884,
	'Singida Region,Tanzania',
	'2834',
	'TZ',
	'Region',
	'Singida Region',
	'TZ-23',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.7364
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069886,
	'Tanga Region,Tanzania',
	'2834',
	'TZ',
	'Region',
	'Tanga Region',
	'TZ-25',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.7424
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069887,
	'Rukwa Region,Tanzania',
	'2834',
	'TZ',
	'Region',
	'Rukwa Region',
	'TZ-20',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.7494
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069888,
	'Tabora Region,Tanzania',
	'2834',
	'TZ',
	'Region',
	'Tabora Region',
	'TZ-24',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.7554
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069889,
	'Iringa Region,Tanzania',
	'2834',
	'TZ',
	'Region',
	'Iringa Region',
	'TZ-04',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.7614
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069890,
	'Kagera Region,Tanzania',
	'2834',
	'TZ',
	'Region',
	'Kagera Region',
	'TZ-05',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.7674
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069892,
	'Shinyanga Region,Tanzania',
	'2834',
	'TZ',
	'Region',
	'Shinyanga Region',
	'TZ-22',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.7734
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069893,
	'Ruvuma Region,Tanzania',
	'2834',
	'TZ',
	'Region',
	'Ruvuma Region',
	'TZ-21',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.7804
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069895,
	'Pwani Region,Tanzania',
	'2834',
	'TZ',
	'Region',
	'Pwani Region',
	'TZ-19',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.7864
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069898,
	'Usulutan Department,El Salvador',
	'2222',
	'SV',
	'Department',
	'Usulutan Department',
	'SV-US',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.7924
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069901,
	'Morazan Department,El Salvador',
	'2222',
	'SV',
	'Department',
	'Morazan Department',
	'SV-MO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.7994
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069903,
	'Ahuachapan Department,El Salvador',
	'2222',
	'SV',
	'Department',
	'Ahuachapan Department',
	'SV-AH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.8055
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069905,
	'Cuscatlan Department,El Salvador',
	'2222',
	'SV',
	'Department',
	'Cuscatlan Department',
	'SV-CU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.8155
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069906,
	'Soldanesti District,Moldova',
	'2498',
	'MD',
	'District',
	'Soldanesti District',
	'MD-SD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.8245
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069907,
	'Rezina District,Moldova',
	'2498',
	'MD',
	'District',
	'Rezina District',
	'MD-RE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.8317
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069909,
	'Floresti District,Moldova',
	'2498',
	'MD',
	'District',
	'Floresti District',
	'MD-FL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.8377
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069910,
	'Stefan Voda District,Moldova',
	'2498',
	'MD',
	'District',
	'Stefan Voda District',
	'MD-SV',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.8436
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069911,
	'Taraclia District,Moldova',
	'2498',
	'MD',
	'District',
	'Taraclia District',
	'MD-TA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.8506
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069912,
	'Transnistria,Moldova',
	'2498',
	'MD',
	'Region',
	'Transnistria',
	'MD-SN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.8566
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069913,
	'Straseni District,Moldova',
	'2498',
	'MD',
	'District',
	'Straseni District',
	'MD-ST',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.8626
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069914,
	'Donduseni District,Moldova',
	'2498',
	'MD',
	'District',
	'Donduseni District',
	'MD-DO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.8696
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069915,
	'Cahul District,Moldova',
	'2498',
	'MD',
	'District',
	'Cahul District',
	'MD-CA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.8756
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069916,
	'Riscani District,Moldova',
	'2498',
	'MD',
	'District',
	'Riscani District',
	'MD-RI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.8826
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069918,
	'Cantemir District,Moldova',
	'2498',
	'MD',
	'District',
	'Cantemir District',
	'MD-CT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.8886
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069919,
	'Orhei District,Moldova',
	'2498',
	'MD',
	'District',
	'Orhei District',
	'MD-OR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.8957
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069920,
	'Leova District,Moldova',
	'2498',
	'MD',
	'District',
	'Leova District',
	'MD-LE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.9016
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069921,
	'Criuleni District,Moldova',
	'2498',
	'MD',
	'District',
	'Criuleni District',
	'MD-CR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.9076
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069922,
	'Anenii Noi,Moldova',
	'2498',
	'MD',
	'District',
	'Anenii Noi',
	'MD-AN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.9147
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069923,
	'Dubasari District,Moldova',
	'2498',
	'MD',
	'District',
	'Dubasari District',
	'MD-DU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.9217
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069924,
	'Gagauzia,Moldova',
	'2498',
	'MD',
	'Region',
	'Gagauzia',
	'MD-GA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.9277
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069927,
	'Falesti District,Moldova',
	'2498',
	'MD',
	'District',
	'Falesti District',
	'MD-FA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.9347
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069928,
	'Glodeni District,Moldova',
	'2498',
	'MD',
	'District',
	'Glodeni District',
	'MD-GL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.9397
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069929,
	'Soroca District,Moldova',
	'2498',
	'MD',
	'District',
	'Soroca District',
	'MD-SO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.9457
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069930,
	'Drochia District,Moldova',
	'2498',
	'MD',
	'District',
	'Drochia District',
	'MD-DR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.9529
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069931,
	'Singerei District,Moldova',
	'2498',
	'MD',
	'District',
	'Singerei District',
	'MD-SI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.9589
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069932,
	'Ocnita District,Moldova',
	'2498',
	'MD',
	'District',
	'Ocnita District',
	'MD-OC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.9659
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069933,
	'Causeni District,Moldova',
	'2498',
	'MD',
	'District',
	'Causeni District',
	'MD-CS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.9719
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069934,
	'Ialoveni District,Moldova',
	'2498',
	'MD',
	'District',
	'Ialoveni District',
	'MD-IA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.9779
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069937,
	'Siem Reap Province,Cambodia',
	'2116',
	'KH',
	'Province',
	'Siem Reap Province',
	'KH-17',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.9849
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069938,
	'Ratanakiri Province,Cambodia',
	'2116',
	'KH',
	'Province',
	'Ratanakiri Province',
	'KH-16',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.9918
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069939,
	'Kampong Chhnang Province,Cambodia',
	'2116',
	'KH',
	'Province',
	'Kampong Chhnang Province',
	'KH-4',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:09.9988
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069940,
	'Kampong Thom Province,Cambodia',
	'2116',
	'KH',
	'Province',
	'Kampong Thom Province',
	'KH-6',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.0058
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069941,
	'Svay Rieng Province,Cambodia',
	'2116',
	'KH',
	'Province',
	'Svay Rieng Province',
	'KH-20',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.0118
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069942,
	'Kratie Province,Cambodia',
	'2116',
	'KH',
	'Province',
	'Kratie Province',
	'KH-10',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.0188
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069943,
	'Kampong Cham Province,Cambodia',
	'2116',
	'KH',
	'Province',
	'Kampong Cham Province',
	'KH-3',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.0248
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069945,
	'Tbong Khmum Province,Cambodia',
	'2116',
	'KH',
	'Province',
	'Tbong Khmum Province',
	'KH-25',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.0317
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069946,
	'Pursat Province,Cambodia',
	'2116',
	'KH',
	'Province',
	'Pursat Province',
	'KH-15',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.0387
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069949,
	'Kep Province,Cambodia',
	'2116',
	'KH',
	'Province',
	'Kep Province',
	'KH-23',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.0457
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069951,
	'Prey Veng Province,Cambodia',
	'2116',
	'KH',
	'Province',
	'Prey Veng Province',
	'KH-14',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.0517
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069952,
	'Battambang Province,Cambodia',
	'2116',
	'KH',
	'Province',
	'Battambang Province',
	'KH-2',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.0577
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069953,
	'Preah Vihear Province,Cambodia',
	'2116',
	'KH',
	'Province',
	'Preah Vihear Province',
	'KH-13',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.0646
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069954,
	'Koh Kong Province,Cambodia',
	'2116',
	'KH',
	'Province',
	'Koh Kong Province',
	'KH-9',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.0716
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069955,
	'Paraguari Department,Paraguay',
	'2600',
	'PY',
	'Department',
	'Paraguari Department',
	'PY-9',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.0786
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069956,
	'Cordillera Department,Paraguay',
	'2600',
	'PY',
	'Department',
	'Cordillera Department',
	'PY-3',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.0856
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069958,
	'Alto Paraguay Department,Paraguay',
	'2600',
	'PY',
	'Department',
	'Alto Paraguay Department',
	'PY-16',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.0926
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069960,
	'Boqueron department,Paraguay',
	'2600',
	'PY',
	'Department',
	'Boqueron department',
	'PY-19',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.0986
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069961,
	'Canindeyu Department,Paraguay',
	'2600',
	'PY',
	'Department',
	'Canindeyu Department',
	'PY-14',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.1055
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069962,
	'Itapua Department,Paraguay',
	'2600',
	'PY',
	'Department',
	'Itapua Department',
	'PY-7',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.1115
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069963,
	'Caazapa Department,Paraguay',
	'2600',
	'PY',
	'Department',
	'Caazapa Department',
	'PY-6',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.1175
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069964,
	'Caaguazu Department,Paraguay',
	'2600',
	'PY',
	'Department',
	'Caaguazu Department',
	'PY-5',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.1235
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069966,
	'Misiones Department,Paraguay',
	'2600',
	'PY',
	'Department',
	'Misiones Department',
	'PY-8',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.1295
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069968,
	'Neembucu Department,Paraguay',
	'2600',
	'PY',
	'Department',
	'Neembucu Department',
	'PY-12',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.1365
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069969,
	'Alto Parana Department,Paraguay',
	'2600',
	'PY',
	'Department',
	'Alto Parana Department',
	'PY-10',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.1424
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069971,
	'Ocotepeque Department,Honduras',
	'2340',
	'HN',
	'Department',
	'Ocotepeque Department',
	'HN-OC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.1494
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069972,
	'Francisco Morazan Department,Honduras',
	'2340',
	'HN',
	'Department',
	'Francisco Morazan Department',
	'HN-FM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.1544
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069973,
	'La Paz Department,Honduras',
	'2340',
	'HN',
	'Department',
	'La Paz Department',
	'BO-L',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.1604
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069975,
	'Intibuca Department,Honduras',
	'2340',
	'HN',
	'Department',
	'Intibuca Department',
	'HN-IN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.1674
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069976,
	'Comayagua Department,Honduras',
	'2340',
	'HN',
	'Department',
	'Comayagua Department',
	'HN-CM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.1744
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069977,
	'Lempira Department,Honduras',
	'2340',
	'HN',
	'Department',
	'Lempira Department',
	'HN-LE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.1813
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069979,
	'Bay Islands Department,Honduras',
	'2340',
	'HN',
	'Department',
	'Bay Islands Department',
	'HN-IB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.1873
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069981,
	'El Paraiso Department,Honduras',
	'2340',
	'HN',
	'Department',
	'El Paraiso Department',
	'HN-EP',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.1943
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069983,
	'Choluteca Department,Honduras',
	'2340',
	'HN',
	'Department',
	'Choluteca Department',
	'HN-CH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.2003
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069984,
	'Olancho Department,Honduras',
	'2340',
	'HN',
	'Department',
	'Olancho Department',
	'HN-OL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.2063
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069985,
	'Central Region,Uganda',
	'2800',
	'UG',
	'Region',
	'Central Region',
	'UG-C',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.2134
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069986,
	'Western Region,Uganda',
	'2800',
	'UG',
	'Region',
	'Western Region',
	'UG-W',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.2195
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069990,
	'Elbasan County,Albania',
	'2008',
	'AL',
	'County',
	'Elbasan County',
	'AL-03',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.2265
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069992,
	'Tirana County,Albania',
	'2008',
	'AL',
	'County',
	'Tirana County',
	'AL-11',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.2326
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069993,
	'Fier County,Albania',
	'2008',
	'AL',
	'County',
	'Fier County',
	'AL-04',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.2387
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069996,
	'Savanne District,Mauritius',
	'2480',
	'MU',
	'District',
	'Savanne District',
	'MU-SA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.2447
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069997,
	'Pamplemousses District,Mauritius',
	'2480',
	'MU',
	'District',
	'Pamplemousses District',
	'MU-PA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.2506
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9069999,
	'Nueva Segovia Department,Nicaragua',
	'2558',
	'NI',
	'Department',
	'Nueva Segovia Department',
	'NI-NS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.2576
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070000,
	'Chinandega Department,Nicaragua',
	'2558',
	'NI',
	'Department',
	'Chinandega Department',
	'NI-CI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.2637
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070001,
	'Rio San Juan Department,Nicaragua',
	'2558',
	'NI',
	'Department',
	'Rio San Juan Department',
	'NI-SJ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.2698
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070003,
	'Boaco Department,Nicaragua',
	'2558',
	'NI',
	'Department',
	'Boaco Department',
	'NI-BO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.2768
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070004,
	'Carazo Department,Nicaragua',
	'2558',
	'NI',
	'Department',
	'Carazo Department',
	'NI-CA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.2838
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070007,
	'Matagalpa Department,Nicaragua',
	'2558',
	'NI',
	'Department',
	'Matagalpa Department',
	'NI-MT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.2898
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070008,
	'Jinotega Department,Nicaragua',
	'2558',
	'NI',
	'Department',
	'Jinotega Department',
	'NI-JI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.2968
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070011,
	'Rivas Department,Nicaragua',
	'2558',
	'NI',
	'Department',
	'Rivas Department',
	'NI-RI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.3038
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070012,
	'Masaya Department,Nicaragua',
	'2558',
	'NI',
	'Department',
	'Masaya Department',
	'NI-MS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.3108
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070013,
	'Chontales Department,Nicaragua',
	'2558',
	'NI',
	'Department',
	'Chontales Department',
	'NI-CO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.3168
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070014,
	'Esteli Department,Nicaragua',
	'2558',
	'NI',
	'Department',
	'Esteli Department',
	'NI-ES',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.3238
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070015,
	'Madriz Department,Nicaragua',
	'2558',
	'NI',
	'Department',
	'Madriz Department',
	'NI-MD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.3287
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070017,
	'Mid-Western Development Region,Nepal',
	'2524',
	'NP',
	'Region',
	'Mid-Western Development Region',
	'NP-2',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.3469
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070018,
	'Far-Western Development Region,Nepal',
	'2524',
	'NP',
	'Region',
	'Far-Western Development Region',
	'NP-5',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.3578
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070020,
	'Mashonaland Central Province,Zimbabwe',
	'2716',
	'ZW',
	'Province',
	'Mashonaland Central Province',
	'ZW-MC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.3698
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070022,
	'Manicaland Province,Zimbabwe',
	'2716',
	'ZW',
	'Province',
	'Manicaland Province',
	'ZW-MA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.3789
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070023,
	'Matabeleland North Province,Zimbabwe',
	'2716',
	'ZW',
	'Province',
	'Matabeleland North Province',
	'ZW-MN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.3859
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070025,
	'Mashonaland West Province,Zimbabwe',
	'2716',
	'ZW',
	'Province',
	'Mashonaland West Province',
	'ZW-MW',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.3919
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070027,
	'Matabeleland South Province,Zimbabwe',
	'2716',
	'ZW',
	'Province',
	'Matabeleland South Province',
	'ZW-MS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.3989
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070028,
	'Mashonaland East Province,Zimbabwe',
	'2716',
	'ZW',
	'Province',
	'Mashonaland East Province',
	'ZW-ME',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.4060
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070031,
	'Andrijevica Municipality,Montenegro',
	'2499',
	'ME',
	'Municipality',
	'Andrijevica Municipality',
	'ME-01',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.4120
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070033,
	'Cetinje Municipality,Montenegro',
	'2499',
	'ME',
	'Municipality',
	'Cetinje Municipality',
	'ME-06',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.4191
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070034,
	'Niksic Municipality,Montenegro',
	'2499',
	'ME',
	'Municipality',
	'Niksic Municipality',
	'ME-12',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.4251
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070035,
	'Berane Municipality,Montenegro',
	'2499',
	'ME',
	'Municipality',
	'Berane Municipality',
	'ME-03',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.4312
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070036,
	'Danilovgrad Municipality,Montenegro',
	'2499',
	'ME',
	'Municipality',
	'Danilovgrad Municipality',
	'ME-07',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.4371
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070037,
	'Rozaje Municipality,Montenegro',
	'2499',
	'ME',
	'Municipality',
	'Rozaje Municipality',
	'ME-17',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.4431
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070038,
	'Plav Municipality,Montenegro',
	'2499',
	'ME',
	'Municipality',
	'Plav Municipality',
	'ME-13',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.4501
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070039,
	'Savnik Municipality,Montenegro',
	'2499',
	'ME',
	'Municipality',
	'Savnik Municipality',
	'ME-18',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.4561
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070040,
	'Zabljak Municipality,Montenegro',
	'2499',
	'ME',
	'Municipality',
	'Zabljak Municipality',
	'ME-21',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.4621
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070041,
	'Kolasin Municipality,Montenegro',
	'2499',
	'ME',
	'Municipality',
	'Kolasin Municipality',
	'ME-09',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.4681
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070042,
	'Copperbelt Province,Zambia',
	'2894',
	'ZM',
	'Province',
	'Copperbelt Province',
	'ZM-08',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.4752
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070043,
	'Luapula Province,Zambia',
	'2894',
	'ZM',
	'Province',
	'Luapula Province',
	'ZM-04',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.4812
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070046,
	'Western Province,Zambia',
	'2894',
	'ZM',
	'Province',
	'Western Province',
	'ZM-01',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.4882
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070048,
	'Southern Province,Zambia',
	'2894',
	'ZM',
	'Province',
	'Southern Province',
	'ZM-07',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.4942
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070049,
	'Lori Province,Armenia',
	'2051',
	'AM',
	'Province',
	'Lori Province',
	'AM-LO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.5011
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070050,
	'Syunik Province,Armenia',
	'2051',
	'AM',
	'Province',
	'Syunik Province',
	'AM-SU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.5071
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070051,
	'Tavush Province,Armenia',
	'2051',
	'AM',
	'Province',
	'Tavush Province',
	'AM-TV',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.5141
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070052,
	'Armavir Province,Armenia',
	'2051',
	'AM',
	'Province',
	'Armavir Province',
	'AM-AV',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.5203
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070053,
	'Yerevan,Armenia',
	'2051',
	'AM',
	'City',
	'Yerevan',
	'AM-ER',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.5274
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070054,
	'Kotayk Province,Armenia',
	'2051',
	'AM',
	'Province',
	'Kotayk Province',
	'AM-KT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.5334
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070055,
	'Ararat Province,Armenia',
	'2051',
	'AM',
	'Province',
	'Ararat Province',
	'AM-AR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.5394
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070056,
	'Vayots Dzor Province,Armenia',
	'2051',
	'AM',
	'Province',
	'Vayots Dzor Province',
	'AM-VD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.5464
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070057,
	'Gegharkunik Province,Armenia',
	'2051',
	'AM',
	'Province',
	'Gegharkunik Province',
	'AM-GR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.5524
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070058,
	'Shirak Province,Armenia',
	'2051',
	'AM',
	'Province',
	'Shirak Province',
	'AM-SH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.5583
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070059,
	'Aragatsotn Province,Armenia',
	'2051',
	'AM',
	'Province',
	'Aragatsotn Province',
	'AM-AG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.5643
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070060,
	'Ziguinchor,Senegal',
	'2686',
	'SN',
	'Region',
	'Ziguinchor',
	'SN-ZG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.5713
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070061,
	'Kaolack Region,Senegal',
	'2686',
	'SN',
	'Region',
	'Kaolack Region',
	'SN-KL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.5793
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070063,
	'Louga Region,Senegal',
	'2686',
	'SN',
	'Region',
	'Louga Region',
	'SN-LG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.5863
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070064,
	'Kolda Region,Senegal',
	'2686',
	'SN',
	'Region',
	'Kolda Region',
	'SN-KD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.5943
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070066,
	'Tambacounda Region,Senegal',
	'2686',
	'SN',
	'Region',
	'Tambacounda Region',
	'SN-TC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.6033
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070067,
	'Matam Region,Senegal',
	'2686',
	'SN',
	'Region',
	'Matam Region',
	'SN-MT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.6094
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070068,
	'Fatick Region,Senegal',
	'2686',
	'SN',
	'Region',
	'Fatick Region',
	'SN-FK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.6154
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070069,
	'Kaffrine Region,Senegal',
	'2686',
	'SN',
	'Region',
	'Kaffrine Region',
	'SN-KA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.6224
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070071,
	'Saint-Louis Region,Senegal',
	'2686',
	'SN',
	'Region',
	'Saint-Louis Region',
	'SN-SL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.6284
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070072,
	'West,Cameroon',
	'2120',
	'CM',
	'Region',
	'West',
	'CM-OU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.6345
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070073,
	'Littoral,Cameroon',
	'2120',
	'CM',
	'Region',
	'Littoral',
	'CM-LT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.6405
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070074,
	'East,Cameroon',
	'2120',
	'CM',
	'Region',
	'East',
	'CM-ES',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.6475
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070076,
	'Southwest,Cameroon',
	'2120',
	'CM',
	'Region',
	'Southwest',
	'CM-SW',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.6535
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070077,
	'Northwest,Cameroon',
	'2120',
	'CM',
	'Region',
	'Northwest',
	'CM-NW',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.6605
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070079,
	'South,Cameroon',
	'2120',
	'CM',
	'Region',
	'South',
	'LB-JA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.6664
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070080,
	'Khomas Region,Namibia',
	'2516',
	'NA',
	'Region',
	'Khomas Region',
	'NA-KH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.6744
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070081,
	'Oshikoto Region,Namibia',
	'2516',
	'NA',
	'Region',
	'Oshikoto Region',
	'NA-OT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.6805
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070082,
	'Otjozondjupa Region,Namibia',
	'2516',
	'NA',
	'Region',
	'Otjozondjupa Region',
	'NA-OD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.6864
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070083,
	'Oshana Region,Namibia',
	'2516',
	'NA',
	'Region',
	'Oshana Region',
	'NA-ON',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.6934
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070084,
	'Zambezi Region,Namibia',
	'2516',
	'NA',
	'Region',
	'Zambezi Region',
	'NA-CA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.7004
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070085,
	'Omusati Region,Namibia',
	'2516',
	'NA',
	'Region',
	'Omusati Region',
	'NA-OS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.7074
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070086,
	'Kavango Region,Namibia',
	'2516',
	'NA',
	'Region',
	'Kavango Region',
	'NA-OK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.7144
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070087,
	'Hardap Region,Namibia',
	'2516',
	'NA',
	'Region',
	'Hardap Region',
	'NA-HA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.7213
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070088,
	'Kunene Region,Namibia',
	'2516',
	'NA',
	'Region',
	'Kunene Region',
	'NA-KU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.7273
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070089,
	'Karas Region,Namibia',
	'2516',
	'NA',
	'Region',
	'Karas Region',
	'NA-KA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.7343
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070090,
	'Benguela Province,Angola',
	'2024',
	'AO',
	'Province',
	'Benguela Province',
	'AO-BGU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.7403
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070091,
	'Cuando Cubango Province,Angola',
	'2024',
	'AO',
	'Province',
	'Cuando Cubango Province',
	'AO-CCU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.7473
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070092,
	'Moxico Province,Angola',
	'2024',
	'AO',
	'Province',
	'Moxico Province',
	'AO-MOX',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.7553
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070094,
	'Bengo Province,Angola',
	'2024',
	'AO',
	'Province',
	'Bengo Province',
	'AO-BGO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.7612
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070095,
	'Zaire Province,Angola',
	'2024',
	'AO',
	'Province',
	'Zaire Province',
	'AO-ZAI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.7682
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070096,
	'Lunda Sul Province,Angola',
	'2024',
	'AO',
	'Province',
	'Lunda Sul Province',
	'AO-LSU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.7742
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070097,
	'Huila Province,Angola',
	'2024',
	'AO',
	'Province',
	'Huila Province',
	'AO-HUI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.7802
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070098,
	'Namibe Province,Angola',
	'2024',
	'AO',
	'Province',
	'Namibe Province',
	'AO-NAM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.7862
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070100,
	'Cabinda Province,Angola',
	'2024',
	'AO',
	'Province',
	'Cabinda Province',
	'AO-CAB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.7922
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070101,
	'Cuanza Norte Province,Angola',
	'2024',
	'AO',
	'Province',
	'Cuanza Norte Province',
	'AO-CNO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.7991
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070102,
	'Huambo Province,Angola',
	'2024',
	'AO',
	'Province',
	'Huambo Province',
	'AO-HUA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.8041
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070103,
	'Saint George,Barbados',
	'2052',
	'BB',
	'Province',
	'Saint George',
	'VC-04',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.8111
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070104,
	'Saint Peter,Barbados',
	'2052',
	'BB',
	'Province',
	'Saint Peter',
	'BB-09',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.8161
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070106,
	'Saint Andrew,Barbados',
	'2052',
	'BB',
	'Province',
	'Saint Andrew',
	'VC-02',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.8221
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070108,
	'Saint Lucy,Barbados',
	'2052',
	'BB',
	'Province',
	'Saint Lucy',
	'BB-07',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.8281
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070109,
	'Saint Philip,Barbados',
	'2052',
	'BB',
	'Province',
	'Saint Philip',
	'BB-10',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.8340
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070110,
	'Christ Church,Barbados',
	'2052',
	'BB',
	'Province',
	'Christ Church',
	'BB-01',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.8400
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070111,
	'Niassa Province,Mozambique',
	'2508',
	'MZ',
	'Province',
	'Niassa Province',
	'MZ-A',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.8460
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070112,
	'Nampula Province,Mozambique',
	'2508',
	'MZ',
	'Province',
	'Nampula Province',
	'MZ-N',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.8520
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070113,
	'Sofala Province,Mozambique',
	'2508',
	'MZ',
	'Province',
	'Sofala Province',
	'MZ-S',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.8590
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070114,
	'Maputo Province,Mozambique',
	'2508',
	'MZ',
	'Province',
	'Maputo Province',
	'MZ-L',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.8661
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070115,
	'Inhambane Province,Mozambique',
	'2508',
	'MZ',
	'Province',
	'Inhambane Province',
	'MZ-I',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.8731
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070116,
	'Tete Province,Mozambique',
	'2508',
	'MZ',
	'Province',
	'Tete Province',
	'MZ-T',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.8794
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070117,
	'Manica Province,Mozambique',
	'2508',
	'MZ',
	'Province',
	'Manica Province',
	'MZ-B',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.8863
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070118,
	'Cabo Delgado Province,Mozambique',
	'2508',
	'MZ',
	'Province',
	'Cabo Delgado Province',
	'MZ-P',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.8923
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070119,
	'Belait District,Brunei',
	'2096',
	'BN',
	'District',
	'Belait District',
	'BN-BE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.8994
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070120,
	'Tutong District,Brunei',
	'2096',
	'BN',
	'District',
	'Tutong District',
	'BN-TU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.9054
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070121,
	'South-East District,Botswana',
	'2072',
	'BW',
	'District',
	'South-East District',
	'BW-SE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.9114
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070122,
	'Kgalagadi District,Botswana',
	'2072',
	'BW',
	'District',
	'Kgalagadi District',
	'BW-KG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.9184
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070123,
	'Kweneng District,Botswana',
	'2072',
	'BW',
	'District',
	'Kweneng District',
	'BW-KW',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.9244
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070124,
	'Kgatleng District,Botswana',
	'2072',
	'BW',
	'District',
	'Kgatleng District',
	'BW-KL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.9304
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070125,
	'Ghanzi District,Botswana',
	'2072',
	'BW',
	'District',
	'Ghanzi District',
	'BW-GH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.9364
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070127,
	'North-East District,Botswana',
	'2072',
	'BW',
	'District',
	'North-East District',
	'BW-NE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.9424
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070129,
	'Nord-Est Department,Haiti',
	'2332',
	'HT',
	'Department',
	'Nord-Est Department',
	'HT-NE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.9484
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070130,
	'Nord-Ouest Department,Haiti',
	'2332',
	'HT',
	'Department',
	'Nord-Ouest Department',
	'HT-NO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.9554
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070131,
	'Sud-Est Department,Haiti',
	'2332',
	'HT',
	'Department',
	'Sud-Est Department',
	'HT-SE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.9614
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070132,
	'Grand''Anse Department,Haiti',
	'2332',
	'HT',
	'Department',
	'Grand''Anse Department',
	'HT-GA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.9674
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070134,
	'Ouest Department,Haiti',
	'2332',
	'HT',
	'Department',
	'Ouest Department',
	'HT-OU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.9734
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070135,
	'Artibonite Department,Haiti',
	'2332',
	'HT',
	'Department',
	'Artibonite Department',
	'HT-AR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.9794
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070137,
	'Nippes Department,Haiti',
	'2332',
	'HT',
	'Department',
	'Nippes Department',
	'HT-NI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.9864
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070138,
	'Ghat District,Libya',
	'2434',
	'LY',
	'District',
	'Ghat District',
	'LY-GT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.9914
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070147,
	'Al Wahat District,Libya',
	'2434',
	'LY',
	'District',
	'Al Wahat District',
	'LY-WA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:10.9984
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070156,
	'Nalut District,Libya',
	'2434',
	'LY',
	'District',
	'Nalut District',
	'LY-NL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.0054
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070157,
	'Murzuq District,Libya',
	'2434',
	'LY',
	'District',
	'Murzuq District',
	'LY-MQ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.0113
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070175,
	'Fianarantsoa Province,Madagascar',
	'2450',
	'MG',
	'Province',
	'Fianarantsoa Province',
	'MG-F',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.0174
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070177,
	'Toliara Province,Madagascar',
	'2450',
	'MG',
	'Province',
	'Toliara Province',
	'MG-U',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.0234
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070180,
	'Antsiranana Province,Madagascar',
	'2450',
	'MG',
	'Province',
	'Antsiranana Province',
	'MG-D',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.0303
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070181,
	'Kayah State,Myanmar (Burma)',
	'2104',
	'MM',
	'State',
	'Kayah State',
	'MM-12',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.0363
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070182,
	'Western Province,Rwanda',
	'2646',
	'RW',
	'Province',
	'Western Province',
	'LK-1',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.0423
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070185,
	'Bolikhamsai Province,Laos',
	'2418',
	'LA',
	'Province',
	'Bolikhamsai Province',
	'LA-BL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.0503
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070186,
	'Houaphanh Province,Laos',
	'2418',
	'LA',
	'Province',
	'Houaphanh Province',
	'LA-HO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.0563
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070187,
	'Champasak Province,Laos',
	'2418',
	'LA',
	'Province',
	'Champasak Province',
	'LA-CH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.0634
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070188,
	'Bokeo Province,Laos',
	'2418',
	'LA',
	'Province',
	'Bokeo Province',
	'LA-BK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.0684
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070189,
	'Savannakhet Province,Laos',
	'2418',
	'LA',
	'Province',
	'Savannakhet Province',
	'LA-SV',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.0753
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070190,
	'Xiangkhouang Province,Laos',
	'2418',
	'LA',
	'Province',
	'Xiangkhouang Province',
	'LA-XI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.0813
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070191,
	'Salavan Province,Laos',
	'2418',
	'LA',
	'Province',
	'Salavan Province',
	'LA-SL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.0884
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070193,
	'Khammouane Province,Laos',
	'2418',
	'LA',
	'Province',
	'Khammouane Province',
	'LA-KH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.0956
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070194,
	'Oudomxay Province,Laos',
	'2418',
	'LA',
	'Province',
	'Oudomxay Province',
	'LA-OU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.1016
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070196,
	'Luang Namtha Province,Laos',
	'2418',
	'LA',
	'Province',
	'Luang Namtha Province',
	'LA-LM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.1096
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070197,
	'Attapeu Province,Laos',
	'2418',
	'LA',
	'Province',
	'Attapeu Province',
	'LA-AT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.1156
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070200,
	'Phongsaly Province,Laos',
	'2418',
	'LA',
	'Province',
	'Phongsaly Province',
	'LA-PH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.1216
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070201,
	'Vientiane Prefecture,Laos',
	'2418',
	'LA',
	'Province',
	'Vientiane Prefecture',
	'LA-VT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.1276
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070202,
	'Raymah Governorate,Yemen',
	'2887',
	'YE',
	'Governorate',
	'Raymah Governorate',
	'YE-RA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.1328
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070203,
	'Shabwah Governorate,Yemen',
	'2887',
	'YE',
	'Governorate',
	'Shabwah Governorate',
	'YE-SH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.1398
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070205,
	'Al Hudaydah Governorate,Yemen',
	'2887',
	'YE',
	'Governorate',
	'Al Hudaydah Governorate',
	'YE-HU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.1458
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070206,
	'Al Mahrah Governorate,Yemen',
	'2887',
	'YE',
	'Governorate',
	'Al Mahrah Governorate',
	'YE-MR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.1528
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070210,
	'Dhamar Governorate,Yemen',
	'2887',
	'YE',
	'Governorate',
	'Dhamar Governorate',
	'YE-DH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.1598
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070211,
	'Al Bayda'' Governorate,Yemen',
	'2887',
	'YE',
	'Governorate',
	'Al Bayda'' Governorate',
	'YE-BA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.1658
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070212,
	'Hajjah Governorate,Yemen',
	'2887',
	'YE',
	'Governorate',
	'Hajjah Governorate',
	'YE-HJ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.1727
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070213,
	'Abyan Governorate,Yemen',
	'2887',
	'YE',
	'Governorate',
	'Abyan Governorate',
	'YE-AB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.1787
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070214,
	'Ad Dali'' Governorate,Yemen',
	'2887',
	'YE',
	'Governorate',
	'Ad Dali'' Governorate',
	'YE-DA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.1837
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070215,
	'Lahij Governorate,Yemen',
	'2887',
	'YE',
	'Governorate',
	'Lahij Governorate',
	'YE-LA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.1897
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070218,
	'Ma''rib Governorate,Yemen',
	'2887',
	'YE',
	'Governorate',
	'Ma''rib Governorate',
	'YE-MA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.1947
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070219,
	'Ibb Governorate,Yemen',
	'2887',
	'YE',
	'Governorate',
	'Ibb Governorate',
	'YE-IB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.2007
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070220,
	'''Amran Governorate,Yemen',
	'2887',
	'YE',
	'Governorate',
	'''Amran Governorate',
	'YE-AM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.2066
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070221,
	'Al Mahwit Governorate,Yemen',
	'2887',
	'YE',
	'Governorate',
	'Al Mahwit Governorate',
	'YE-MW',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.2126
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070222,
	'Stann Creek District,Belize',
	'2084',
	'BZ',
	'District',
	'Stann Creek District',
	'BZ-SC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.2176
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070224,
	'Orange Walk District,Belize',
	'2084',
	'BZ',
	'District',
	'Orange Walk District',
	'BZ-OW',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.2236
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070226,
	'Cayo District,Belize',
	'2084',
	'BZ',
	'District',
	'Cayo District',
	'BZ-CY',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.2296
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070228,
	'Zou Department,Benin',
	'2204',
	'BJ',
	'Department',
	'Zou Department',
	'BJ-ZO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.2366
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070229,
	'Collines Department,Benin',
	'2204',
	'BJ',
	'Department',
	'Collines Department',
	'BJ-CO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.2426
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070231,
	'Oueme Department,Benin',
	'2204',
	'BJ',
	'Department',
	'Oueme Department',
	'BJ-OU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.2485
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070232,
	'Atlantique Department,Benin',
	'2204',
	'BJ',
	'Department',
	'Atlantique Department',
	'BJ-AQ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.2535
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070233,
	'Atakora Department,Benin',
	'2204',
	'BJ',
	'Department',
	'Atakora Department',
	'BJ-AK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.2605
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070234,
	'Kouffo Department,Benin',
	'2204',
	'BJ',
	'Department',
	'Kouffo Department',
	'BJ-KO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.2665
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070237,
	'Segou Region,Mali',
	'2466',
	'ML',
	'Region',
	'Segou Region',
	'ML-4',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.2725
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070238,
	'Sikasso Region,Mali',
	'2466',
	'ML',
	'Region',
	'Sikasso Region',
	'ML-3',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.2785
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070239,
	'Gao Region,Mali',
	'2466',
	'ML',
	'Region',
	'Gao Region',
	'ML-7',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.2844
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070240,
	'Koulikoro Region,Mali',
	'2466',
	'ML',
	'Region',
	'Koulikoro Region',
	'ML-2',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.2904
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070241,
	'Tombouctou Region,Mali',
	'2466',
	'ML',
	'Region',
	'Tombouctou Region',
	'ML-6',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.2964
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070242,
	'Mopti Region,Mali',
	'2466',
	'ML',
	'Region',
	'Mopti Region',
	'ML-5',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.3024
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070243,
	'Kidal Region,Mali',
	'2466',
	'ML',
	'Region',
	'Kidal Region',
	'ML-8',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.3084
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070246,
	'Sud-Ouest Region,Burkina Faso',
	'2854',
	'BF',
	'Region',
	'Sud-Ouest Region',
	'BF-13',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.3154
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070248,
	'Boucle du Mouhoun Region,Burkina Faso',
	'2854',
	'BF',
	'Region',
	'Boucle du Mouhoun Region',
	'BF-01',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.3213
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070249,
	'Hauts-Bassins Region,Burkina Faso',
	'2854',
	'BF',
	'Region',
	'Hauts-Bassins Region',
	'BF-09',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.3273
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070250,
	'Cascades Region,Burkina Faso',
	'2854',
	'BF',
	'Region',
	'Cascades Region',
	'BF-02',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.3343
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070251,
	'Centre-Ouest Region,Burkina Faso',
	'2854',
	'BF',
	'Region',
	'Centre-Ouest Region',
	'BF-06',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.3403
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070253,
	'Sahel Region,Burkina Faso',
	'2854',
	'BF',
	'Region',
	'Sahel Region',
	'BF-12',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.3463
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070255,
	'Est Region,Burkina Faso',
	'2854',
	'BF',
	'Region',
	'Est Region',
	'BF-08',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.3533
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070256,
	'Sermersooq Municipality,Greenland',
	'2304',
	'GL',
	'Municipality',
	'Sermersooq Municipality',
	'GL-SM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.3602
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070257,
	'Qeqqata Municipality,Greenland',
	'2304',
	'GL',
	'Municipality',
	'Qeqqata Municipality',
	'GL-QE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.3672
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070258,
	'Qaasuitsup Municipality,Greenland',
	'2304',
	'GL',
	'Municipality',
	'Qaasuitsup Municipality',
	'GL-QA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.3742
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070259,
	'Kujalleq Municipality,Greenland',
	'2304',
	'GL',
	'Municipality',
	'Kujalleq Municipality',
	'GL-KU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.3802
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070260,
	'Savanes Region,Togo',
	'2768',
	'TG',
	'Region',
	'Savanes Region',
	'TG-S',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.3862
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070261,
	'Centrale Region,Togo',
	'2768',
	'TG',
	'Region',
	'Centrale Region',
	'TG-C',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.3931
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070262,
	'Kara Region,Togo',
	'2768',
	'TG',
	'Region',
	'Kara Region',
	'TG-K',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.3991
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070263,
	'Maritime Region,Togo',
	'2768',
	'TG',
	'Region',
	'Maritime Region',
	'TG-M',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.4051
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070264,
	'Plateaux Region,Togo',
	'2768',
	'TG',
	'Region',
	'Plateaux Region',
	'TG-P',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.4101
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070265,
	'Shiselweni Region,Swaziland',
	'2748',
	'SZ',
	'Region',
	'Shiselweni Region',
	'SZ-SH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.4171
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070266,
	'Lubombo Region,Swaziland',
	'2748',
	'SZ',
	'Region',
	'Lubombo Region',
	'SZ-LU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.4231
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070267,
	'Hhohho Region,Swaziland',
	'2748',
	'SZ',
	'Region',
	'Hhohho Region',
	'SZ-HH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.4291
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070269,
	'Khatlon Province,Tajikistan',
	'2762',
	'TJ',
	'Province',
	'Khatlon Province',
	'TJ-KT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.4361
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070270,
	'Sughd Province,Tajikistan',
	'2762',
	'TJ',
	'Province',
	'Sughd Province',
	'TJ-SU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.4411
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070271,
	'Gorno-Badakhshan Autonomous Province,Tajikistan',
	'2762',
	'TJ',
	'Province',
	'Gorno-Badakhshan Autonomous Province',
	'TJ-GB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.4490
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070272,
	'Districts of Republican Subordination,Tajikistan',
	'2762',
	'TJ',
	'Province',
	'Districts of Republican Subordination',
	'TJ-RR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.4550
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070273,
	'Dosso Region,Niger',
	'2562',
	'NE',
	'Region',
	'Dosso Region',
	'NE-3',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.4610
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070274,
	'Diffa Region,Niger',
	'2562',
	'NE',
	'Region',
	'Diffa Region',
	'NE-2',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.4670
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070275,
	'Maradi Region,Niger',
	'2562',
	'NE',
	'Region',
	'Maradi Region',
	'NE-4',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.4730
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070276,
	'Tillaberi Region,Niger',
	'2562',
	'NE',
	'Region',
	'Tillaberi Region',
	'NE-6',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.4790
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070277,
	'Tahoua Region,Niger',
	'2562',
	'NE',
	'Region',
	'Tahoua Region',
	'NE-5',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.4869
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070278,
	'Agadez Region,Niger',
	'2562',
	'NE',
	'Region',
	'Agadez Region',
	'NE-1',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.4930
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070279,
	'Zinder Region,Niger',
	'2562',
	'NE',
	'Region',
	'Zinder Region',
	'NE-7',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.4990
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070285,
	'Al Rayyan Municipality,Qatar',
	'2634',
	'QA',
	'Municipality',
	'Al Rayyan Municipality',
	'QA-RA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.5060
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070286,
	'Atyrau Province,Kazakhstan',
	'2398',
	'KZ',
	'Province',
	'Atyrau Province',
	'KZ-ATY',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.5121
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070287,
	'North Kazakhstan Province,Kazakhstan',
	'2398',
	'KZ',
	'Province',
	'North Kazakhstan Province',
	'KZ-SEV',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.5181
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070288,
	'Jambyl Province,Kazakhstan',
	'2398',
	'KZ',
	'Province',
	'Jambyl Province',
	'KZ-ZHA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.5241
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070289,
	'Akmola Province,Kazakhstan',
	'2398',
	'KZ',
	'Province',
	'Akmola Province',
	'KZ-AKM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.5311
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070290,
	'Almaty Province,Kazakhstan',
	'2398',
	'KZ',
	'Province',
	'Almaty Province',
	'KZ-ALM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.5372
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070291,
	'Mangystau Province,Kazakhstan',
	'2398',
	'KZ',
	'Province',
	'Mangystau Province',
	'KZ-MAN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.5432
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070292,
	'Karagandy Province,Kazakhstan',
	'2398',
	'KZ',
	'Province',
	'Karagandy Province',
	'KZ-KAR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.5492
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070297,
	'Guanacaste Province,Costa Rica',
	'2188',
	'CR',
	'Province',
	'Guanacaste Province',
	'CR-G',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.5552
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070298,
	'Puntarenas Province,Costa Rica',
	'2188',
	'CR',
	'Province',
	'Puntarenas Province',
	'CR-P',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.5602
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070300,
	'Maldonado Department,Uruguay',
	'2858',
	'UY',
	'Department',
	'Maldonado Department',
	'UY-MA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.5672
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070302,
	'Cerro Largo Department,Uruguay',
	'2858',
	'UY',
	'Department',
	'Cerro Largo Department',
	'UY-CL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.5732
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070305,
	'Tindouf Province,Algeria',
	'2012',
	'DZ',
	'Province',
	'Tindouf Province',
	'DZ-37',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.5802
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070355,
	'Beqaa Governorate,Lebanon',
	'2422',
	'LB',
	'Governorate',
	'Beqaa Governorate',
	'LB-BI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.5873
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070356,
	'Dhaka Division,Bangladesh',
	'2050',
	'BD',
	'Province',
	'Dhaka Division',
	'BD-C',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.5944
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070357,
	'Rangpur Division,Bangladesh',
	'2050',
	'BD',
	'Province',
	'Rangpur Division',
	'BD-F',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.6004
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070358,
	'Khulna Division,Bangladesh',
	'2050',
	'BD',
	'Province',
	'Khulna Division',
	'BD-D',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.6063
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070359,
	'Chittagong Division,Bangladesh',
	'2050',
	'BD',
	'Province',
	'Chittagong Division',
	'BD-B',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.6133
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070360,
	'Suchitepequez,Guatemala',
	'2320',
	'GT',
	'Department',
	'Suchitepequez',
	'GT-SU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.6203
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070361,
	'Alta Verapaz Department,Guatemala',
	'2320',
	'GT',
	'Department',
	'Alta Verapaz Department',
	'GT-AV',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.6263
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070362,
	'Izabal Department,Guatemala',
	'2320',
	'GT',
	'Department',
	'Izabal Department',
	'GT-IZ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.6323
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070363,
	'Baja Verapaz Department,Guatemala',
	'2320',
	'GT',
	'Department',
	'Baja Verapaz Department',
	'GT-BV',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.6393
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070365,
	'Southern Region,Iceland',
	'2352',
	'IS',
	'Region',
	'Southern Region',
	'MW-S',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.6453
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070366,
	'Northeastern Region,Iceland',
	'2352',
	'IS',
	'Region',
	'Northeastern Region',
	'IS-6',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.6553
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070368,
	'Volta Region,Ghana',
	'2288',
	'GH',
	'Region',
	'Volta Region',
	'GH-TV',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.6603
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070369,
	'Northern Region,Ghana',
	'2288',
	'GH',
	'Region',
	'Northern Region',
	'UG-N',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.6663
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070370,
	'Upper West Region,Ghana',
	'2288',
	'GH',
	'Region',
	'Upper West Region',
	'GH-UW',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.6733
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070371,
	'Upper East Region,Ghana',
	'2288',
	'GH',
	'Region',
	'Upper East Region',
	'GH-UE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.6794
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070374,
	'Saint Thomas Parish,Jamaica',
	'2388',
	'JM',
	'Region',
	'Saint Thomas Parish',
	'JM-03',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.6854
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070375,
	'Saint Catherine Parish,Jamaica',
	'2388',
	'JM',
	'Region',
	'Saint Catherine Parish',
	'JM-14',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.6914
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070376,
	'Manchester Parish,Jamaica',
	'2388',
	'JM',
	'Region',
	'Manchester Parish',
	'JM-12',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.6983
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070377,
	'Trelawny Parish,Jamaica',
	'2388',
	'JM',
	'Region',
	'Trelawny Parish',
	'JM-07',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.7043
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070382,
	'Unguja North Region,Tanzania',
	'2834',
	'TZ',
	'Region',
	'Unguja North Region',
	'TZ-07',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.7104
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070383,
	'Lindi Region,Tanzania',
	'2834',
	'TZ',
	'Region',
	'Lindi Region',
	'TZ-12',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.7164
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070384,
	'Mtwara Region,Tanzania',
	'2834',
	'TZ',
	'Region',
	'Mtwara Region',
	'TZ-17',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.7225
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070386,
	'Nakhchivan Autonomous Republic,Azerbaijan',
	'2031',
	'AZ',
	'Region',
	'Nakhchivan Autonomous Republic',
	'AZ-NX',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.7285
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070387,
	'Cabanas Department,El Salvador',
	'2222',
	'SV',
	'Department',
	'Cabanas Department',
	'SV-CA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.7355
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070388,
	'Chalatenango Department,El Salvador',
	'2222',
	'SV',
	'Department',
	'Chalatenango Department',
	'SV-CH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.7415
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070390,
	'Nisporeni District,Moldova',
	'2498',
	'MD',
	'District',
	'Nisporeni District',
	'MD-NI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.7485
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070391,
	'Ungheni District,Moldova',
	'2498',
	'MD',
	'District',
	'Ungheni District',
	'MD-UN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.7535
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070392,
	'Hincesti District,Moldova',
	'2498',
	'MD',
	'District',
	'Hincesti District',
	'MD-HI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.7605
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070393,
	'Briceni District,Moldova',
	'2498',
	'MD',
	'District',
	'Briceni District',
	'MD-BR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.7665
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070395,
	'Pailin Province,Cambodia',
	'2116',
	'KH',
	'Province',
	'Pailin Province',
	'KH-24',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.7735
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070396,
	'Mondulkiri Province,Cambodia',
	'2116',
	'KH',
	'Province',
	'Mondulkiri Province',
	'KH-11',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.7805
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070397,
	'Kampot Province,Cambodia',
	'2116',
	'KH',
	'Province',
	'Kampot Province',
	'KH-7',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.7865
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070398,
	'Phnom Penh,Cambodia',
	'2116',
	'KH',
	'Province',
	'Phnom Penh',
	'KH-12',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.7924
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070399,
	'Banteay Meanchey Province,Cambodia',
	'2116',
	'KH',
	'Province',
	'Banteay Meanchey Province',
	'KH-1',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.7985
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070400,
	'Atlantida Department,Honduras',
	'2340',
	'HN',
	'Department',
	'Atlantida Department',
	'HN-AT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.8045
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070402,
	'Gracias a Dios Department,Honduras',
	'2340',
	'HN',
	'Department',
	'Gracias a Dios Department',
	'HN-GD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.8105
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070403,
	'Savanes District,Cote d''Ivoire',
	'2384',
	'CI',
	'District',
	'Savanes District',
	'CI-03',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.8175
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070404,
	'Vallee du Bandama District,Cote d''Ivoire',
	'2384',
	'CI',
	'District',
	'Vallee du Bandama District',
	'CI-04',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.8236
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070405,
	'Denguele District,Cote d''Ivoire',
	'2384',
	'CI',
	'District',
	'Denguele District',
	'CI-10',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.8306
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070406,
	'Zanzan District,Cote d''Ivoire',
	'2384',
	'CI',
	'District',
	'Zanzan District',
	'CI-08',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.8367
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070409,
	'Grand Port District,Mauritius',
	'2480',
	'MU',
	'District',
	'Grand Port District',
	'MU-GP',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.8427
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070412,
	'Rodrigues District,Mauritius',
	'2480',
	'MU',
	'District',
	'Rodrigues District',
	'MU-RO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.8488
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070413,
	'Flacq District,Mauritius',
	'2480',
	'MU',
	'District',
	'Flacq District',
	'MU-FL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.8548
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070415,
	'Plaines Wilhems District,Mauritius',
	'2480',
	'MU',
	'District',
	'Plaines Wilhems District',
	'MU-PW',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.8618
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070416,
	'Bijelo Polje Municipality,Montenegro',
	'2499',
	'ME',
	'Municipality',
	'Bijelo Polje Municipality',
	'ME-04',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.8668
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070417,
	'Kotor Municipality,Montenegro',
	'2499',
	'ME',
	'Municipality',
	'Kotor Municipality',
	'ME-10',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.8738
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070418,
	'Pluzine Municipality,Montenegro',
	'2499',
	'ME',
	'Municipality',
	'Pluzine Municipality',
	'ME-15',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.8788
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070419,
	'Mojkovac Municipality,Montenegro',
	'2499',
	'ME',
	'Municipality',
	'Mojkovac Municipality',
	'ME-11',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.8848
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070422,
	'Pljevlja Municipality,Montenegro',
	'2499',
	'ME',
	'Municipality',
	'Pljevlja Municipality',
	'ME-14',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.8917
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070423,
	'Tivat Municipality,Montenegro',
	'2499',
	'ME',
	'Municipality',
	'Tivat Municipality',
	'ME-19',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.8977
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070425,
	'Diourbel Region,Senegal',
	'2686',
	'SN',
	'Region',
	'Diourbel Region',
	'SN-DB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.9047
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070427,
	'Omaheke Region,Namibia',
	'2516',
	'NA',
	'Region',
	'Omaheke Region',
	'NA-OH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.9107
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070428,
	'Ohangwena Region,Namibia',
	'2516',
	'NA',
	'Region',
	'Ohangwena Region',
	'NA-OW',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.9187
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070429,
	'Erongo Region,Namibia',
	'2516',
	'NA',
	'Region',
	'Erongo Region',
	'NA-ER',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.9257
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070430,
	'Lunda Norte Province,Angola',
	'2024',
	'AO',
	'Province',
	'Lunda Norte Province',
	'AO-LNO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.9317
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070431,
	'Luanda Province,Angola',
	'2024',
	'AO',
	'Province',
	'Luanda Province',
	'AO-LUA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.9387
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070432,
	'Cunene Province,Angola',
	'2024',
	'AO',
	'Province',
	'Cunene Province',
	'AO-CNN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.9466
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070433,
	'Cuanza Sul Province,Angola',
	'2024',
	'AO',
	'Province',
	'Cuanza Sul Province',
	'AO-CUS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.9526
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070435,
	'Temburong District,Brunei',
	'2096',
	'BN',
	'District',
	'Temburong District',
	'BN-TE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.9596
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070436,
	'Brunei-Muara District,Brunei',
	'2096',
	'BN',
	'District',
	'Brunei-Muara District',
	'BN-BM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.9656
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070439,
	'Batken Province,Kyrgyzstan',
	'2417',
	'KG',
	'Region',
	'Batken Province',
	'KG-B',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.9776
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070440,
	'Chuy Province,Kyrgyzstan',
	'2417',
	'KG',
	'Region',
	'Chuy Province',
	'KG-C',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.9835
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070441,
	'Naryn Region,Kyrgyzstan',
	'2417',
	'KG',
	'Region',
	'Naryn Region',
	'KG-N',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:11.9925
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070443,
	'Osh Region,Kyrgyzstan',
	'2417',
	'KG',
	'Region',
	'Osh Region',
	'KG-O',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.0105
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070444,
	'Southern District,Botswana',
	'2072',
	'BW',
	'District',
	'Southern District',
	'BW-SO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.0155
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070452,
	'Bamako Capital District,Mali',
	'2466',
	'ML',
	'Region',
	'Bamako Capital District',
	'ML-BKO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.0224
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070453,
	'Kayes Region,Mali',
	'2466',
	'ML',
	'Region',
	'Kayes Region',
	'ML-1',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.0274
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9070455,
	'Guelmim-Es Semara,Western Sahara',
	'2732',
	'EH',
	'Region',
	'Guelmim-Es Semara',
	'MA-14',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.0354
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9073330,
	'Santa Cruz Province,Cajamarca,Peru',
	'20799',
	'PE',
	'Province',
	'Santa Cruz Province',
	'AR-Z',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.0414
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9073358,
	'Songkhla,Songkhla,Thailand',
	'21044',
	'TH',
	'City',
	'Songkhla',
	'TH-90',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.0464
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9073359,
	'Trang,Trang,Thailand',
	'21045',
	'TH',
	'City',
	'Trang',
	'TH-92',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.0534
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9073360,
	'Phuket,Phuket,Thailand',
	'21042',
	'TH',
	'City',
	'Phuket',
	'TH-83',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.0594
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9073362,
	'Surat Thani,Surat Thani,Thailand',
	'9047154',
	'TH',
	'City',
	'Surat Thani',
	'TH-84',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.0653
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9073365,
	'Lampang,Lampang,Thailand',
	'21037',
	'TH',
	'City',
	'Lampang',
	'TH-52',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.0703
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9073366,
	'Chiang Mai,Chiang Mai,Thailand',
	'21036',
	'TH',
	'City',
	'Chiang Mai',
	'TH-50',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.0763
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9073368,
	'Phitsanulok,Phitsanulok,Thailand',
	'21040',
	'TH',
	'City',
	'Phitsanulok',
	'TH-65',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.0823
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9073369,
	'Nakhon Sawan,Nakhon Sawan,Thailand',
	'9047130',
	'TH',
	'City',
	'Nakhon Sawan',
	'TH-60',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.0893
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9073370,
	'Phra Nakhon Si Ayutthaya,Phra Nakhon Si Ayutthaya,Thailand',
	'21028',
	'TH',
	'City',
	'Phra Nakhon Si Ayutthaya',
	'TH-14',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.0953
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9073372,
	'Nonthaburi,Nonthaburi,Thailand',
	'21026',
	'TH',
	'City',
	'Nonthaburi',
	'TH-12',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.1012
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9073374,
	'Samut Sakhon,Samut Sakhon,Thailand',
	'21487',
	'TH',
	'City',
	'Samut Sakhon',
	'TH-74',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.1072
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9073375,
	'Nakhon Pathom,Nakhon Pathom,Thailand',
	'21488',
	'TH',
	'City',
	'Nakhon Pathom',
	'TH-73',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.1132
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9073378,
	'Rayong,Rayong,Thailand',
	'9047143',
	'TH',
	'City',
	'Rayong',
	'TH-21',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.1192
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9073379,
	'Chanthaburi,Chanthaburi,Thailand',
	'9047116',
	'TH',
	'City',
	'Chanthaburi',
	'TH-22',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.1262
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9073380,
	'Ubon Ratchathani,Ubon Ratchathani,Thailand',
	'21033',
	'TH',
	'City',
	'Ubon Ratchathani',
	'TH-34',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.1332
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9073381,
	'Nakhon Ratchasima,Nakhon Ratchasima,Thailand',
	'21032',
	'TH',
	'City',
	'Nakhon Ratchasima',
	'TH-30',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.1403
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9073384,
	'Udon Thani,Udon Thani,Thailand',
	'9047158',
	'TH',
	'City',
	'Udon Thani',
	'TH-41',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.1462
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9073615,
	'Porto Novo,Oueme Department,Benin',
	'9070231',
	'BJ',
	'City',
	'Porto Novo',
	'CV-PN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.1525
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9073688,
	'Sabha,Sabha District,Libya',
	'9070146',
	'LY',
	'City',
	'Sabha',
	'LY-SB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.1594
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9073708,
	'Moka,Moka District,Mauritius',
	'9070414',
	'MU',
	'City',
	'Moka',
	'MU-MO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.1654
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9073723,
	'Sfax,Sfax,Tunisia',
	'9075948',
	'TN',
	'City',
	'Sfax',
	'TN-61',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.1724
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9073724,
	'Sousse,Sousse,Tunisia',
	'9075949',
	'TN',
	'City',
	'Sousse',
	'TN-51',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.1794
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9073725,
	'Dodoma,Dodoma Region,Tanzania',
	'9069885',
	'TZ',
	'City',
	'Dodoma',
	'TZ-03',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.1854
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9073726,
	'Mbeya,Mbeya Region,Tanzania',
	'9069879',
	'TZ',
	'City',
	'Mbeya',
	'TZ-14',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.1914
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9073727,
	'Morogoro,Morogoro Region,Tanzania',
	'9069894',
	'TZ',
	'City',
	'Morogoro',
	'TZ-16',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.1974
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9073770,
	'Oriental,Morocco',
	'2504',
	'MA',
	'Region',
	'Oriental',
	'MA-04',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.2033
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9073791,
	'Zacatecas,Zacatecas,Zacatecas,Mexico',
	'20726',
	'MX',
	'City',
	'Zacatecas',
	'MX-ZAC',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.2093
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9073861,
	'Santa Catarina,Santa Catarina,Nuevo Leon,Mexico',
	'20713',
	'MX',
	'City',
	'Santa Catarina',
	'CV-CA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.2173
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9073894,
	'La Paz,La Paz,State of Mexico,Mexico',
	'20709',
	'MX',
	'City',
	'La Paz',
	'HN-LP',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.2233
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9073973,
	'Puebla,Baja California,Mexico',
	'20696',
	'MX',
	'City',
	'Puebla',
	'MX-PUE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.2293
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9074007,
	'Veracruz,Veracruz,Mexico',
	'20724',
	'MX',
	'City',
	'Veracruz',
	'MX-VER',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.2353
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9074066,
	'Cao Bang,Cao Bang,Vietnam',
	'9047169',
	'VN',
	'City',
	'Cao Bang',
	'VN-04',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.2423
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9074071,
	'Lao Cai,Lao Cai,Vietnam',
	'9047180',
	'VN',
	'City',
	'Lao Cai',
	'VN-02',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.2483
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9074085,
	'Son La,Son La,Vietnam',
	'9047184',
	'VN',
	'City',
	'Son La',
	'VN-05',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.2553
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9074109,
	'Lai Chau,Lai Chau,Vietnam',
	'9047178',
	'VN',
	'City',
	'Lai Chau',
	'VN-01',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.2624
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9074833,
	'Chaiyaphum,Ang Thong,Thailand',
	'9047111',
	'TH',
	'City',
	'Chaiyaphum',
	'TH-36',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.2683
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075379,
	'Canillo,Andorra',
	'2020',
	'AD',
	'Region',
	'Canillo',
	'AD-02',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.2753
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075380,
	'Encamp,Andorra',
	'2020',
	'AD',
	'Region',
	'Encamp',
	'AD-03',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.2803
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075381,
	'La Massana,Andorra',
	'2020',
	'AD',
	'Region',
	'La Massana',
	'AD-04',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.2863
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075382,
	'Ordino,Andorra',
	'2020',
	'AD',
	'Region',
	'Ordino',
	'AD-05',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.2923
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075383,
	'Escaldes-Engordany,Andorra',
	'2020',
	'AD',
	'Region',
	'Escaldes-Engordany',
	'AD-08',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.2983
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075384,
	'Andorra la Vella,Andorra',
	'2020',
	'AD',
	'Region',
	'Andorra la Vella',
	'AD-07',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.3052
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075386,
	'Jowzjan,Afghanistan',
	'2004',
	'AF',
	'Province',
	'Jowzjan',
	'AF-JOW',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.3132
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075387,
	'Paktika,Afghanistan',
	'2004',
	'AF',
	'Province',
	'Paktika',
	'AF-PKA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.3202
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075388,
	'Khost,Afghanistan',
	'2004',
	'AF',
	'Province',
	'Khost',
	'AF-KHO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.3252
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075389,
	'Farah,Afghanistan',
	'2004',
	'AF',
	'Province',
	'Farah',
	'AF-FRA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.3322
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075390,
	'Helmand,Afghanistan',
	'2004',
	'AF',
	'Province',
	'Helmand',
	'AF-HEL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.3393
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075391,
	'Nimruz,Afghanistan',
	'2004',
	'AF',
	'Province',
	'Nimruz',
	'AF-NIM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.3463
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075392,
	'Wardak,Afghanistan',
	'2004',
	'AF',
	'Province',
	'Wardak',
	'AF-WAR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.3533
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075393,
	'Kabul,Afghanistan',
	'2004',
	'AF',
	'Province',
	'Kabul',
	'AF-KAB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.3592
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075395,
	'Ghor,Afghanistan',
	'2004',
	'AF',
	'Province',
	'Ghor',
	'AF-GHO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.3662
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075396,
	'Herat,Afghanistan',
	'2004',
	'AF',
	'Province',
	'Herat',
	'AF-HER',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.3722
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075397,
	'Logar,Afghanistan',
	'2004',
	'AF',
	'Province',
	'Logar',
	'AF-LOG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.3784
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075398,
	'Ghazni,Afghanistan',
	'2004',
	'AF',
	'Province',
	'Ghazni',
	'AF-GHA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.3853
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075399,
	'Balkh,Afghanistan',
	'2004',
	'AF',
	'Province',
	'Balkh',
	'AF-BAL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.3913
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075400,
	'Nangarhar,Afghanistan',
	'2004',
	'AF',
	'Province',
	'Nangarhar',
	'AF-NAN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.3984
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075401,
	'Faryab,Afghanistan',
	'2004',
	'AF',
	'Province',
	'Faryab',
	'AF-FYB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.4064
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075403,
	'Takhar,Afghanistan',
	'2004',
	'AF',
	'Province',
	'Takhar',
	'AF-TAK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.4124
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075404,
	'Kandahar,Afghanistan',
	'2004',
	'AF',
	'Province',
	'Kandahar',
	'AF-KAN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.4194
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075405,
	'Kunduz,Afghanistan',
	'2004',
	'AF',
	'Province',
	'Kunduz',
	'AF-KDZ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.4253
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075407,
	'Samangan,Afghanistan',
	'2004',
	'AF',
	'Province',
	'Samangan',
	'AF-SAM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.4323
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075408,
	'Baghlan,Afghanistan',
	'2004',
	'AF',
	'Province',
	'Baghlan',
	'AF-BGL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.4383
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075409,
	'Badakhshan,Afghanistan',
	'2004',
	'AF',
	'Province',
	'Badakhshan',
	'AF-BDS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.4443
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075410,
	'Paktia,Afghanistan',
	'2004',
	'AF',
	'Province',
	'Paktia',
	'AF-PIA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.4513
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075411,
	'Laghman,Afghanistan',
	'2004',
	'AF',
	'Province',
	'Laghman',
	'AF-LAG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.4573
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075412,
	'Zabul,Afghanistan',
	'2004',
	'AF',
	'Province',
	'Zabul',
	'AF-ZAB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.4642
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075416,
	'Saint Mary,Antigua and Barbuda',
	'2028',
	'AG',
	'Region',
	'Saint Mary',
	'AG-05',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.4712
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075423,
	'Malanje Province,Angola',
	'2024',
	'AO',
	'Province',
	'Malanje Province',
	'AO-MAL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.4772
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075427,
	'Centre-Sud Region,Burkina Faso',
	'2854',
	'BF',
	'Region',
	'Centre-Sud Region',
	'BF-07',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.4832
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075428,
	'Plateau-Central Region,Burkina Faso',
	'2854',
	'BF',
	'Region',
	'Plateau-Central Region',
	'BF-11',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.4902
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075429,
	'Centre-Nord Region,Burkina Faso',
	'2854',
	'BF',
	'Region',
	'Centre-Nord Region',
	'BF-05',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.4972
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075430,
	'Bujumbura Mairie,Burundi',
	'2108',
	'BI',
	'Province',
	'Bujumbura Mairie',
	'BI-BM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.5031
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075435,
	'Sint Eustatius,Caribbean Netherlands',
	'2535',
	'BQ',
	'Municipality',
	'Sint Eustatius',
	'BQ-SE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.5091
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075436,
	'Bonaire,Caribbean Netherlands',
	'2535',
	'BQ',
	'Municipality',
	'Bonaire',
	'BQ-BO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.5161
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075459,
	'Ombella-M''Poko,Central African Republic',
	'2140',
	'CF',
	'Prefecture',
	'Ombella-M''Poko',
	'CF-MP',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.5231
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075460,
	'Bangui,Central African Republic',
	'2140',
	'CF',
	'Municipality',
	'Bangui',
	'CF-BGF',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.5291
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075464,
	'Brazzaville,Republic of the Congo',
	'2178',
	'CG',
	'Municipality',
	'Brazzaville',
	'CG-BZV',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.5361
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075465,
	'Adamawa,Cameroon',
	'2120',
	'CM',
	'Region',
	'Adamawa',
	'NG-AD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.5420
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075466,
	'Heredia Province,Costa Rica',
	'2188',
	'CR',
	'Province',
	'Heredia Province',
	'CR-H',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.5490
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075467,
	'Ribeira Grande,Cape Verde',
	'2132',
	'CV',
	'Municipality',
	'Ribeira Grande',
	'CV-RG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.5550
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075469,
	'Tarrafal,Cape Verde',
	'2132',
	'CV',
	'Municipality',
	'Tarrafal',
	'CV-TA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.5610
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075470,
	'Praia,Cape Verde',
	'2132',
	'CV',
	'Municipality',
	'Praia',
	'CV-PR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.5680
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075472,
	'Ribeira Grande de Santiago,Cape Verde',
	'2132',
	'CV',
	'Municipality',
	'Ribeira Grande de Santiago',
	'CV-RS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.5740
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075475,
	'Sal,Cape Verde',
	'2132',
	'CV',
	'Municipality',
	'Sal',
	'CV-SL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.5799
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075476,
	'Sao Domingos,Cape Verde',
	'2132',
	'CV',
	'Municipality',
	'Sao Domingos',
	'CV-SD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.5859
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075477,
	'Sao Lourenco dos Orgaos,Cape Verde',
	'2132',
	'CV',
	'Municipality',
	'Sao Lourenco dos Orgaos',
	'CV-SO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.5919
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075480,
	'Sao Salvador do Mundo,Cape Verde',
	'2132',
	'CV',
	'Municipality',
	'Sao Salvador do Mundo',
	'CV-SS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.5989
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075481,
	'Sao Filipe,Cape Verde',
	'2132',
	'CV',
	'Municipality',
	'Sao Filipe',
	'CV-SF',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.6059
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075484,
	'Saint George Parish,Dominica',
	'2212',
	'DM',
	'Municipality',
	'Saint George Parish',
	'DM-04',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.6118
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075485,
	'Saint John Parish,Dominica',
	'2212',
	'DM',
	'Municipality',
	'Saint John Parish',
	'DM-05',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.6188
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075486,
	'Saint Paul Parish,Dominica',
	'2212',
	'DM',
	'Municipality',
	'Saint Paul Parish',
	'DM-10',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.6248
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075488,
	'La Altagracia,Dominican Republic',
	'2214',
	'DO',
	'Province',
	'La Altagracia',
	'DO-11',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.6318
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075490,
	'Southern Red Sea,Eritrea',
	'2232',
	'ER',
	'Region',
	'Southern Red Sea',
	'ER-DK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.6378
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075491,
	'Tigray,Ethiopia',
	'2231',
	'ET',
	'Region',
	'Tigray',
	'ET-TI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.6448
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075492,
	'Benishangul-Gumuz,Ethiopia',
	'2231',
	'ET',
	'Region',
	'Benishangul-Gumuz',
	'ET-BE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.6587
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075493,
	'Amhara,Ethiopia',
	'2231',
	'ET',
	'Region',
	'Amhara',
	'ET-AM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.6657
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075494,
	'Southern Nations, Nationalities, and People''s Region,Ethiopia',
	'2231',
	'ET',
	'Region',
	'Southern Nations, Nationalities, and People''s Region',
	'ET-SN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.6717
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075495,
	'Oromia,Ethiopia',
	'2231',
	'ET',
	'Region',
	'Oromia',
	'ET-OR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.6777
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075496,
	'Afar,Ethiopia',
	'2231',
	'ET',
	'Region',
	'Afar',
	'ET-AF',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.6837
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075497,
	'Somali,Ethiopia',
	'2231',
	'ET',
	'Region',
	'Somali',
	'ET-SO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.6896
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075498,
	'Dire Dawa,Ethiopia',
	'2231',
	'ET',
	'Municipality',
	'Dire Dawa',
	'ET-DD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.6966
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075499,
	'Addis Ababa,Ethiopia',
	'2231',
	'ET',
	'Municipality',
	'Addis Ababa',
	'ET-AA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.7046
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075500,
	'Central Ostrobothnia,Finland',
	'2246',
	'FI',
	'Region',
	'Central Ostrobothnia',
	'FI-07',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.7106
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075501,
	'Southwest Finland,Finland',
	'2246',
	'FI',
	'Region',
	'Southwest Finland',
	'FI-19',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.7166
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075502,
	'Satakunta,Finland',
	'2246',
	'FI',
	'Region',
	'Satakunta',
	'FI-17',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.7226
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075503,
	'Tavastia Proper,Finland',
	'2246',
	'FI',
	'Region',
	'Tavastia Proper',
	'FI-06',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.7295
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075504,
	'Ostrobothnia,Finland',
	'2246',
	'FI',
	'Region',
	'Ostrobothnia',
	'FI-12',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.7355
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075505,
	'Pirkanmaa,Finland',
	'2246',
	'FI',
	'Region',
	'Pirkanmaa',
	'FI-11',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.7415
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075506,
	'Uusimaa,Finland',
	'2246',
	'FI',
	'Region',
	'Uusimaa',
	'FI-18',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.7465
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075507,
	'Central Finland,Finland',
	'2246',
	'FI',
	'Region',
	'Central Finland',
	'FI-08',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.7525
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075508,
	'Lapland,Finland',
	'2246',
	'FI',
	'Region',
	'Lapland',
	'FI-10',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.7585
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075509,
	'Northern Savonia,Finland',
	'2246',
	'FI',
	'Region',
	'Northern Savonia',
	'FI-15',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.7645
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075510,
	'North Karelia,Finland',
	'2246',
	'FI',
	'Region',
	'North Karelia',
	'FI-13',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.7705
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075511,
	'South Karelia,Finland',
	'2246',
	'FI',
	'Region',
	'South Karelia',
	'FI-02',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.7766
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075512,
	'Southern Ostrobothnia,Finland',
	'2246',
	'FI',
	'Region',
	'Southern Ostrobothnia',
	'FI-03',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.7836
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075513,
	'Kainuu,Finland',
	'2246',
	'FI',
	'Region',
	'Kainuu',
	'FI-05',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.7886
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075514,
	'Northern Ostrobothnia,Finland',
	'2246',
	'FI',
	'Region',
	'Northern Ostrobothnia',
	'FI-14',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.7956
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075515,
	'Kymenlaakso,Finland',
	'2246',
	'FI',
	'Region',
	'Kymenlaakso',
	'FI-09',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.8016
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075517,
	'Southern Savonia,Finland',
	'2246',
	'FI',
	'Region',
	'Southern Savonia',
	'FI-04',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.8097
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075518,
	'Central Division,Fiji',
	'2242',
	'FJ',
	'Municipality',
	'Central Division',
	'FJ-C',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.8156
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075519,
	'Northern Division,Fiji',
	'2242',
	'FJ',
	'Municipality',
	'Northern Division',
	'FJ-N',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.8216
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075520,
	'Eastern Division,Fiji',
	'2242',
	'FJ',
	'Municipality',
	'Eastern Division',
	'FJ-E',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.8286
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075521,
	'Western Division,Fiji',
	'2242',
	'FJ',
	'Municipality',
	'Western Division',
	'FJ-W',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.8368
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075522,
	'Ogooue-Ivindo,Gabon',
	'2266',
	'GA',
	'Province',
	'Ogooue-Ivindo',
	'GA-6',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.8428
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075523,
	'Ogooue-Maritime,Gabon',
	'2266',
	'GA',
	'Province',
	'Ogooue-Maritime',
	'GA-8',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.8497
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075524,
	'Ngounie,Gabon',
	'2266',
	'GA',
	'Province',
	'Ngounie',
	'GA-4',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.8567
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075525,
	'Haut-Ogooue,Gabon',
	'2266',
	'GA',
	'Province',
	'Haut-Ogooue',
	'GA-2',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.8627
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075526,
	'Nyanga,Gabon',
	'2266',
	'GA',
	'Province',
	'Nyanga',
	'GA-5',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.8697
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075527,
	'Woleu-Ntem,Gabon',
	'2266',
	'GA',
	'Province',
	'Woleu-Ntem',
	'GA-9',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.8768
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075528,
	'Moyen-Ogooue,Gabon',
	'2266',
	'GA',
	'Province',
	'Moyen-Ogooue',
	'GA-3',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.8828
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075529,
	'Ogooue-Lolo,Gabon',
	'2266',
	'GA',
	'Province',
	'Ogooue-Lolo',
	'GA-7',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.8888
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075530,
	'Estuaire,Gabon',
	'2266',
	'GA',
	'Province',
	'Estuaire',
	'GA-1',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.8978
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075531,
	'Carriacou and Petite Martinique,Grenada',
	'2308',
	'GD',
	'Region',
	'Carriacou and Petite Martinique',
	'GD-10',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.9048
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075533,
	'Saint Mark,Grenada',
	'2308',
	'GD',
	'Region',
	'Saint Mark',
	'GD-05',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.9109
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075536,
	'Saint Patrick,Grenada',
	'2308',
	'GD',
	'Region',
	'Saint Patrick',
	'VC-05',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.9179
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075552,
	'Kankan,Guinea',
	'2324',
	'GN',
	'Region',
	'Kankan',
	'GN-K',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.9239
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075553,
	'Litoral,Equatorial Guinea',
	'2226',
	'GQ',
	'Province',
	'Litoral',
	'GQ-LI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.9309
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075554,
	'Bioko Norte,Equatorial Guinea',
	'2226',
	'GQ',
	'Province',
	'Bioko Norte',
	'GQ-BN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.9369
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075561,
	'Bafata,Guinea-Bissau',
	'2624',
	'GW',
	'Region',
	'Bafata',
	'GW-BA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.9438
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075562,
	'Oio,Guinea-Bissau',
	'2624',
	'GW',
	'Region',
	'Oio',
	'GW-OI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.9508
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075563,
	'Bissau,Guinea-Bissau',
	'2624',
	'GW',
	'Region',
	'Bissau',
	'GW-BS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.9578
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075564,
	'Gabu,Guinea-Bissau',
	'2624',
	'GW',
	'Region',
	'Gabu',
	'GW-GA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.9648
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075565,
	'Cacheu,Guinea-Bissau',
	'2624',
	'GW',
	'Region',
	'Cacheu',
	'GW-CA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.9708
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075566,
	'Biombo,Guinea-Bissau',
	'2624',
	'GW',
	'Region',
	'Biombo',
	'GW-BM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.9779
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075567,
	'Cuyuni-Mazaruni,Guyana',
	'2328',
	'GY',
	'Region',
	'Cuyuni-Mazaruni',
	'GY-CU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.9879
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075568,
	'East Berbice-Corentyne,Guyana',
	'2328',
	'GY',
	'Region',
	'East Berbice-Corentyne',
	'GY-EB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:12.9949
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075569,
	'Upper Takutu-Upper Essequibo,Guyana',
	'2328',
	'GY',
	'Region',
	'Upper Takutu-Upper Essequibo',
	'GY-UT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.0018
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075570,
	'Upper Demerara-Berbice,Guyana',
	'2328',
	'GY',
	'Region',
	'Upper Demerara-Berbice',
	'GY-UD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.0088
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075571,
	'Demerara-Mahaica,Guyana',
	'2328',
	'GY',
	'Region',
	'Demerara-Mahaica',
	'GY-DE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.0148
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075572,
	'Essequibo Islands-West Demerara,Guyana',
	'2328',
	'GY',
	'Region',
	'Essequibo Islands-West Demerara',
	'GY-ES',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.0228
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075585,
	'Kandal Province,Cambodia',
	'2116',
	'KH',
	'Province',
	'Kandal Province',
	'KH-8',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.0298
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075586,
	'Kostanay Province,Kazakhstan',
	'2398',
	'KZ',
	'Province',
	'Kostanay Province',
	'KZ-KUS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.0398
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075587,
	'East Kazakhstan Province,Kazakhstan',
	'2398',
	'KZ',
	'Province',
	'East Kazakhstan Province',
	'KZ-VOS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.0468
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075588,
	'South Kazakhstan Province,Kazakhstan',
	'2398',
	'KZ',
	'Province',
	'South Kazakhstan Province',
	'KZ-YUZ',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.0538
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075589,
	'Aktobe Province,Kazakhstan',
	'2398',
	'KZ',
	'Province',
	'Aktobe Province',
	'KZ-AKT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.0598
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075590,
	'Mauren,Liechtenstein',
	'2438',
	'LI',
	'Municipality',
	'Mauren',
	'LI-04',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.0657
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075591,
	'Vaduz,Liechtenstein',
	'2438',
	'LI',
	'Municipality',
	'Vaduz',
	'LI-11',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.0717
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075592,
	'Schellenberg,Liechtenstein',
	'2438',
	'LI',
	'Municipality',
	'Schellenberg',
	'LI-08',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.0777
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075593,
	'Triesenberg,Liechtenstein',
	'2438',
	'LI',
	'Municipality',
	'Triesenberg',
	'LI-10',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.0837
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075594,
	'Montserrado,Liberia',
	'2430',
	'LR',
	'County',
	'Montserrado',
	'LR-MO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.0907
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075600,
	'Aizpute Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Aizpute Municipality',
	'LV-003',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.0957
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075601,
	'Burtnieki Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Burtnieki Municipality',
	'LV-019',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.1028
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075602,
	'Jelgava Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Jelgava Municipality',
	'LV-041',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.1078
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075603,
	'Cesvaine Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Cesvaine Municipality',
	'LV-021',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.1148
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075605,
	'Ape Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Ape Municipality',
	'LV-009',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.1207
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075607,
	'Alsunga Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Alsunga Municipality',
	'LV-006',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.1277
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075608,
	'Priekule Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Priekule Municipality',
	'LV-074',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.1337
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075610,
	'Daugavpils Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Daugavpils Municipality',
	'LV-025',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.1407
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075612,
	'Sala Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Sala Municipality',
	'LV-085',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.1467
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075615,
	'Baldone Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Baldone Municipality',
	'LV-013',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.1517
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075616,
	'Auce Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Auce Municipality',
	'LV-010',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.1587
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075619,
	'Aglona Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Aglona Municipality',
	'LV-001',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.1656
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075621,
	'Ventspils Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Ventspils Municipality',
	'LV-106',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.1726
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075622,
	'Bauska Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Bauska Municipality',
	'LV-016',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.1786
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075623,
	'Gulbene Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Gulbene Municipality',
	'LV-033',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.1856
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075625,
	'Saulkrasti Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Saulkrasti Municipality',
	'LV-089',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.1916
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075626,
	'Aloja Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Aloja Municipality',
	'LV-005',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.1976
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075628,
	'Aizkraukle Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Aizkraukle Municipality',
	'LV-002',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.2035
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075629,
	'Tukums Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Tukums Municipality',
	'LV-099',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.2105
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075632,
	'Sigulda Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Sigulda Municipality',
	'LV-091',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.2155
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075640,
	'Iecava Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Iecava Municipality',
	'LV-034',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.2215
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075641,
	'Talsi Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Talsi Municipality',
	'LV-097',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.2275
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075644,
	'Ludza Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Ludza Municipality',
	'LV-058',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.2335
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075645,
	'Dobele Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Dobele Municipality',
	'LV-026',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.2394
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075646,
	'Ozolnieki Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Ozolnieki Municipality',
	'LV-069',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.2454
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075647,
	'Cibla Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Cibla Municipality',
	'LV-023',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.2514
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075649,
	'Garkalne Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Garkalne Municipality',
	'LV-031',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.2564
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075650,
	'Durbe Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Durbe Municipality',
	'LV-028',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.2624
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075652,
	'Madona Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Madona Municipality',
	'LV-059',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.2684
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075653,
	'Engure Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Engure Municipality',
	'LV-029',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.2744
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075655,
	'Skrunda Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Skrunda Municipality',
	'LV-093',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.2803
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075662,
	'Zilupe Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Zilupe Municipality',
	'LV-110',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.2863
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075664,
	'Rauna Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Rauna Municipality',
	'LV-076',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.2933
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075668,
	'Rucava Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Rucava Municipality',
	'LV-081',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.2993
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075669,
	'Saldus Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Saldus Municipality',
	'LV-088',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.3053
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075670,
	'Valka Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Valka Municipality',
	'LV-101',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.3113
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075671,
	'Amata Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Amata Municipality',
	'LV-008',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.3173
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075673,
	'Vecumnieki Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Vecumnieki Municipality',
	'LV-105',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.3243
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075675,
	'Baltinava Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Baltinava Municipality',
	'LV-014',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.3303
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075676,
	'Nereta Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Nereta Municipality',
	'LV-065',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.3373
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075677,
	'Krimulda Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Krimulda Municipality',
	'LV-048',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.3443
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075679,
	'Balvi Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Balvi Municipality',
	'LV-015',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.3514
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075681,
	'Jaunpils Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Jaunpils Municipality',
	'LV-040',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.3576
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075682,
	'Vecpiebalga Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Vecpiebalga Municipality',
	'LV-104',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.3647
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075683,
	'Koknese Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Koknese Municipality',
	'LV-046',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.3749
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075684,
	'Krustpils Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Krustpils Municipality',
	'LV-049',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.3819
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075685,
	'Dundaga Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Dundaga Municipality',
	'LV-027',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.3879
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075689,
	'Mazsalaca Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Mazsalaca Municipality',
	'LV-060',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.3949
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075690,
	'Smiltene Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Smiltene Municipality',
	'LV-094',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.4049
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075691,
	'Jaunjelgava Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Jaunjelgava Municipality',
	'LV-038',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.4109
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075693,
	'Salaspils Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Salaspils Municipality',
	'LV-087',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.4189
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075698,
	'Kandava Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Kandava Municipality',
	'LV-043',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.4259
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075701,
	'Carnikava Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Carnikava Municipality',
	'LV-020',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.4319
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075702,
	'Roja Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Roja Municipality',
	'LV-079',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.4388
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075704,
	'Jaunpiebalga Municipality,Latvia',
	'2428',
	'LV',
	'Municipality',
	'Jaunpiebalga Municipality',
	'LV-039',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.4468
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075707,
	'Chisinau,Moldova',
	'2498',
	'MD',
	'Municipality',
	'Chisinau',
	'MD-CU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.4528
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075712,
	'Municipality of Karbinci,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Karbinci',
	'MK-37',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.4598
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075713,
	'Municipality of Ilinden,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Ilinden',
	'MK-34',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.4668
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075716,
	'Municipality of Berovo,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Berovo',
	'MK-03',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.4767
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075717,
	'Municipality of Prilep,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Prilep',
	'MK-62',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.4867
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075718,
	'Municipality of Gevgelija,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Gevgelija',
	'MK-18',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.4948
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075719,
	'Municipality of Tearce,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Tearce',
	'MK-75',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.5019
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075720,
	'Municipality of Kavadarci,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Kavadarci',
	'MK-36',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.5090
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075721,
	'Municipality of Zelenikovo,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Zelenikovo',
	'MK-32',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.5161
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075723,
	'Municipality of Makedonski Brod,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Makedonski Brod',
	'MK-52',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.5222
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075724,
	'Municipality of Staro Nagorichane,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Staro Nagorichane',
	'MK-71',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.5292
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075726,
	'Municipality of Bogovinje,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Bogovinje',
	'MK-06',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.5353
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075727,
	'Municipality of Veles,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Veles',
	'MK-13',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.5422
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075729,
	'Municipality of Struga,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Struga',
	'MK-72',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.5482
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075730,
	'Municipality of Negotino,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Negotino',
	'MK-54',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.5572
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075733,
	'Municipality of Rosoman,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Rosoman',
	'MK-67',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.5632
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075735,
	'Municipality of Bitola,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Bitola',
	'MK-04',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.5692
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075736,
	'Municipality of Vinica,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Vinica',
	'MK-14',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.5763
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075737,
	'Municipality of Kumanovo,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Kumanovo',
	'MK-47',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.5824
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075738,
	'Municipality of Jegunovce,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Jegunovce',
	'MK-35',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.5894
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075739,
	'Municipality of Resen,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Resen',
	'MK-66',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.5954
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075740,
	'Municipality of Debar,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Debar',
	'MK-21',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.6024
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075742,
	'Municipality of Lipkovo,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Lipkovo',
	'MK-48',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.6084
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075743,
	'Municipality of Bogdanci,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Bogdanci',
	'MK-05',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.6154
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075744,
	'Municipality of Debarca,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Debarca',
	'MK-22',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.6224
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075745,
	'Municipality of Kratovo,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Kratovo',
	'MK-43',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.6283
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075746,
	'Municipality of Dolneni,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Dolneni',
	'MK-27',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.6353
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075747,
	'Municipality of Demir Hisar,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Demir Hisar',
	'MK-25',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.6413
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075748,
	'Municipality of Mogila,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Mogila',
	'MK-53',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.6483
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075749,
	'Municipality of Sveti Nikole,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Sveti Nikole',
	'MK-69',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.6543
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075750,
	'Municipality of Lozovo,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Lozovo',
	'MK-49',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.6603
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075751,
	'Municipality of Novo Selo,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Novo Selo',
	'MK-56',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.6672
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075752,
	'Municipality of Dojran,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Dojran',
	'MK-26',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.6732
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075753,
	'Municipality of Strumitsa,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Strumitsa',
	'MK-73',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.6792
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075760,
	'Municipality of Bosilovo,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Bosilovo',
	'MK-07',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.6862
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075761,
	'Municipality of Kriva Palanka,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Kriva Palanka',
	'MK-44',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.6932
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075766,
	'Municipality of Tetovo,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Tetovo',
	'MK-76',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.6992
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075768,
	'Municipality of Gostivar,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Gostivar',
	'MK-19',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.7051
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075769,
	'Municipality of Vasilevo,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Vasilevo',
	'MK-11',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.7111
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075771,
	'Municipality of Valandovo,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Valandovo',
	'MK-10',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.7171
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075772,
	'Municipality of Demir Kapija,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Demir Kapija',
	'MK-24',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.7231
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075775,
	'Municipality of Ohrid,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Ohrid',
	'MK-58',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.7301
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075776,
	'Municipality of Gradsko,Macedonia (FYROM)',
	'2807',
	'MK',
	'Municipality',
	'Municipality of Gradsko',
	'MK-20',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.7361
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075777,
	'Chin State,Myanmar (Burma)',
	'2104',
	'MM',
	'State',
	'Chin State',
	'MM-14',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.7420
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075778,
	'Tov,Mongolia',
	'2496',
	'MN',
	'Province',
	'Tov',
	'MN-047',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.7480
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075779,
	'Khentii,Mongolia',
	'2496',
	'MN',
	'Province',
	'Khentii',
	'MN-039',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.7540
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075780,
	'Govisumber,Mongolia',
	'2496',
	'MN',
	'Province',
	'Govisumber',
	'MN-064',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.7630
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075782,
	'Arkhangai,Mongolia',
	'2496',
	'MN',
	'Province',
	'Arkhangai',
	'MN-073',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.7700
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075783,
	'Dornogovi,Mongolia',
	'2496',
	'MN',
	'Province',
	'Dornogovi',
	'MN-063',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.7769
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075784,
	'Govi-Altai,Mongolia',
	'2496',
	'MN',
	'Province',
	'Govi-Altai',
	'MN-065',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.7829
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075785,
	'Dornod,Mongolia',
	'2496',
	'MN',
	'Province',
	'Dornod',
	'MN-061',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.7909
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075786,
	'Darkhan-Uul,Mongolia',
	'2496',
	'MN',
	'Province',
	'Darkhan-Uul',
	'MN-037',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.7969
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075787,
	'Dundgovi,Mongolia',
	'2496',
	'MN',
	'Province',
	'Dundgovi',
	'MN-059',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.8029
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075789,
	'Orkhon,Mongolia',
	'2496',
	'MN',
	'Province',
	'Orkhon',
	'MN-035',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.8089
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075790,
	'Khovd,Mongolia',
	'2496',
	'MN',
	'Province',
	'Khovd',
	'MN-043',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.8158
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075791,
	'Sukhbaatar,Mongolia',
	'2496',
	'MN',
	'Province',
	'Sukhbaatar',
	'MN-051',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.8218
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075793,
	'Zavkhan,Mongolia',
	'2496',
	'MN',
	'Province',
	'Zavkhan',
	'MN-057',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.8278
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075795,
	'Bayankhongor,Mongolia',
	'2496',
	'MN',
	'Province',
	'Bayankhongor',
	'MN-069',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.8348
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075796,
	'Selenge,Mongolia',
	'2496',
	'MN',
	'Province',
	'Selenge',
	'MN-049',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.8398
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075797,
	'Uvs,Mongolia',
	'2496',
	'MN',
	'Province',
	'Uvs',
	'MN-046',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.8468
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075798,
	'Bulgan,Mongolia',
	'2496',
	'MN',
	'Province',
	'Bulgan',
	'MN-067',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.8527
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075804,
	'Hodh Ech Chargui,Mauritania',
	'2478',
	'MR',
	'Region',
	'Hodh Ech Chargui',
	'MR-01',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.8587
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075805,
	'Adrar,Mauritania',
	'2478',
	'MR',
	'Region',
	'Adrar',
	'MR-07',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.8647
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075806,
	'Gorgol,Mauritania',
	'2478',
	'MR',
	'Region',
	'Gorgol',
	'MR-04',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.8707
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075807,
	'Trarza,Mauritania',
	'2478',
	'MR',
	'Region',
	'Trarza',
	'MR-06',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.8767
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075808,
	'Guidimaka,Mauritania',
	'2478',
	'MR',
	'Region',
	'Guidimaka',
	'MR-10',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.8827
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075809,
	'Dakhlet Nouadhibou,Mauritania',
	'2478',
	'MR',
	'Region',
	'Dakhlet Nouadhibou',
	'MR-08',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.8887
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075810,
	'Inchiri,Mauritania',
	'2478',
	'MR',
	'Region',
	'Inchiri',
	'MR-12',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.8946
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075811,
	'Male,Maldives',
	'2462',
	'MV',
	'City',
	'Male',
	'MV-MLE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.9006
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075815,
	'Gaza Province,Mozambique',
	'2508',
	'MZ',
	'Province',
	'Gaza Province',
	'MZ-G',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.9066
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075816,
	'Zambezia Province,Mozambique',
	'2508',
	'MZ',
	'Province',
	'Zambezia Province',
	'MZ-Q',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.9126
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075819,
	'Morobe Province,Papua New Guinea',
	'2598',
	'PG',
	'Province',
	'Morobe Province',
	'PG-MPL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.9186
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075820,
	'East New Britain Province,Papua New Guinea',
	'2598',
	'PG',
	'Province',
	'East New Britain Province',
	'PG-EBR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.9236
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075823,
	'Western Highlands Province,Papua New Guinea',
	'2598',
	'PG',
	'Province',
	'Western Highlands Province',
	'PG-WHM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.9296
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075824,
	'West New Britain Province,Papua New Guinea',
	'2598',
	'PG',
	'Province',
	'West New Britain Province',
	'PG-WBK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.9356
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075825,
	'National Capital District,Papua New Guinea',
	'2598',
	'PG',
	'Province',
	'National Capital District',
	'PG-NCD',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.9425
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075826,
	'Autonomous Region of Bougainville,Papua New Guinea',
	'2598',
	'PG',
	'Province',
	'Autonomous Region of Bougainville',
	'PG-NSB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.9485
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075827,
	'Eastern Highlands Province,Papua New Guinea',
	'2598',
	'PG',
	'Province',
	'Eastern Highlands Province',
	'PG-EHG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.9545
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075828,
	'East Sepik Province,Papua New Guinea',
	'2598',
	'PG',
	'Province',
	'East Sepik Province',
	'PG-ESW',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.9615
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075829,
	'Enga Province,Papua New Guinea',
	'2598',
	'PG',
	'Province',
	'Enga Province',
	'PG-EPW',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.9675
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075831,
	'Milne Bay Province,Papua New Guinea',
	'2598',
	'PG',
	'Province',
	'Milne Bay Province',
	'PG-MBA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.9755
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075832,
	'Chimbu Province,Papua New Guinea',
	'2598',
	'PG',
	'Province',
	'Chimbu Province',
	'PG-CPK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.9814
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075834,
	'Madang Province,Papua New Guinea',
	'2598',
	'PG',
	'Province',
	'Madang Province',
	'PG-MPM',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.9874
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075835,
	'New Ireland Province,Papua New Guinea',
	'2598',
	'PG',
	'Province',
	'New Ireland Province',
	'PG-NIK',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.9935
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075895,
	'Western Area,Sierra Leone',
	'2694',
	'SL',
	'Region',
	'Western Area',
	'SL-W',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:13.9995
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075898,
	'Domagnano,San Marino',
	'2674',
	'SM',
	'Municipality',
	'Domagnano',
	'SM-03',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.0056
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075899,
	'Fiorentino,San Marino',
	'2674',
	'SM',
	'Municipality',
	'Fiorentino',
	'SM-05',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.0117
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075900,
	'Montegiardino,San Marino',
	'2674',
	'SM',
	'Municipality',
	'Montegiardino',
	'SM-08',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.0197
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075901,
	'Faetano,San Marino',
	'2674',
	'SM',
	'Municipality',
	'Faetano',
	'SM-04',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.0257
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075902,
	'Acquaviva,San Marino',
	'2674',
	'SM',
	'Municipality',
	'Acquaviva',
	'SM-01',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.0316
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075904,
	'Borgo Maggiore,San Marino',
	'2674',
	'SM',
	'Municipality',
	'Borgo Maggiore',
	'SM-06',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.0376
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075905,
	'Serravalle,San Marino',
	'2674',
	'SM',
	'Municipality',
	'Serravalle',
	'SM-09',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.0437
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075906,
	'Chiesanuova,San Marino',
	'2674',
	'SM',
	'Municipality',
	'Chiesanuova',
	'SM-02',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.0497
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075907,
	'Shabeellaha Hoose,Somalia',
	'2706',
	'SO',
	'Region',
	'Shabeellaha Hoose',
	'SO-SH',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.0577
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075908,
	'Banaadir,Somalia',
	'2706',
	'SO',
	'Region',
	'Banaadir',
	'SO-BN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.0647
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075909,
	'Sanaag,Somalia',
	'2706',
	'SO',
	'Region',
	'Sanaag',
	'SO-SA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.0707
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075910,
	'Woqooyi Galbeed,Somalia',
	'2706',
	'SO',
	'Region',
	'Woqooyi Galbeed',
	'SO-WO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.0776
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075911,
	'Sool,Somalia',
	'2706',
	'SO',
	'Region',
	'Sool',
	'SO-SO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.0836
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075912,
	'Mudug,Somalia',
	'2706',
	'SO',
	'Region',
	'Mudug',
	'SO-MU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.0906
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075915,
	'Togdheer,Somalia',
	'2706',
	'SO',
	'Region',
	'Togdheer',
	'SO-TO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.0967
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075916,
	'Awdal,Somalia',
	'2706',
	'SO',
	'Region',
	'Awdal',
	'SO-AW',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.1037
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075917,
	'Gedo,Somalia',
	'2706',
	'SO',
	'Region',
	'Gedo',
	'SO-GE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.1098
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075925,
	'Chari-Baguirmi,Chad',
	'2148',
	'TD',
	'Region',
	'Chari-Baguirmi',
	'TD-CB',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.1158
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075926,
	'Hadjer-Lamis Region,Chad',
	'2148',
	'TD',
	'Region',
	'Hadjer-Lamis Region',
	'TD-HL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.1217
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075927,
	'Borkou,Chad',
	'2148',
	'TD',
	'Region',
	'Borkou',
	'TD-BO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.1277
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075930,
	'Ahal,Turkmenistan',
	'2795',
	'TM',
	'Region',
	'Ahal',
	'TM-A',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.1347
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075931,
	'Balkan,Turkmenistan',
	'2795',
	'TM',
	'Region',
	'Balkan',
	'TM-B',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.1417
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075933,
	'Mary,Turkmenistan',
	'2795',
	'TM',
	'Region',
	'Mary',
	'TM-M',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.1487
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075934,
	'Lebap,Turkmenistan',
	'2795',
	'TM',
	'Region',
	'Lebap',
	'TM-L',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.1547
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075935,
	'Ariana,Tunisia',
	'2788',
	'TN',
	'Governorate',
	'Ariana',
	'TN-12',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.1607
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075937,
	'Medenine,Tunisia',
	'2788',
	'TN',
	'Governorate',
	'Medenine',
	'TN-82',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.1678
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075938,
	'Gafsa,Tunisia',
	'2788',
	'TN',
	'Governorate',
	'Gafsa',
	'TN-71',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.1738
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075939,
	'Mahdia,Tunisia',
	'2788',
	'TN',
	'Governorate',
	'Mahdia',
	'TN-53',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.1808
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075941,
	'Nabeul,Tunisia',
	'2788',
	'TN',
	'Governorate',
	'Nabeul',
	'TN-21',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.1878
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075942,
	'Ben Arous,Tunisia',
	'2788',
	'TN',
	'Governorate',
	'Ben Arous',
	'TN-13',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.1957
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075943,
	'Monastir,Tunisia',
	'2788',
	'TN',
	'Governorate',
	'Monastir',
	'TN-52',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.2027
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075944,
	'Kef,Tunisia',
	'2788',
	'TN',
	'Governorate',
	'Kef',
	'TN-33',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.2097
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075950,
	'Jendouba,Tunisia',
	'2788',
	'TN',
	'Governorate',
	'Jendouba',
	'TN-32',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.2157
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075952,
	'San Juan-Laventille Regional Corporation,Trinidad and Tobago',
	'2780',
	'TT',
	'Region',
	'San Juan-Laventille Regional Corporation',
	'TT-SJL',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.2218
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075954,
	'Couva-Tabaquite-Talparo Regional Corporation,Trinidad and Tobago',
	'2780',
	'TT',
	'Region',
	'Couva-Tabaquite-Talparo Regional Corporation',
	'TT-CTT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.2287
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075955,
	'Eastern Tobago,Trinidad and Tobago',
	'2780',
	'TT',
	'Region',
	'Eastern Tobago',
	'TT-ETO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.2347
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075956,
	'Diego Martin Regional Corporation,Trinidad and Tobago',
	'2780',
	'TT',
	'Region',
	'Diego Martin Regional Corporation',
	'TT-DMN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.2418
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075958,
	'Sangre Grande Regional Corporation,Trinidad and Tobago',
	'2780',
	'TT',
	'Region',
	'Sangre Grande Regional Corporation',
	'TT-SGE',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.2477
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075959,
	'Western Tobago,Trinidad and Tobago',
	'2780',
	'TT',
	'Region',
	'Western Tobago',
	'TT-WTO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.2548
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075960,
	'Princes Town Regional Corporation,Trinidad and Tobago',
	'2780',
	'TT',
	'Region',
	'Princes Town Regional Corporation',
	'TT-PRT',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.2608
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075961,
	'Siparia Regional Corporation,Trinidad and Tobago',
	'2780',
	'TT',
	'Region',
	'Siparia Regional Corporation',
	'TT-SIP',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.2678
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075962,
	'Point Fortin Borough Corporation,Trinidad and Tobago',
	'2780',
	'TT',
	'Municipality',
	'Point Fortin Borough Corporation',
	'TT-PTF',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.2738
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075963,
	'Port of Spain Corporation,Trinidad and Tobago',
	'2780',
	'TT',
	'Municipality',
	'Port of Spain Corporation',
	'TT-POS',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.2798
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075965,
	'Arima Borough Corporation,Trinidad and Tobago',
	'2780',
	'TT',
	'Municipality',
	'Arima Borough Corporation',
	'TT-ARI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.2858
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075966,
	'Chaguanas Borough Corporation,Trinidad and Tobago',
	'2780',
	'TT',
	'Municipality',
	'Chaguanas Borough Corporation',
	'TT-CHA',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.2928
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075970,
	'Bukhara Region,Uzbekistan',
	'2860',
	'UZ',
	'Region',
	'Bukhara Region',
	'UZ-BU',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.2987
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075971,
	'Jizzakh Region,Uzbekistan',
	'2860',
	'UZ',
	'Region',
	'Jizzakh Region',
	'UZ-JI',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.3047
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075972,
	'Andijan region,Uzbekistan',
	'2860',
	'UZ',
	'Region',
	'Andijan region',
	'UZ-AN',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.3107
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075975,
	'Republic of Karakalpakstan,Uzbekistan',
	'2860',
	'UZ',
	'Region',
	'Republic of Karakalpakstan',
	'UZ-QR',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.3177
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075977,
	'Namangan Region,Uzbekistan',
	'2860',
	'UZ',
	'Region',
	'Namangan Region',
	'UZ-NG',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.3247
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075978,
	'Navoiy Region,Uzbekistan',
	'2860',
	'UZ',
	'Region',
	'Navoiy Region',
	'UZ-NW',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.3308
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075981,
	'Tashkent Region,Uzbekistan',
	'2860',
	'UZ',
	'Region',
	'Tashkent Region',
	'UZ-TO',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.3368
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075983,
	'Grenadines,Saint Vincent and the Grenadines',
	'2670',
	'VC',
	'Region',
	'Grenadines',
	'VC-06',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.3428
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075988,
	'Vargas,Venezuela',
	'2862',
	'VE',
	'State',
	'Vargas',
	'VE-X',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.3487
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075995,
	'Eastern Province,Zambia',
	'2894',
	'ZM',
	'Province',
	'Eastern Province',
	'ZM-03',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.3577
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9075996,
	'Northern Province,Zambia',
	'2894',
	'ZM',
	'Province',
	'Northern Province',
	'ZM-05',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.3647
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9076147,
	'Kairouan,Kairouan,Tunisia',
	'9075936',
	'TN',
	'City',
	'Kairouan',
	'TN-41',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- 2020-04-11 13:23:14.3717
INSERT INTO [t_ResultItem]([location_code],[location_name],[location_code_parent],[country_iso_code],[location_type],[geo_name],[geo_id],[geo_name_CN],[ObjectDBID]) VALUES (
	9076295,
	'Bizerte,Bizerte,Tunisia',
	'9075945',
	'TN',
	'City',
	'Bizerte',
	'TN-23',
	Null,
	Null
); SELECT last_insert_rowid() AS InsertID;

-- ====2020-04-11 15:18:33.3136====

-- 2020-04-11 15:18:33.3136
SELECT * FROM [t_Config_DB];

-- 2020-04-11 15:18:33.3156
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-11 15:21:07.5247====

-- 2020-04-11 15:21:07.5247
SELECT * FROM [t_Config_DB];

-- 2020-04-11 15:21:07.5247
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-11 15:23:34.5406====

-- 2020-04-11 15:23:34.5406
SELECT * FROM [t_Config_DB];

-- 2020-04-11 15:23:34.5416
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-11 15:49:32.6247====

-- 2020-04-11 15:49:32.6247
SELECT * FROM [t_Config_DB];

-- 2020-04-11 15:49:32.6257
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-11 16:08:41.7911====

-- 2020-04-11 16:08:41.7911
SELECT * FROM [t_Config_DB];

-- 2020-04-11 16:08:41.7921
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-13 18:27:56.2749====

-- 2020-04-13 18:27:56.2749
SELECT * FROM [t_Config_DB];

-- 2020-04-13 18:27:56.2759
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-13 20:09:46.7119====

-- 2020-04-13 20:09:46.7119
SELECT * FROM [t_Config_DB];

-- 2020-04-13 20:09:46.7129
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-13 20:11:31.7854====

-- 2020-04-13 20:11:31.7854
SELECT * FROM [t_Config_DB];

-- 2020-04-13 20:11:31.7854
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-14 09:27:33.2271====

-- 2020-04-14 09:27:33.2271
SELECT * FROM [t_Config_DB];

-- 2020-04-14 09:27:33.2271
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-14 10:08:28.0001====

-- 2020-04-14 10:08:28.0001
SELECT * FROM [t_Config_DB];

-- 2020-04-14 10:08:28.0001
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-14 10:18:42.3249====

-- 2020-04-14 10:18:42.3249
SELECT * FROM [t_Config_DB];

-- 2020-04-14 10:18:42.3259
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-14 14:51:21.1968====

-- 2020-04-14 14:51:21.1968
SELECT * FROM [t_Config_DB];

-- 2020-04-14 14:51:21.1968
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-14 15:16:51.1354====

-- 2020-04-14 15:16:51.1354
SELECT * FROM [t_Config_DB];

-- 2020-04-14 15:16:51.1354
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-14 15:18:16.7793====

-- 2020-04-14 15:18:16.7793
SELECT * FROM [t_Config_DB];

-- 2020-04-14 15:18:16.7793
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-14 15:21:07.2593====

-- 2020-04-14 15:21:07.2593
SELECT * FROM [t_Config_DB];

-- 2020-04-14 15:21:07.2593
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-14 15:34:19.6585====

-- 2020-04-14 15:34:19.6585
SELECT * FROM [t_Config_DB];

-- 2020-04-14 15:34:19.6585
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-14 15:36:33.8757====

-- 2020-04-14 15:36:33.8757
SELECT * FROM [t_Config_DB];

-- 2020-04-14 15:36:33.8757
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-14 15:38:53.6665====

-- 2020-04-14 15:38:53.6665
SELECT * FROM [t_Config_DB];

-- 2020-04-14 15:38:53.6665
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-14 15:49:13.6153====

-- 2020-04-14 15:49:13.6153
SELECT * FROM [t_Config_DB];

-- 2020-04-14 15:49:13.6153
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-14 16:34:53.3299====

-- 2020-04-14 16:34:53.3299
SELECT * FROM [t_Config_DB];

-- 2020-04-14 16:34:53.3299
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-14 16:51:13.7525====

-- 2020-04-14 16:51:13.7525
SELECT * FROM [t_Config_DB];

-- 2020-04-14 16:51:13.7525
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-14 16:54:23.1936====

-- 2020-04-14 16:54:23.1936
SELECT * FROM [t_Config_DB];

-- 2020-04-14 16:54:23.1936
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-16 10:14:43.8500====

-- 2020-04-16 10:14:43.8500
SELECT * FROM [t_Config_DB];

-- 2020-04-16 10:14:43.8500
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-16 10:17:32.6745====

-- 2020-04-16 10:17:32.6745
SELECT * FROM [t_Config_DB];

-- 2020-04-16 10:17:32.6745
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-16 10:19:58.9442====

-- 2020-04-16 10:19:58.9442
SELECT * FROM [t_Config_DB];

-- 2020-04-16 10:19:58.9442
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-16 10:24:05.4115====

-- 2020-04-16 10:24:05.4115
SELECT * FROM [t_Config_DB];

-- 2020-04-16 10:24:05.4115
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-16 10:25:31.8567====

-- 2020-04-16 10:25:31.8567
SELECT * FROM [t_Config_DB];

-- 2020-04-16 10:25:31.8567
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-16 10:28:00.5564====

-- 2020-04-16 10:28:00.5564
SELECT * FROM [t_Config_DB];

-- 2020-04-16 10:28:00.5564
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-16 13:04:41.4467====

-- 2020-04-16 13:04:41.4467
SELECT * FROM [t_Config_DB];

-- 2020-04-16 13:04:41.4477
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-16 13:32:04.8040====

-- 2020-04-16 13:32:04.8040
SELECT * FROM [t_Config_DB];

-- 2020-04-16 13:32:04.8040
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-16 13:38:00.4490====

-- 2020-04-16 13:38:00.4490
SELECT * FROM [t_Config_DB];

-- 2020-04-16 13:38:00.4490
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-16 13:46:53.9583====

-- 2020-04-16 13:46:53.9583
SELECT * FROM [t_Config_DB];

-- 2020-04-16 13:46:53.9593
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-16 13:50:40.8286====

-- 2020-04-16 13:50:40.8286
SELECT * FROM [t_Config_DB];

-- 2020-04-16 13:50:40.8286
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-16 13:58:12.7431====

-- 2020-04-16 13:58:12.7431
SELECT * FROM [t_Config_DB];

-- 2020-04-16 13:58:12.7431
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-16 14:03:30.7589====

-- 2020-04-16 14:03:30.7589
SELECT * FROM [t_Config_DB];

-- 2020-04-16 14:03:30.7599
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-16 14:07:53.6759====

-- 2020-04-16 14:07:53.6759
SELECT * FROM [t_Config_DB];

-- 2020-04-16 14:07:53.6759
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-16 14:10:44.6465====

-- 2020-04-16 14:10:44.6465
SELECT * FROM [t_Config_DB];

-- 2020-04-16 14:10:44.6465
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-16 14:26:31.4091====

-- 2020-04-16 14:26:31.4091
SELECT * FROM [t_Config_DB];

-- 2020-04-16 14:26:31.4091
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-16 14:30:07.5139====

-- 2020-04-16 14:30:07.5139
SELECT * FROM [t_Config_DB];

-- 2020-04-16 14:30:07.5139
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-16 14:42:22.1940====

-- 2020-04-16 14:42:22.1940
SELECT * FROM [t_Config_DB];

-- 2020-04-16 14:42:22.1940
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-16 15:03:08.5098====

-- 2020-04-16 15:03:08.5098
SELECT * FROM [t_Config_DB];

-- 2020-04-16 15:03:08.5098
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-16 15:10:37.4251====

-- 2020-04-16 15:10:37.4251
SELECT * FROM [t_Config_DB];

-- 2020-04-16 15:10:37.4251
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-16 15:12:03.7633====

-- 2020-04-16 15:12:03.7633
SELECT * FROM [t_Config_DB];

-- 2020-04-16 15:12:03.7633
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-16 15:16:18.5347====

-- 2020-04-16 15:16:18.5347
SELECT * FROM [t_Config_DB];

-- 2020-04-16 15:16:18.5347
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-16 15:18:45.3776====

-- 2020-04-16 15:18:45.3776
SELECT * FROM [t_Config_DB];

-- 2020-04-16 15:18:45.3776
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-16 15:23:15.6363====

-- 2020-04-16 15:23:15.6363
SELECT * FROM [t_Config_DB];

-- 2020-04-16 15:23:15.6363
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-16 15:27:52.5376====

-- 2020-04-16 15:27:52.5376
SELECT * FROM [t_Config_DB];

-- 2020-04-16 15:27:52.5376
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-16 15:30:15.7193====

-- 2020-04-16 15:30:15.7193
SELECT * FROM [t_Config_DB];

-- 2020-04-16 15:30:15.7193
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-16 15:33:03.9388====

-- 2020-04-16 15:33:03.9388
SELECT * FROM [t_Config_DB];

-- 2020-04-16 15:33:03.9398
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-04-17 16:33:42.7106====

-- 2020-04-17 16:33:42.7106
SELECT * FROM [t_Config_DB];

-- 2020-04-17 16:33:42.7116
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-07 09:41:25.8760====

-- 2020-05-07 09:41:25.8760
SELECT * FROM [t_Config_DB];

-- 2020-05-07 09:41:25.8760
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-13 17:24:16.9996====

-- 2020-05-13 17:24:16.9996
SELECT * FROM [t_Config_DB];

-- 2020-05-13 17:24:17.0006
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-13 17:25:17.9960====

-- 2020-05-13 17:25:17.9960
SELECT * FROM [t_Config_DB];

-- 2020-05-13 17:25:17.9960
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-13 17:27:06.9150====

-- 2020-05-13 17:27:06.9150
SELECT * FROM [t_Config_DB];

-- 2020-05-13 17:27:06.9150
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-13 17:28:29.2655====

-- 2020-05-13 17:28:29.2655
SELECT * FROM [t_Config_DB];

-- 2020-05-13 17:28:29.2665
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-18 16:47:01.0784====

-- 2020-05-18 16:47:01.0784
SELECT * FROM [t_Config_DB];

-- 2020-05-18 16:47:01.0804
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-18 16:53:24.6115====

-- 2020-05-18 16:53:24.6115
SELECT * FROM [t_Config_DB];

-- 2020-05-18 16:53:24.6125
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-18 17:00:01.1918====

-- 2020-05-18 17:00:01.1918
SELECT * FROM [t_Config_DB];

-- 2020-05-18 17:00:01.1918
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-18 17:04:13.4568====

-- 2020-05-18 17:04:13.4568
SELECT * FROM [t_Config_DB];

-- 2020-05-18 17:04:13.4578
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-18 17:44:14.1507====

-- 2020-05-18 17:44:14.1507
SELECT * FROM [t_Config_DB];

-- 2020-05-18 17:44:14.1517
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-19 17:40:03.0891====

-- 2020-05-19 17:40:03.0891
SELECT * FROM [t_Config_DB];

-- 2020-05-19 17:40:03.0900
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-19 17:52:58.7232====

-- 2020-05-19 17:52:58.7232
SELECT * FROM [t_Config_DB];

-- 2020-05-19 17:52:58.7232
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-20 09:29:04.0562====

-- 2020-05-20 09:29:04.0562
SELECT * FROM [t_Config_DB];

-- 2020-05-20 09:29:04.0562
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-20 09:31:21.4667====

-- 2020-05-20 09:31:21.4667
SELECT * FROM [t_Config_DB];

-- 2020-05-20 09:31:21.4667
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-20 11:03:06.9123====

-- 2020-05-20 11:03:06.9123
SELECT * FROM [t_Config_DB];

-- 2020-05-20 11:03:06.9123
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-20 11:09:23.7779====

-- 2020-05-20 11:09:23.7779
SELECT * FROM [t_Config_DB];

-- 2020-05-20 11:09:23.7789
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-20 11:19:25.3525====

-- 2020-05-20 11:19:25.3525
SELECT * FROM [t_Config_DB];

-- 2020-05-20 11:19:25.3525
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-20 15:46:43.9650====

-- 2020-05-20 15:46:43.9650
SELECT * FROM [t_Config_DB];

-- 2020-05-20 15:46:43.9660
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-20 15:47:39.5489====

-- 2020-05-20 15:47:39.5489
SELECT * FROM [t_Config_DB];

-- 2020-05-20 15:47:39.5489
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-20 15:58:48.8547====

-- 2020-05-20 15:58:48.8547
SELECT * FROM [t_Config_DB];

-- 2020-05-20 15:58:48.8557
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-20 16:28:01.6037====

-- 2020-05-20 16:28:01.6037
SELECT * FROM [t_Config_DB];

-- 2020-05-20 16:28:01.6047
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-20 16:46:54.6928====

-- 2020-05-20 16:46:54.6928
SELECT * FROM [t_Config_DB];

-- 2020-05-20 16:46:54.6928
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-20 16:49:55.5856====

-- 2020-05-20 16:49:55.5856
SELECT * FROM [t_Config_DB];

-- 2020-05-20 16:49:55.5856
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-20 17:01:03.7545====

-- 2020-05-20 17:01:03.7545
SELECT * FROM [t_Config_DB];

-- 2020-05-20 17:01:03.7555
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-20 17:55:20.9764====

-- 2020-05-20 17:55:20.9764
SELECT * FROM [t_Config_DB];

-- 2020-05-20 17:55:20.9764
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-20 17:59:02.6612====

-- 2020-05-20 17:59:02.6612
SELECT * FROM [t_Config_DB];

-- 2020-05-20 17:59:02.6622
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-20 18:02:42.5288====

-- 2020-05-20 18:02:42.5288
SELECT * FROM [t_Config_DB];

-- 2020-05-20 18:02:42.5288
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-20 18:04:53.6704====

-- 2020-05-20 18:04:53.6704
SELECT * FROM [t_Config_DB];

-- 2020-05-20 18:04:53.6704
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-21 14:51:38.8818====

-- 2020-05-21 14:51:38.8818
SELECT * FROM [t_Config_DB];

-- 2020-05-21 14:51:38.8818
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-21 15:58:55.2221====

-- 2020-05-21 15:58:55.2221
SELECT * FROM [t_Config_DB];

-- 2020-05-21 15:58:55.2230
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-21 16:00:37.8008====

-- 2020-05-21 16:00:37.8008
SELECT * FROM [t_Config_DB];

-- 2020-05-21 16:00:37.8018
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-22 17:38:45.7126====

-- 2020-05-22 17:38:45.7126
SELECT * FROM [t_Config_DB];

-- 2020-05-22 17:38:45.7136
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-26 17:48:55.0851====

-- 2020-05-26 17:48:55.0851
SELECT * FROM [t_Config_DB];

-- 2020-05-26 17:48:55.0861
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-26 17:49:31.9549====

-- 2020-05-26 17:49:31.9549
SELECT * FROM [t_Config_DB];

-- 2020-05-26 17:49:31.9559
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-26 18:16:31.2092====

-- 2020-05-26 18:16:31.2092
SELECT * FROM [t_Config_DB];

-- 2020-05-26 18:16:31.2092
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-27 15:24:01.6297====

-- 2020-05-27 15:24:01.6297
SELECT * FROM [t_Config_DB];

-- 2020-05-27 15:24:01.6297
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-28 09:49:00.3667====

-- 2020-05-28 09:49:00.3667
SELECT * FROM [t_Config_DB];

-- 2020-05-28 09:49:00.3697
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-28 09:51:43.9247====

-- 2020-05-28 09:51:43.9247
SELECT * FROM [t_Config_DB];

-- 2020-05-28 09:51:43.9256
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-29 14:27:44.9829====

-- 2020-05-29 14:27:44.9829
SELECT * FROM [t_Config_DB];

-- 2020-05-29 14:27:44.9849
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-29 14:28:49.5891====

-- 2020-05-29 14:28:49.5891
SELECT * FROM [t_Config_DB];

-- 2020-05-29 14:28:49.5891
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-29 14:31:57.5713====

-- 2020-05-29 14:31:57.5713
SELECT * FROM [t_Config_DB];

-- 2020-05-29 14:31:57.5713
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-30 09:46:52.0967====

-- 2020-05-30 09:46:52.0967
SELECT * FROM [t_Config_DB];

-- 2020-05-30 09:46:52.0967
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-30 10:00:37.0612====

-- 2020-05-30 10:00:37.0612
SELECT * FROM [t_Config_DB];

-- 2020-05-30 10:00:37.0612
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-30 10:30:18.0729====

-- 2020-05-30 10:30:18.0729
SELECT * FROM [t_Config_DB];

-- 2020-05-30 10:30:18.0729
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-30 10:32:32.8757====

-- 2020-05-30 10:32:32.8757
SELECT * FROM [t_Config_DB];

-- 2020-05-30 10:32:32.8757
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-30 10:45:04.0198====

-- 2020-05-30 10:45:04.0198
SELECT * FROM [t_Config_DB];

-- 2020-05-30 10:45:04.0208
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-30 10:45:47.7116====

-- 2020-05-30 10:45:47.7116
SELECT * FROM [t_Config_DB];

-- 2020-05-30 10:45:47.7126
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-30 10:47:18.3488====

-- 2020-05-30 10:47:18.3488
SELECT * FROM [t_Config_DB];

-- 2020-05-30 10:47:18.3488
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-30 10:48:02.3107====

-- 2020-05-30 10:48:02.3107
SELECT * FROM [t_Config_DB];

-- 2020-05-30 10:48:02.3107
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-30 10:54:13.9033====

-- 2020-05-30 10:54:13.9033
SELECT * FROM [t_Config_DB];

-- 2020-05-30 10:54:13.9033
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-30 10:54:50.1069====

-- 2020-05-30 10:54:50.1069
SELECT * FROM [t_Config_DB];

-- 2020-05-30 10:54:50.1079
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-30 11:23:48.8542====

-- 2020-05-30 11:23:48.8542
SELECT * FROM [t_Config_DB];

-- 2020-05-30 11:23:48.8542
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-30 11:24:14.2913====

-- 2020-05-30 11:24:14.2913
SELECT * FROM [t_Config_DB];

-- 2020-05-30 11:24:14.2913
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-30 11:24:41.1569====

-- 2020-05-30 11:24:41.1569
SELECT * FROM [t_Config_DB];

-- 2020-05-30 11:24:41.1579
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-30 11:32:27.5246====

-- 2020-05-30 11:32:27.5246
SELECT * FROM [t_Config_DB];

-- 2020-05-30 11:32:27.5246
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-30 11:33:22.8409====

-- 2020-05-30 11:33:22.8409
SELECT * FROM [t_Config_DB];

-- 2020-05-30 11:33:22.8419
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-30 11:36:56.0775====

-- 2020-05-30 11:36:56.0775
SELECT * FROM [t_Config_DB];

-- 2020-05-30 11:36:56.0775
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-30 11:38:33.7763====

-- 2020-05-30 11:38:33.7763
SELECT * FROM [t_Config_DB];

-- 2020-05-30 11:38:33.7773
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-30 11:39:16.5660====

-- 2020-05-30 11:39:16.5660
SELECT * FROM [t_Config_DB];

-- 2020-05-30 11:39:16.5660
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-30 11:42:14.0386====

-- 2020-05-30 11:42:14.0386
SELECT * FROM [t_Config_DB];

-- 2020-05-30 11:42:14.0386
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-30 11:47:06.8591====

-- 2020-05-30 11:47:06.8591
SELECT * FROM [t_Config_DB];

-- 2020-05-30 11:47:06.8591
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-30 13:47:06.5461====

-- 2020-05-30 13:47:06.5461
SELECT * FROM [t_Config_DB];

-- 2020-05-30 13:47:06.5471
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-30 13:47:46.3739====

-- 2020-05-30 13:47:46.3739
SELECT * FROM [t_Config_DB];

-- 2020-05-30 13:47:46.3739
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-30 13:48:45.5188====

-- 2020-05-30 13:48:45.5188
SELECT * FROM [t_Config_DB];

-- 2020-05-30 13:48:45.5188
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-30 13:49:24.9051====

-- 2020-05-30 13:49:24.9051
SELECT * FROM [t_Config_DB];

-- 2020-05-30 13:49:24.9061
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-05-30 18:15:11.3955====

-- 2020-05-30 18:15:11.3955
SELECT * FROM [t_Config_DB];

-- 2020-05-30 18:15:11.3955
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-06-01 09:21:59.6078====

-- 2020-06-01 09:21:59.6078
SELECT * FROM [t_Config_DB];

-- 2020-06-01 09:21:59.6088
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-06-01 09:23:20.9776====

-- 2020-06-01 09:23:20.9776
SELECT * FROM [t_Config_DB];

-- 2020-06-01 09:23:20.9786
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-06-12 17:47:41.8662====

-- 2020-06-12 17:47:41.8662
SELECT * FROM [t_Config_DB];

-- 2020-06-12 17:47:41.8662
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-06-12 17:52:26.3706====

-- 2020-06-12 17:52:26.3706
SELECT * FROM [t_Config_DB];

-- 2020-06-12 17:52:26.3706
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-06-12 17:53:58.3866====

-- 2020-06-12 17:53:58.3866
SELECT * FROM [t_Config_DB];

-- 2020-06-12 17:53:58.3876
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-06-12 17:55:55.8187====

-- 2020-06-12 17:55:55.8187
SELECT * FROM [t_Config_DB];

-- 2020-06-12 17:55:55.8187
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-06-12 17:58:12.1847====

-- 2020-06-12 17:58:12.1847
SELECT * FROM [t_Config_DB];

-- 2020-06-12 17:58:12.1847
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-06-12 17:59:27.5147====

-- 2020-06-12 17:59:27.5147
SELECT * FROM [t_Config_DB];

-- 2020-06-12 17:59:27.5147
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-06-12 18:13:50.8792====

-- 2020-06-12 18:13:50.8792
SELECT * FROM [t_Config_DB];

-- 2020-06-12 18:13:50.8802
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-06-16 11:23:24.9461====

-- 2020-06-16 11:23:24.9461
SELECT * FROM [t_Config_DB];

-- 2020-06-16 11:23:24.9481
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-06-16 11:26:15.1901====

-- 2020-06-16 11:26:15.1901
SELECT * FROM [t_Config_DB];

-- 2020-06-16 11:26:15.1901
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-06-22 15:39:17.0045====

-- 2020-06-22 15:39:17.0045
SELECT * FROM [t_Config_DB];

-- 2020-06-22 15:39:17.0055
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-06-22 15:48:44.8573====

-- 2020-06-22 15:48:44.8573
SELECT * FROM [t_Config_DB];

-- 2020-06-22 15:48:44.8583
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-06-22 15:58:51.4416====

-- 2020-06-22 15:58:51.4416
SELECT * FROM [t_Config_DB];

-- 2020-06-22 15:58:51.4416
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-06-22 16:03:49.8018====

-- 2020-06-22 16:03:49.8018
SELECT * FROM [t_Config_DB];

-- 2020-06-22 16:03:49.8018
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-06-22 17:46:06.0215====

-- 2020-06-22 17:46:06.0215
SELECT * FROM [t_Config_DB];

-- 2020-06-22 17:46:06.0215
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-06-22 17:48:01.6551====

-- 2020-06-22 17:48:01.6551
SELECT * FROM [t_Config_DB];

-- 2020-06-22 17:48:01.6551
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-06-22 17:49:04.6307====

-- 2020-06-22 17:49:04.6307
SELECT * FROM [t_Config_DB];

-- 2020-06-22 17:49:04.6307
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-06-22 17:50:59.6334====

-- 2020-06-22 17:50:59.6334
SELECT * FROM [t_Config_DB];

-- 2020-06-22 17:50:59.6334
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-06-22 17:56:03.5339====

-- 2020-06-22 17:56:03.5339
SELECT * FROM [t_Config_DB];

-- 2020-06-22 17:56:03.5349
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-06-22 17:57:11.8320====

-- 2020-06-22 17:57:11.8320
SELECT * FROM [t_Config_DB];

-- 2020-06-22 17:57:11.8320
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-06-22 18:13:58.6393====

-- 2020-06-22 18:13:58.6393
SELECT * FROM [t_Config_DB];

-- 2020-06-22 18:13:58.6393
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-06-22 18:15:57.5035====

-- 2020-06-22 18:15:57.5035
SELECT * FROM [t_Config_DB];

-- 2020-06-22 18:15:57.5035
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-06-23 11:04:58.3069====

-- 2020-06-23 11:04:58.3069
SELECT * FROM [t_Config_DB];

-- 2020-06-23 11:04:58.3069
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-06-23 11:22:50.3935====

-- 2020-06-23 11:22:50.3935
SELECT * FROM [t_Config_DB];

-- 2020-06-23 11:22:50.3935
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-06-23 11:25:51.9794====

-- 2020-06-23 11:25:51.9794
SELECT * FROM [t_Config_DB];

-- 2020-06-23 11:25:51.9794
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-06-23 11:29:09.4933====

-- 2020-06-23 11:29:09.4933
SELECT * FROM [t_Config_DB];

-- 2020-06-23 11:29:09.4933
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-07-23 18:01:44.4122====

-- 2020-07-23 18:01:44.4122
SELECT * FROM [t_Config_DB];

-- 2020-07-23 18:01:44.4122
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-07-23 18:03:53.7757====

-- 2020-07-23 18:03:53.7757
SELECT * FROM [t_Config_DB];

-- 2020-07-23 18:03:53.7767
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-07-23 18:07:05.7573====

-- 2020-07-23 18:07:05.7573
SELECT * FROM [t_Config_DB];

-- 2020-07-23 18:07:05.7573
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-07-23 18:08:11.9588====

-- 2020-07-23 18:08:11.9588
SELECT * FROM [t_Config_DB];

-- 2020-07-23 18:08:11.9588
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-08-06 14:16:29.5421====

-- 2020-08-06 14:16:29.5421
SELECT * FROM [t_Config_DB];

-- 2020-08-06 14:16:29.5431
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-08-07 16:36:00.9357====

-- 2020-08-07 16:36:00.9357
SELECT * FROM [t_Config_DB];

-- 2020-08-07 16:36:00.9377
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-08-14 15:59:50.0826====

-- 2020-08-14 15:59:50.0826
SELECT * FROM [t_Config_DB];

-- 2020-08-14 15:59:50.0836
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-08-14 17:27:25.6829====

-- 2020-08-14 17:27:25.6829
SELECT * FROM [t_Config_DB];

-- 2020-08-14 17:27:25.6849
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-08-14 18:57:40.8803====

-- 2020-08-14 18:57:40.8803
SELECT * FROM [t_Config_DB];

-- 2020-08-14 18:57:40.8813
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-08-14 18:58:47.3519====

-- 2020-08-14 18:58:47.3519
SELECT * FROM [t_Config_DB];

-- 2020-08-14 18:58:47.3519
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-08-17 11:35:59.4613====

-- 2020-08-17 11:35:59.4613
SELECT * FROM [t_Config_DB];

-- 2020-08-17 11:35:59.4633
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-08-17 11:36:57.0017====

-- 2020-08-17 11:36:57.0017
SELECT * FROM [t_Config_DB];

-- 2020-08-17 11:36:57.0026
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-08-18 10:57:16.6418====

-- 2020-08-18 10:57:16.6418
SELECT * FROM [t_Config_DB];

-- 2020-08-18 10:57:16.6428
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-08-18 14:41:53.5713====

-- 2020-08-18 14:41:53.5713
SELECT * FROM [t_Config_DB];

-- 2020-08-18 14:41:53.5723
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-08-28 17:17:54.5735====

-- 2020-08-28 17:17:54.5735
SELECT * FROM [t_Config_DB];

-- 2020-08-28 17:17:54.5744
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-08-28 17:19:22.7651====

-- 2020-08-28 17:19:22.7651
SELECT * FROM [t_Config_DB];

-- 2020-08-28 17:19:22.7651
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-08-28 17:31:41.4571====

-- 2020-08-28 17:31:41.4571
SELECT * FROM [t_Config_DB];

-- 2020-08-28 17:31:41.4571
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-08-28 17:34:13.9097====

-- 2020-08-28 17:34:13.9097
SELECT * FROM [t_Config_DB];

-- 2020-08-28 17:34:13.9107
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-09-01 15:20:30.8619====

-- 2020-09-01 15:20:30.8619
SELECT * FROM [t_Config_DB];

-- 2020-09-01 15:20:30.8629
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-09-01 15:21:16.8712====

-- 2020-09-01 15:21:16.8712
SELECT * FROM [t_Config_DB];

-- 2020-09-01 15:21:16.8712
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-09-01 15:41:51.8781====

-- 2020-09-01 15:41:51.8781
SELECT * FROM [t_Config_DB];

-- 2020-09-01 15:41:51.8781
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-09-01 16:13:02.7955====

-- 2020-09-01 16:13:02.7955
SELECT * FROM [t_Config_DB];

-- 2020-09-01 16:13:02.7965
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-09-01 16:15:58.8162====

-- 2020-09-01 16:15:58.8162
SELECT * FROM [t_Config_DB];

-- 2020-09-01 16:15:58.8171
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-09-01 16:16:48.5968====

-- 2020-09-01 16:16:48.5968
SELECT * FROM [t_Config_DB];

-- 2020-09-01 16:16:48.5978
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-09-01 16:21:08.2692====

-- 2020-09-01 16:21:08.2692
SELECT * FROM [t_Config_DB];

-- 2020-09-01 16:21:08.2692
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-09-01 16:23:55.9895====

-- 2020-09-01 16:23:55.9895
SELECT * FROM [t_Config_DB];

-- 2020-09-01 16:23:55.9895
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-09-01 16:25:17.9218====

-- 2020-09-01 16:25:17.9218
SELECT * FROM [t_Config_DB];

-- 2020-09-01 16:25:17.9228
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-09-01 17:16:31.4486====

-- 2020-09-01 17:16:31.4486
SELECT * FROM [t_Config_DB];

-- 2020-09-01 17:16:31.4486
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-09-01 17:26:34.1295====

-- 2020-09-01 17:26:34.1295
SELECT * FROM [t_Config_DB];

-- 2020-09-01 17:26:34.1295
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-09-01 17:41:46.0810====

-- 2020-09-01 17:41:46.0810
SELECT * FROM [t_Config_DB];

-- 2020-09-01 17:41:46.0810
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-09-01 17:48:30.7995====

-- 2020-09-01 17:48:30.7995
SELECT * FROM [t_Config_DB];

-- 2020-09-01 17:48:30.7995
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-09-01 18:06:25.7447====

-- 2020-09-01 18:06:25.7447
SELECT * FROM [t_Config_DB];

-- 2020-09-01 18:06:25.7457
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-09-01 18:08:39.0239====

-- 2020-09-01 18:08:39.0239
SELECT * FROM [t_Config_DB];

-- 2020-09-01 18:08:39.0249
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-09-01 18:10:47.0090====

-- 2020-09-01 18:10:47.0090
SELECT * FROM [t_Config_DB];

-- 2020-09-01 18:10:47.0100
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-09-01 18:14:57.1794====

-- 2020-09-01 18:14:57.1794
SELECT * FROM [t_Config_DB];

-- 2020-09-01 18:14:57.1794
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-09-01 18:15:53.8410====

-- 2020-09-01 18:15:53.8410
SELECT * FROM [t_Config_DB];

-- 2020-09-01 18:15:53.8410
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-09-01 18:19:14.2192====

-- 2020-09-01 18:19:14.2192
SELECT * FROM [t_Config_DB];

-- 2020-09-01 18:19:14.2192
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-09-02 10:22:46.9225====

-- 2020-09-02 10:22:46.9225
SELECT * FROM [t_Config_DB];

-- 2020-09-02 10:22:46.9235
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-09-02 10:45:06.0758====

-- 2020-09-02 10:45:06.0758
SELECT * FROM [t_Config_DB];

-- 2020-09-02 10:45:06.0758
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-09-02 14:37:21.4486====

-- 2020-09-02 14:37:21.4496
SELECT * FROM [t_Config_DB];

-- 2020-09-02 14:37:21.4506
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-09-02 14:37:51.0013====

-- 2020-09-02 14:37:51.0013
SELECT * FROM [t_Config_DB];

-- 2020-09-02 14:37:51.0013
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-09-02 14:42:29.3332====

-- 2020-09-02 14:42:29.3332
SELECT * FROM [t_Config_DB];

-- 2020-09-02 14:42:29.3342
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-09-02 14:43:57.5107====

-- 2020-09-02 14:43:57.5107
SELECT * FROM [t_Config_DB];

-- 2020-09-02 14:43:57.5107
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-09-02 14:44:58.8486====

-- 2020-09-02 14:44:58.8496
SELECT * FROM [t_Config_DB];

-- 2020-09-02 14:44:58.8496
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-09-02 14:47:10.9220====

-- 2020-09-02 14:47:10.9220
SELECT * FROM [t_Config_DB];

-- 2020-09-02 14:47:10.9230
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-09-04 16:57:41.5190====

-- 2020-09-04 16:57:41.5190
SELECT * FROM [t_Config_DB];

-- 2020-09-04 16:57:41.5210
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-09-04 17:41:55.1572====

-- 2020-09-04 17:41:55.1572
SELECT * FROM [t_Config_DB];

-- 2020-09-04 17:41:55.1581
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-09-18 15:27:33.4606====

-- 2020-09-18 15:27:33.4606
SELECT * FROM [t_Config_DB];

-- 2020-09-18 15:27:33.4616
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-09-18 15:38:57.6373====

-- 2020-09-18 15:38:57.6373
SELECT * FROM [t_Config_DB];

-- 2020-09-18 15:38:57.6373
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-11-05 15:04:09.2017====

-- 2020-11-05 15:04:09.2017
SELECT * FROM [t_Config_DB];

-- 2020-11-05 15:04:09.2037
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-11-05 15:04:37.4257====

-- 2020-11-05 15:04:37.4257
SELECT * FROM [t_Config_DB];

-- 2020-11-05 15:04:37.4257
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-11-05 15:16:30.3969====

-- 2020-11-05 15:16:30.3969
SELECT * FROM [t_Config_DB];

-- 2020-11-05 15:16:30.3969
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-11-19 10:17:15.8347====

-- 2020-11-19 10:17:15.8347
SELECT * FROM [t_Config_DB];

-- 2020-11-19 10:17:15.8357
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-11-19 10:39:31.4113====

-- 2020-11-19 10:39:31.4113
SELECT * FROM [t_Config_DB];

-- 2020-11-19 10:39:31.4123
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-11-19 10:42:39.0727====

-- 2020-11-19 10:42:39.0727
SELECT * FROM [t_Config_DB];

-- 2020-11-19 10:42:39.0727
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-11-19 17:27:10.2731====

-- 2020-11-19 17:27:10.2731
SELECT * FROM [t_Config_DB];

-- 2020-11-19 17:27:10.2741
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-11-19 17:27:42.2481====

-- 2020-11-19 17:27:42.2481
SELECT * FROM [t_Config_DB];

-- 2020-11-19 17:27:42.2491
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-11-19 17:30:00.4454====

-- 2020-11-19 17:30:00.4454
SELECT * FROM [t_Config_DB];

-- 2020-11-19 17:30:00.4454
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-11-19 17:31:22.1699====

-- 2020-11-19 17:31:22.1699
SELECT * FROM [t_Config_DB];

-- 2020-11-19 17:31:22.1699
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-11-20 13:23:10.7496====

-- 2020-11-20 13:23:10.7496
SELECT * FROM [t_Config_DB];

-- 2020-11-20 13:23:10.7496
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-11-20 17:54:23.5346====

-- 2020-11-20 17:54:23.5346
SELECT * FROM [t_Config_DB];

-- 2020-11-20 17:54:23.5346
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-11-25 18:51:25.6219====

-- 2020-11-25 18:51:25.6219
SELECT * FROM [t_Config_DB];

-- 2020-11-25 18:51:25.6229
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-11-25 19:31:06.6960====

-- 2020-11-25 19:31:06.6960
SELECT * FROM [t_Config_DB];

-- 2020-11-25 19:31:06.6970
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-14 11:29:47.2823====

-- 2020-12-14 11:29:47.2823
SELECT * FROM [t_Config_DB];

-- 2020-12-14 11:29:47.2842
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-14 11:30:37.9357====

-- 2020-12-14 11:30:37.9357
SELECT * FROM [t_Config_DB];

-- 2020-12-14 11:30:37.9357
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-14 11:34:03.4775====

-- 2020-12-14 11:34:03.4775
SELECT * FROM [t_Config_DB];

-- 2020-12-14 11:34:03.4775
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-14 11:37:43.7669====

-- 2020-12-14 11:37:43.7669
SELECT * FROM [t_Config_DB];

-- 2020-12-14 11:37:43.7679
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-14 11:40:37.6316====

-- 2020-12-14 11:40:37.6316
SELECT * FROM [t_Config_DB];

-- 2020-12-14 11:40:37.6326
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-14 13:38:20.5380====

-- 2020-12-14 13:38:20.5380
SELECT * FROM [t_Config_DB];

-- 2020-12-14 13:38:20.5380
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-14 16:07:02.3096====

-- 2020-12-14 16:07:02.3096
SELECT * FROM [t_Config_DB];

-- 2020-12-14 16:07:02.3096
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-14 16:39:59.3413====

-- 2020-12-14 16:39:59.3413
SELECT * FROM [t_Config_DB];

-- 2020-12-14 16:39:59.3413
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-14 17:59:00.4740====

-- 2020-12-14 17:59:00.4740
SELECT * FROM [t_Config_DB];

-- 2020-12-14 17:59:00.4750
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-14 18:01:49.8064====

-- 2020-12-14 18:01:49.8064
SELECT * FROM [t_Config_DB];

-- 2020-12-14 18:01:49.8074
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-14 18:03:37.4536====

-- 2020-12-14 18:03:37.4536
SELECT * FROM [t_Config_DB];

-- 2020-12-14 18:03:37.4536
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-14 18:03:58.5245====

-- 2020-12-14 18:03:58.5245
SELECT * FROM [t_Config_DB];

-- 2020-12-14 18:03:58.5255
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-14 18:08:00.3861====

-- 2020-12-14 18:08:00.3861
SELECT * FROM [t_Config_DB];

-- 2020-12-14 18:08:00.3861
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-14 18:08:18.5468====

-- 2020-12-14 18:08:18.5468
SELECT * FROM [t_Config_DB];

-- 2020-12-14 18:08:18.5468
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-14 18:10:18.1688====

-- 2020-12-14 18:10:18.1688
SELECT * FROM [t_Config_DB];

-- 2020-12-14 18:10:18.1688
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-23 15:50:20.0402====

-- 2020-12-23 15:50:20.0402
SELECT * FROM [t_Config_DB];

-- 2020-12-23 15:50:20.0412
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-23 16:40:59.4663====

-- 2020-12-23 16:40:59.4663
SELECT * FROM [t_Config_DB];

-- 2020-12-23 16:40:59.4663
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-23 18:04:39.6942====

-- 2020-12-23 18:04:39.6942
SELECT * FROM [t_Config_DB];

-- 2020-12-23 18:04:39.6952
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-23 18:07:09.4078====

-- 2020-12-23 18:07:09.4078
SELECT * FROM [t_Config_DB];

-- 2020-12-23 18:07:09.4078
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-23 18:07:25.3914====

-- 2020-12-23 18:07:25.3914
SELECT * FROM [t_Config_DB];

-- 2020-12-23 18:07:25.3914
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-23 18:09:24.1346====

-- 2020-12-23 18:09:24.1346
SELECT * FROM [t_Config_DB];

-- 2020-12-23 18:09:24.1356
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-23 18:10:30.0082====

-- 2020-12-23 18:10:30.0082
SELECT * FROM [t_Config_DB];

-- 2020-12-23 18:10:30.0082
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-23 18:41:42.3949====

-- 2020-12-23 18:41:42.3949
SELECT * FROM [t_Config_DB];

-- 2020-12-23 18:41:42.3949
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-23 18:44:01.4343====

-- 2020-12-23 18:44:01.4343
SELECT * FROM [t_Config_DB];

-- 2020-12-23 18:44:01.4353
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-23 18:55:08.3591====

-- 2020-12-23 18:55:08.3591
SELECT * FROM [t_Config_DB];

-- 2020-12-23 18:55:08.3591
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-23 18:55:43.1540====

-- 2020-12-23 18:55:43.1540
SELECT * FROM [t_Config_DB];

-- 2020-12-23 18:55:43.1550
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-24 09:50:44.7550====

-- 2020-12-24 09:50:44.7550
SELECT * FROM [t_Config_DB];

-- 2020-12-24 09:50:44.7550
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-25 16:35:55.7386====

-- 2020-12-25 16:35:55.7386
SELECT * FROM [t_Config_DB];

-- 2020-12-25 16:35:55.7396
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-25 17:14:45.8783====

-- 2020-12-25 17:14:45.8783
SELECT * FROM [t_Config_DB];

-- 2020-12-25 17:14:45.8793
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-25 17:46:18.5225====

-- 2020-12-25 17:46:18.5225
SELECT * FROM [t_Config_DB];

-- 2020-12-25 17:46:18.5225
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-25 17:50:47.9849====

-- 2020-12-25 17:50:47.9849
SELECT * FROM [t_Config_DB];

-- 2020-12-25 17:50:47.9849
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-25 18:05:48.8416====

-- 2020-12-25 18:05:48.8416
SELECT * FROM [t_Config_DB];

-- 2020-12-25 18:05:48.8416
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-26 10:34:51.8515====

-- 2020-12-26 10:34:51.8515
SELECT * FROM [t_Config_DB];

-- 2020-12-26 10:34:51.8515
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-26 10:35:23.6944====

-- 2020-12-26 10:35:23.6944
SELECT * FROM [t_Config_DB];

-- 2020-12-26 10:35:23.6944
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-26 10:37:40.1125====

-- 2020-12-26 10:37:40.1125
SELECT * FROM [t_Config_DB];

-- 2020-12-26 10:37:40.1125
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-26 10:38:05.4907====

-- 2020-12-26 10:38:05.4907
SELECT * FROM [t_Config_DB];

-- 2020-12-26 10:38:05.4907
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-26 10:40:11.7619====

-- 2020-12-26 10:40:11.7619
SELECT * FROM [t_Config_DB];

-- 2020-12-26 10:40:11.7619
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-26 11:38:20.6184====

-- 2020-12-26 11:38:20.6184
SELECT * FROM [t_Config_DB];

-- 2020-12-26 11:38:20.6184
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-26 11:42:09.1072====

-- 2020-12-26 11:42:09.1072
SELECT * FROM [t_Config_DB];

-- 2020-12-26 11:42:09.1072
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-26 11:53:05.0749====

-- 2020-12-26 11:53:05.0749
SELECT * FROM [t_Config_DB];

-- 2020-12-26 11:53:05.0759
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-26 12:02:00.9007====

-- 2020-12-26 12:02:00.9007
SELECT * FROM [t_Config_DB];

-- 2020-12-26 12:02:00.9007
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-28 14:50:35.3078====

-- 2020-12-28 14:50:35.3078
SELECT * FROM [t_Config_DB];

-- 2020-12-28 14:50:35.3078
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-29 11:18:45.6876====

-- 2020-12-29 11:18:45.6876
SELECT * FROM [t_Config_DB];

-- 2020-12-29 11:18:45.6886
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-30 14:45:08.1920====

-- 2020-12-30 14:45:08.1920
SELECT * FROM [t_Config_DB];

-- 2020-12-30 14:45:08.1920
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-30 14:47:08.5478====

-- 2020-12-30 14:47:08.5478
SELECT * FROM [t_Config_DB];

-- 2020-12-30 14:47:08.5478
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-30 14:57:11.4714====

-- 2020-12-30 14:57:11.4714
SELECT * FROM [t_Config_DB];

-- 2020-12-30 14:57:11.4724
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-30 16:31:22.4697====

-- 2020-12-30 16:31:22.4697
SELECT * FROM [t_Config_DB];

-- 2020-12-30 16:31:22.4697
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-30 16:35:42.2036====

-- 2020-12-30 16:35:42.2036
SELECT * FROM [t_Config_DB];

-- 2020-12-30 16:35:42.2046
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-30 17:15:55.5771====

-- 2020-12-30 17:15:55.5771
SELECT * FROM [t_Config_DB];

-- 2020-12-30 17:15:55.5771
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-30 17:22:43.9259====

-- 2020-12-30 17:22:43.9259
SELECT * FROM [t_Config_DB];

-- 2020-12-30 17:22:43.9259
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-30 17:25:42.8733====

-- 2020-12-30 17:25:42.8733
SELECT * FROM [t_Config_DB];

-- 2020-12-30 17:25:42.8743
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-30 18:36:37.3678====

-- 2020-12-30 18:36:37.3678
SELECT * FROM [t_Config_DB];

-- 2020-12-30 18:36:37.3678
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-30 18:38:09.3179====

-- 2020-12-30 18:38:09.3179
SELECT * FROM [t_Config_DB];

-- 2020-12-30 18:38:09.3179
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-30 18:38:40.9912====

-- 2020-12-30 18:38:40.9912
SELECT * FROM [t_Config_DB];

-- 2020-12-30 18:38:40.9912
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-31 09:19:13.3159====

-- 2020-12-31 09:19:13.3159
SELECT * FROM [t_Config_DB];

-- 2020-12-31 09:19:13.3169
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-31 09:32:09.5477====

-- 2020-12-31 09:32:09.5477
SELECT * FROM [t_Config_DB];

-- 2020-12-31 09:32:09.5487
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-31 10:13:24.5543====

-- 2020-12-31 10:13:24.5543
SELECT * FROM [t_Config_DB];

-- 2020-12-31 10:13:24.5553
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2020-12-31 14:19:30.0982====

-- 2020-12-31 14:19:30.0982
SELECT * FROM [t_Config_DB];

-- 2020-12-31 14:19:30.0992
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-01-04 09:27:47.3774====

-- 2021-01-04 09:27:47.3774
SELECT * FROM [t_Config_DB];

-- 2021-01-04 09:27:47.3774
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-01-11 16:03:07.9349====

-- 2021-01-11 16:03:07.9349
SELECT * FROM [t_Config_DB];

-- 2021-01-11 16:03:07.9359
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-01-11 16:03:44.4476====

-- 2021-01-11 16:03:44.4476
SELECT * FROM [t_Config_DB];

-- 2021-01-11 16:03:44.4476
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-01-11 16:31:48.3568====

-- 2021-01-11 16:31:48.3568
SELECT * FROM [t_Config_DB];

-- 2021-01-11 16:31:48.3568
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-01-11 16:33:33.9035====

-- 2021-01-11 16:33:33.9035
SELECT * FROM [t_Config_DB];

-- 2021-01-11 16:33:33.9035
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-01-11 18:03:49.9516====

-- 2021-01-11 18:03:49.9516
SELECT * FROM [t_Config_DB];

-- 2021-01-11 18:03:49.9526
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-01-11 18:17:14.2880====

-- 2021-01-11 18:17:14.2880
SELECT * FROM [t_Config_DB];

-- 2021-01-11 18:17:14.2890
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-01-12 13:21:15.0425====

-- 2021-01-12 13:21:15.0425
SELECT * FROM [t_Config_DB];

-- 2021-01-12 13:21:15.0435
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-01-12 15:02:15.4299====

-- 2021-01-12 15:02:15.4299
SELECT * FROM [t_Config_DB];

-- 2021-01-12 15:02:15.4299
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-01-12 15:06:51.4636====

-- 2021-01-12 15:06:51.4636
SELECT * FROM [t_Config_DB];

-- 2021-01-12 15:06:51.4636
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-01-12 15:10:58.1895====

-- 2021-01-12 15:10:58.1895
SELECT * FROM [t_Config_DB];

-- 2021-01-12 15:10:58.1895
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-01-12 15:12:35.5002====

-- 2021-01-12 15:12:35.5002
SELECT * FROM [t_Config_DB];

-- 2021-01-12 15:12:35.5012
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-01-12 15:34:58.2339====

-- 2021-01-12 15:34:58.2339
SELECT * FROM [t_Config_DB];

-- 2021-01-12 15:34:58.2339
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-01-12 15:43:15.3238====

-- 2021-01-12 15:43:15.3238
SELECT * FROM [t_Config_DB];

-- 2021-01-12 15:43:15.3248
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-01-12 16:35:02.4197====

-- 2021-01-12 16:35:02.4197
SELECT * FROM [t_Config_DB];

-- 2021-01-12 16:35:02.4207
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-01-12 16:40:21.3827====

-- 2021-01-12 16:40:21.3827
SELECT * FROM [t_Config_DB];

-- 2021-01-12 16:40:21.3837
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-01-12 16:41:56.3955====

-- 2021-01-12 16:41:56.3955
SELECT * FROM [t_Config_DB];

-- 2021-01-12 16:41:56.3955
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-01-12 16:42:20.5360====

-- 2021-01-12 16:42:20.5360
SELECT * FROM [t_Config_DB];

-- 2021-01-12 16:42:20.5370
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-01-12 16:46:59.9529====

-- 2021-01-12 16:46:59.9529
SELECT * FROM [t_Config_DB];

-- 2021-01-12 16:46:59.9539
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-01-12 17:06:35.8000====

-- 2021-01-12 17:06:35.8000
SELECT * FROM [t_Config_DB];

-- 2021-01-12 17:06:35.8000
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-01-13 13:26:33.4242====

-- 2021-01-13 13:26:33.4242
SELECT * FROM [t_Config_DB];

-- 2021-01-13 13:26:33.4242
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-01-13 13:27:14.9312====

-- 2021-01-13 13:27:14.9312
SELECT * FROM [t_Config_DB];

-- 2021-01-13 13:27:14.9312
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-01-13 13:37:21.8076====

-- 2021-01-13 13:37:21.8076
SELECT * FROM [t_Config_DB];

-- 2021-01-13 13:37:21.8076
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-01-15 16:59:08.1646====

-- 2021-01-15 16:59:08.1646
SELECT * FROM [t_Config_DB];

-- 2021-01-15 16:59:08.1656
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-01-27 15:22:36.2936====

-- 2021-01-27 15:22:36.2936
SELECT * FROM [t_Config_DB];

-- 2021-01-27 15:22:36.2946
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-02-03 16:55:51.0722====

-- 2021-02-03 16:55:51.0722
SELECT * FROM [t_Config_DB];

-- 2021-02-03 16:55:51.0732
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-01 11:27:42.7411====

-- 2021-03-01 11:27:42.7411
SELECT * FROM [t_Config_DB];

-- 2021-03-01 11:27:42.7421
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-01 17:41:59.5746====

-- 2021-03-01 17:41:59.5746
SELECT * FROM [t_Config_DB];

-- 2021-03-01 17:41:59.5746
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-02 15:35:49.9506====

-- 2021-03-02 15:35:49.9506
SELECT * FROM [t_Config_DB];

-- 2021-03-02 15:35:49.9506
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-03 13:20:40.4890====

-- 2021-03-03 13:20:40.4890
SELECT * FROM [t_Config_DB];

-- 2021-03-03 13:20:40.4900
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-03 13:38:05.6265====

-- 2021-03-03 13:38:05.6265
SELECT * FROM [t_Config_DB];

-- 2021-03-03 13:38:05.6285
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-04 15:23:29.1200====

-- 2021-03-04 15:23:29.1200
SELECT * FROM [t_Config_DB];

-- 2021-03-04 15:23:29.1210
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-16 13:31:33.4522====

-- 2021-03-16 13:31:33.4522
SELECT * FROM [t_Config_DB];

-- 2021-03-16 13:31:33.4541
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-16 13:32:40.0651====

-- 2021-03-16 13:32:40.0651
SELECT * FROM [t_Config_DB];

-- 2021-03-16 13:32:40.0651
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-16 13:39:37.3864====

-- 2021-03-16 13:39:37.3864
SELECT * FROM [t_Config_DB];

-- 2021-03-16 13:39:37.3864
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-16 13:42:02.9353====

-- 2021-03-16 13:42:02.9353
SELECT * FROM [t_Config_DB];

-- 2021-03-16 13:42:02.9353
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-16 13:43:00.7029====

-- 2021-03-16 13:43:00.7029
SELECT * FROM [t_Config_DB];

-- 2021-03-16 13:43:00.7039
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-16 13:43:47.3466====

-- 2021-03-16 13:43:47.3466
SELECT * FROM [t_Config_DB];

-- 2021-03-16 13:43:47.3466
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-16 13:45:49.1673====

-- 2021-03-16 13:45:49.1673
SELECT * FROM [t_Config_DB];

-- 2021-03-16 13:45:49.1673
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-16 13:56:59.5986====

-- 2021-03-16 13:56:59.5986
SELECT * FROM [t_Config_DB];

-- 2021-03-16 13:56:59.5996
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-16 13:57:47.6770====

-- 2021-03-16 13:57:47.6770
SELECT * FROM [t_Config_DB];

-- 2021-03-16 13:57:47.6770
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-16 14:00:17.8055====

-- 2021-03-16 14:00:17.8055
SELECT * FROM [t_Config_DB];

-- 2021-03-16 14:00:17.8065
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-16 14:02:08.7022====

-- 2021-03-16 14:02:08.7022
SELECT * FROM [t_Config_DB];

-- 2021-03-16 14:02:08.7032
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-16 14:08:00.3419====

-- 2021-03-16 14:08:00.3419
SELECT * FROM [t_Config_DB];

-- 2021-03-16 14:08:00.3419
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-16 15:02:14.4733====

-- 2021-03-16 15:02:14.4733
SELECT * FROM [t_Config_DB];

-- 2021-03-16 15:02:14.4733
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-16 15:18:45.5697====

-- 2021-03-16 15:18:45.5697
SELECT * FROM [t_Config_DB];

-- 2021-03-16 15:18:45.5697
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-16 15:19:36.3000====

-- 2021-03-16 15:19:36.3000
SELECT * FROM [t_Config_DB];

-- 2021-03-16 15:19:36.3010
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-16 15:27:36.0049====

-- 2021-03-16 15:27:36.0049
SELECT * FROM [t_Config_DB];

-- 2021-03-16 15:27:36.0049
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-16 16:14:44.6871====

-- 2021-03-16 16:14:44.6871
SELECT * FROM [t_Config_DB];

-- 2021-03-16 16:14:44.6871
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-16 16:47:45.9670====

-- 2021-03-16 16:47:45.9670
SELECT * FROM [t_Config_DB];

-- 2021-03-16 16:47:45.9670
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-16 17:24:36.4640====

-- 2021-03-16 17:24:36.4640
SELECT * FROM [t_Config_DB];

-- 2021-03-16 17:24:36.4640
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-16 17:28:07.1650====

-- 2021-03-16 17:28:07.1650
SELECT * FROM [t_Config_DB];

-- 2021-03-16 17:28:07.1660
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-16 17:41:34.9099====

-- 2021-03-16 17:41:34.9099
SELECT * FROM [t_Config_DB];

-- 2021-03-16 17:41:34.9099
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-16 17:42:34.9950====

-- 2021-03-16 17:42:34.9950
SELECT * FROM [t_Config_DB];

-- 2021-03-16 17:42:34.9950
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-16 17:51:17.8716====

-- 2021-03-16 17:51:17.8716
SELECT * FROM [t_Config_DB];

-- 2021-03-16 17:51:17.8716
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-16 17:51:57.8487====

-- 2021-03-16 17:51:57.8487
SELECT * FROM [t_Config_DB];

-- 2021-03-16 17:51:57.8497
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-16 17:52:17.2189====

-- 2021-03-16 17:52:17.2189
SELECT * FROM [t_Config_DB];

-- 2021-03-16 17:52:17.2189
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-16 17:56:02.7249====

-- 2021-03-16 17:56:02.7249
SELECT * FROM [t_Config_DB];

-- 2021-03-16 17:56:02.7259
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-16 18:12:29.2855====

-- 2021-03-16 18:12:29.2855
SELECT * FROM [t_Config_DB];

-- 2021-03-16 18:12:29.2865
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-16 18:13:26.3642====

-- 2021-03-16 18:13:26.3642
SELECT * FROM [t_Config_DB];

-- 2021-03-16 18:13:26.3652
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-16 18:14:08.8239====

-- 2021-03-16 18:14:08.8239
SELECT * FROM [t_Config_DB];

-- 2021-03-16 18:14:08.8239
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-16 18:25:38.8842====

-- 2021-03-16 18:25:38.8842
SELECT * FROM [t_Config_DB];

-- 2021-03-16 18:25:38.8852
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-16 18:28:32.5512====

-- 2021-03-16 18:28:32.5512
SELECT * FROM [t_Config_DB];

-- 2021-03-16 18:28:32.5522
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-16 18:29:22.6790====

-- 2021-03-16 18:29:22.6790
SELECT * FROM [t_Config_DB];

-- 2021-03-16 18:29:22.6800
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-16 18:31:32.5408====

-- 2021-03-16 18:31:32.5408
SELECT * FROM [t_Config_DB];

-- 2021-03-16 18:31:32.5408
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-16 18:40:47.5539====

-- 2021-03-16 18:40:47.5539
SELECT * FROM [t_Config_DB];

-- 2021-03-16 18:40:47.5539
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-16 18:45:44.1520====

-- 2021-03-16 18:45:44.1520
SELECT * FROM [t_Config_DB];

-- 2021-03-16 18:45:44.1520
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-17 10:00:07.2280====

-- 2021-03-17 10:00:07.2280
SELECT * FROM [t_Config_DB];

-- 2021-03-17 10:00:07.2280
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-17 14:12:09.5738====

-- 2021-03-17 14:12:09.5738
SELECT * FROM [t_Config_DB];

-- 2021-03-17 14:12:09.5738
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-17 14:14:56.1536====

-- 2021-03-17 14:14:56.1536
SELECT * FROM [t_Config_DB];

-- 2021-03-17 14:14:56.1536
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-25 16:28:11.8019====

-- 2021-03-25 16:28:11.8019
SELECT * FROM [t_Config_DB];

-- 2021-03-25 16:28:11.8039
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-25 16:28:59.2934====

-- 2021-03-25 16:28:59.2934
SELECT * FROM [t_Config_DB];

-- 2021-03-25 16:28:59.2934
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-29 15:28:15.9327====

-- 2021-03-29 15:28:15.9327
SELECT * FROM [t_Config_DB];

-- 2021-03-29 15:28:15.9357
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-29 16:45:52.3222====

-- 2021-03-29 16:45:52.3222
SELECT * FROM [t_Config_DB];

-- 2021-03-29 16:45:52.3222
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-03-30 15:56:48.0063====

-- 2021-03-30 15:56:48.0063
SELECT * FROM [t_Config_DB];

-- 2021-03-30 15:56:48.0063
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-04-09 15:48:36.0121====

-- 2021-04-09 15:48:36.0121
SELECT * FROM [t_Config_DB];

-- 2021-04-09 15:48:36.0131
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-04-13 14:38:40.2785====

-- 2021-04-13 14:38:40.2785
SELECT * FROM [t_Config_DB];

-- 2021-04-13 14:38:40.2795
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-04-14 15:41:21.6776====

-- 2021-04-14 15:41:21.6776
SELECT * FROM [t_Config_DB];

-- 2021-04-14 15:41:21.6776
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-04-14 15:48:49.3703====

-- 2021-04-14 15:48:49.3703
SELECT * FROM [t_Config_DB];

-- 2021-04-14 15:48:49.3703
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-04-14 16:17:12.2424====

-- 2021-04-14 16:17:12.2424
SELECT * FROM [t_Config_DB];

-- 2021-04-14 16:17:12.2424
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-04-14 16:19:35.7674====

-- 2021-04-14 16:19:35.7674
SELECT * FROM [t_Config_DB];

-- 2021-04-14 16:19:35.7674
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-04-14 16:28:48.5774====

-- 2021-04-14 16:28:48.5774
SELECT * FROM [t_Config_DB];

-- 2021-04-14 16:28:48.5774
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-04-14 16:38:19.0531====

-- 2021-04-14 16:38:19.0531
SELECT * FROM [t_Config_DB];

-- 2021-04-14 16:38:19.0541
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-04-15 18:53:23.9249====

-- 2021-04-15 18:53:23.9249
SELECT * FROM [t_Config_DB];

-- 2021-04-15 18:53:23.9309
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-04-19 13:51:46.7455====

-- 2021-04-19 13:51:46.7455
SELECT * FROM [t_Config_DB];

-- 2021-04-19 13:51:46.7465
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-04-30 15:10:25.4783====

-- 2021-04-30 15:10:25.4783
SELECT * FROM [t_Config_DB];

-- 2021-04-30 15:10:25.4793
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-05-11 18:03:34.9699====

-- 2021-05-11 18:03:34.9699
SELECT * FROM [t_Config_DB];

-- 2021-05-11 18:03:34.9709
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-05-12 14:05:00.1055====

-- 2021-05-12 14:05:00.1055
SELECT * FROM [t_Config_DB];

-- 2021-05-12 14:05:00.1074
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-05-19 15:47:54.5304====

-- 2021-05-19 15:47:54.5304
SELECT * FROM [t_Config_DB];

-- 2021-05-19 15:47:54.5314
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-05-26 18:56:07.5666====

-- 2021-05-26 18:56:07.5666
SELECT * FROM [t_Config_DB];

-- 2021-05-26 18:56:07.5686
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-06-02 10:31:25.3469====

-- 2021-06-02 10:31:25.3479
SELECT * FROM [t_Config_DB];

-- 2021-06-02 10:31:25.3569
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-06-09 14:44:11.4935====

-- 2021-06-09 14:44:11.4945
SELECT * FROM [t_Config_DB];

-- 2021-06-09 14:44:11.5045
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


-- ====2021-06-10 16:38:14.0943====

-- 2021-06-10 16:38:14.0943
SELECT * FROM [t_Config_DB];

-- 2021-06-10 16:38:14.1032
CREATE TABLE IF NOT EXISTS [t_ResultItem] (
	[location_code] INTEGER,
	[location_name] VARCHAR,
	[location_code_parent] VARCHAR,
	[counTry_iso_code] VARCHAR,
	[location_type] VARCHAR,
	[geo_name] VARCHAR,
	[geo_id] VARCHAR,
	[geo_name_CN] VARCHAR,
	[ObjectDBID] INTEGER PRIMARY KEY AUTOINCREMENT);


